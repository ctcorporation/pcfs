﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface IMapEnumRepository : IGenericRepository<Mapping_Enums>
    {
        SatelliteModel SatelliteModel { get; }
    }
}