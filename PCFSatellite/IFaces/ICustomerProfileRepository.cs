﻿
using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface ICustomerProfileRepository : IGenericRepository<Customer_Profile>
    {
        SatelliteModel SatelliteModel { get; }
    }
}