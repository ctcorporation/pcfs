﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface IDTSRepository : IGenericRepository<DTS>
    {
        SatelliteModel SatelliteModel { get; }
    }
}