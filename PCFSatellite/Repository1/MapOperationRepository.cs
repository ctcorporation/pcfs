﻿using PCFSatellite.Models;
using System.Collections.Generic;
using System.Linq;

namespace PCFSatellite.Repository
{
    public class MapOperationRepository : GenericRepository<MapOperation>, IMapOperationRepository
    {
        public MapOperationRepository(SatelliteModel context)
            : base(context)
        {

        }

        public List<string> GetDistinctMaps()
        {
            return Context.Set<MapOperation>().Select(x => x.MD_MapDescription).Distinct().ToList();
        }

        public SatelliteModel SatelliteModel
        {
            get
            { return Context as SatelliteModel; }
        }
    }
}
