﻿using PCFSatellite.Models;

namespace PCFSatellite.Repository
{
    public class MapEnumRepository : GenericRepository<Mapping_Enums>, IMapEnumRepository
    {
        public MapEnumRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get { return Context as SatelliteModel; }

        }
    }
}
