﻿using PCFSatellite.Models;

namespace PCFSatellite.Repository
{
    public class DTSRepository : GenericRepository<DTS>, IDTSRepository
    {
        public DTSRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get
            {
                return Context as SatelliteModel;
            }
        }
    }
}
