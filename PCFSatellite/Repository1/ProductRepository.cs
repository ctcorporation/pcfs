﻿using PCFSatellite.Models;

namespace PCFSatellite.Repository
{
    public class ProductRepository : GenericRepository<Models.Product>, IProductRepository
    {
        public ProductRepository(SatelliteModel connString)
            : base(connString)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get { return Context as SatelliteModel; }
        }
    }
}
