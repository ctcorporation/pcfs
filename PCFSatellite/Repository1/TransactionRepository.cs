﻿
using PCFSatellite.Models;

namespace PCFSatellite
{
    public class TransactionRepository : GenericRepository<Transaction>, ITransactionRepository
    {

        public TransactionRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel NodeDataContext
        {
            get
            {
                return Context as SatelliteModel;
            }
        }
    }


}
