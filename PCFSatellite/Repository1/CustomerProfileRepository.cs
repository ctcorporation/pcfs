﻿
using PCFSatellite.Models;

namespace PCFSatellite.Repository
{
    public class CustomerProfileRepository : GenericRepository<Customer_Profile>, ICustomerProfileRepository
    {
        public CustomerProfileRepository(SatelliteModel context)
            : base(context)
        {


        }

        public SatelliteModel SatelliteModel
        {
            get
            {
                return Context as SatelliteModel;
            }
        }
    }
}
