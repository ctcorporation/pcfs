﻿

using PCFSatellite.Models;

namespace PCFSatellite
{
    public class Cargowise_EnumsRepository : GenericRepository<Cargowise_Enums>, ICargowise_EnumsRepository
    {
        public Cargowise_EnumsRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get
            {
                return Context as SatelliteModel;
            }
        }

    }
}
