﻿using PCFSatellite.Models;

namespace PCFSatellite.Repository
{
    public class CargowiseContextRepository : GenericRepository<CargowiseContext>, ICargowiseContextRepository
    {
        public CargowiseContextRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get
            {
                return Context as SatelliteModel;
            }
        }
    }
}
