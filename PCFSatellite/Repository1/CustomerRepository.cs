﻿

using PCFSatellite.Models;

namespace PCFSatellite
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get
            {
                return Context as SatelliteModel;

            }
        }
    }
}
