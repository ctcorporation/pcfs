﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace PCFSatellite
{
    public static class NodeExtensions
    {
        public static string ToLogString(this Exception exception, string environmentStackTrace)
        {
            List<string> environmentStackTraceLines = GetUserStackTraceLines(environmentStackTrace);
            if (environmentStackTraceLines.Count > 0)
            {
                environmentStackTraceLines.RemoveAt(0);
            }


            List<string> stackTraceLines = GetStackTraceLines(exception.StackTrace);
            stackTraceLines.AddRange(environmentStackTraceLines);

            string fullStackTrace = String.Join(Environment.NewLine, stackTraceLines);

            string logMessage = exception.Message + Environment.NewLine + fullStackTrace;
            return logMessage;
        }

        private static List<string> GetStackTraceLines(string stackTrace)
        {
            return stackTrace.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
        }

        /// <summary>
        ///  Gets a list of stack frame lines, as strings, only including those for which line number is known.
        /// </summary>
        /// <param name="fullStackTrace">Full stack trace, including external code.</param>
        private static List<string> GetUserStackTraceLines(string fullStackTrace)
        {
            List<string> outputList = new List<string>();
            Regex regex = new Regex(@"([^\)]*\)) in (.*):line (\d)*$");

            List<string> stackTraceLines = GetStackTraceLines(fullStackTrace);
            foreach (string stackTraceLine in stackTraceLines)
            {
                if (!regex.IsMatch(stackTraceLine))
                {
                    continue;
                }

                outputList.Add(stackTraceLine);
            }

            return outputList;
        }

        public static string NodeExists(this XElement parent, string elementName, string retVal = null)
        {
            var foundEl = parent.Element(elementName);
            if (foundEl != null)
            {
                return foundEl.Value;
            }
            return retVal;
        }
    }


    public static class StringEx
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string RemoveInvalidChars(this string value)
        {
            if (value.Contains(@"/"))
            {
                value = value.Replace('/', '-');
            }
            if (value.Contains(@"\"))
            {
                value = value.Replace('\\', '-');
            }
            return value;
        }
    }



    public static class TableEx
    {
        public static bool ContainsColumn(this DataRow tbl, string columnName)
        {
            if (tbl.Table.Columns.Contains(columnName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}
