﻿using System;

namespace PCFSatellite
{
    public class HeartBeatArgs : EventArgs
    {
        public string CustCode { get; set; }
        public string Message { get; set; }


    }
}
