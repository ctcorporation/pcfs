﻿using System;

namespace CNodeBE
{
    public class CustProfileRecord
    {

        public Guid C_id
        {
            get;
            set;
        }
        public Guid P_id
        {
            get;
            set;
        }
        public string C_name
        {
            get;
            set;
        }
        public string C_is_active
        {
            get;
            set;
        }
        public string C_on_hold
        {
            get;
            set;
        }
        public string C_path
        {
            get;
            set;
        }
        public string C_ftp_client
        {
            get;
            set;
        }
        public string P_reasoncode
        {
            get;
            set;
        }
        public string P_EventCode
        {
            get;
            set;
        }
        public string P_server
        {
            get;
            set;
        }
        public string P_username
        {
            get;
            set;
        }
        public string P_path
        {
            get;
            set;
        }
        public string P_password
        {
            get;
            set;
        }

        public bool P_ssl
        {
            get; set;
        }
        public string P_delivery
        {
            get;
            set;
        }
        public string P_Recipientid
        {
            get;
            set;
        }
        public string P_Senderid
        {
            get;
            set;
        }
        public string P_description
        {
            get;
            set;
        }
        public string P_port
        {
            get;
            set;
        }
        public string C_code
        {
            get;
            set;
        }
        public string P_Direction
        {
            get;
            set;
        }
        public Boolean P_DTS
        {
            get;
            set;
        }
        public string P_BillTo
        {
            get;
            set;
        }
        public string P_MsgType
        {
            get;
            set;
        }
        public string P_EmailAddress
        {
            get;
            set;
        }
        public string P_Subject
        {
            get;
            set;
        }

        public string P_Library
        {
            get;
            set;
        }
        public string P_Method
        {
            get;
            set;
        }
        public string P_Params
        {
            get;
            set;
        }

        public string P_NotifyEmail
        {
            get;
            set;
        }
        public string P_XSD
        {
            get;
            set;
        }

        public int F_DataFrom
        {
            get;
            set;
        }
        public bool F_HasHeaders
        {
            get;
            set;
        }
        public bool F_FieldCount
        {
            get;
            set;
        }

        public int F_HeaderFrom
        {
            get;
            set;
        }
    }
}
