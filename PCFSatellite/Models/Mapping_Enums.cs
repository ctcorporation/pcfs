﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PCFSatellite.Models
{
    public class Mapping_Enums
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid E_ID
        { get; set; }

        [MaxLength(50)]
        public string E_PROCESSNAME
        { get; set; }

        [MaxLength(50)]
        public string E_FIELDNAME
        { get; set; }

        [MaxLength(3)]
        public string E_FIELDTYPE
        { get; set; }

        public int E_LENGTH
        {
            get; set;

        }
    }
}
