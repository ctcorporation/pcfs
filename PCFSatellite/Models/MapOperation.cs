namespace PCFSatellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MapOperation
    {
        [Key]
        public Guid MD_ID { get; set; }

        [Required]
        [StringLength(100)]
        public string MD_MapDescription { get; set; }

        [Required]
        [StringLength(4)]
        public string MD_Type { get; set; }

        [StringLength(50)]
        public string MD_FromField { get; set; }

        [StringLength(50)]
        public string MD_ToField { get; set; }

        public string MD_DataType { get; set; }
    }
}
