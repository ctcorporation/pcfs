namespace PCFSatellite.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PROCESSING_ERRORS")]
    public partial class Processing_Error
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid E_PK { get; set; }

        [StringLength(15)]
        public string E_SENDERID { get; set; }

        [StringLength(15)]
        public string E_RECIPIENTID { get; set; }

        public DateTime? E_PROCDATE { get; set; }

        public string E_FILENAME { get; set; }

        public string E_ERRORDESC { get; set; }

        [StringLength(10)]
        public string E_ERRORCODE { get; set; }

        public Guid? E_P { get; set; }

        [Required]
        [StringLength(1)]
        public string E_IGNORE { get; set; }

        [StringLength(50)]
        public string E_REFERENCE { get; set; }

        [StringLength(20)]
        public string E_REFTYPE { get; set; }
    }
}
