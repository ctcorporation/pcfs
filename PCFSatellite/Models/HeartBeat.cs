﻿
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PCFSatellite
{
    static class HeartBeat
    {

        static Guid GetCustID(string custCode)
        {
            using (NodeData.IUnitOfWork uow = new NodeData.DTO.UnitOfWork(new NodeDataContext(Globals.CTCconnString())))
            {
                var cust = uow.Customers.Find(x => x.C_CODE == custCode).FirstOrDefault();
                return cust.C_ID;
            }
        }
        public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters)
        {
            try
            {

            }

            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;


            }
            using (NodeData.IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.CTCconnString())))
            {
                NodeData.Models.HeartBeat heartBeat = new NodeData.Models.HeartBeat();
                var cust = uow.Customers.Find(x => x.C_CODE == custCode).FirstOrDefault();
                if (cust == null)
                {
                    return;
                }
                heartBeat = uow.HeartBeats.Find(x => x.HB_C == cust.C_ID).FirstOrDefault();
                bool newHeartBeat = false;
                if (heartBeat == null)
                {
                    heartBeat = new NodeData.Models.HeartBeat
                    {
                        HB_ID = Guid.NewGuid(),
                        HB_C = GetCustID(custCode),
                        HB_NAME = Assembly.GetExecutingAssembly().GetName().Name,
                        HB_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name),
                        HB_ISMONITORED = "Y"

                    };
                    newHeartBeat = true;
                }
                heartBeat.HB_LASTCHECKIN = DateTime.Now;
                heartBeat.HB_LASTOPERATION = operation;
                string pList = string.Empty;
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        pList += parameters[i].Name + ": " + parameters[i].ToString();
                    }
                }
                heartBeat.HB_LASTOPERATIONPARAMS = pList;
                if (operation == "Starting")
                {
                    heartBeat.HB_OPENED = DateTime.Now;
                    Process currentProcess = Process.GetCurrentProcess();
                    heartBeat.HB_PID = currentProcess.Id;
                }
                if (operation == "Stopping")
                {

                    heartBeat.HB_OPENED = null;
                    heartBeat.HB_LASTCHECKIN = null;
                    heartBeat.HB_PID = 0;

                }
                if (newHeartBeat)
                {
                    uow.HeartBeats.Add(heartBeat);
                }
                uow.Complete();
            }
        }
    }
}
