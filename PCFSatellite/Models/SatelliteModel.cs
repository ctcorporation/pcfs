namespace PCFSatellite.Models
{
    using System.Data.Entity;

    //public class SatelliteDBConfiguration : DbConfiguration
    //{

    //    public SatelliteDBConfiguration()
    //    {
    //        SetTransactionHandler(SqlProviderServices.ProviderInvariantName, () => new CommitFailureHandler());
    //        SetExecutionStrategy(SqlProviderServices.ProviderInvariantName, () => new SqlAzureExecutionStrategy());
    //    }


    //}
    //[DbConfigurationType(typeof(SatelliteDBConfiguration))]
    public partial class SatelliteModel : DbContext
    {
        public SatelliteModel()
            : base("name=SatelliteModel")
        {
        }

        public SatelliteModel(string connString)
            : base(connString)
        {


        }

        public virtual DbSet<Cargowise_Enums> Cargowise_Enums { get; set; }
        public virtual DbSet<CargowiseContext> CargowiseContexts { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DTS> DTS { get; set; }
        public virtual DbSet<MapOperation> MapOperations { get; set; }
        public virtual DbSet<Mapping_Definition> Mapping_Definition { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Processing_Error> Processing_Errors { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Transaction_Log> Transaction_Logs { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<FileDescription> FileDescriptions { get; set; }
        public virtual DbSet<ToDo> ToDo { get; set; }
        public virtual DbSet<Customer_Profile> CustomerProfiles { get; set; }
        public virtual DbSet<Mapping_Enums> MapEnums { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cargowise_Enums>()
                .Property(e => e.CW_ENUMTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Cargowise_Enums>()
                .Property(e => e.CW_ENUM)
                .IsFixedLength();

            modelBuilder.Entity<Cargowise_Enums>()
                .Property(e => e.CW_MAPVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<CargowiseContext>()
                .Property(e => e.CC_Context)
                .IsUnicode(false);

            modelBuilder.Entity<CargowiseContext>()
                .Property(e => e.CC_Description)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.S_CONTACTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.S_EMAILADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.S_ALERTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_FINALPROCESSING)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_DTSTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_SEARCHPATTERN)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_NEWVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_QUALIFIER)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_TARGET)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_CURRENTVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<Mapping_Definition>()
                .Property(e => e.M_DataType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_ARTICLEID)
                .IsFixedLength();

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_PARTNUM)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_PRODUCTNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_BARCODE)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_UOM)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_CWUOM)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_CHILDARTICLE)
                .IsFixedLength();

            modelBuilder.Entity<Product>()
                .Property(e => e.PW_CHILDUOM)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_ERRORDESC)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_ERRORCODE)
                .IsFixedLength();

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_IGNORE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_REFERENCE)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Error>()
                .Property(e => e.E_REFTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_REASONCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SERVER)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DELIVERY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PORT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_LIBNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MESSAGETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_EMAILADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SSL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SENDEREMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SUBJECT)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MESSAGEDESCR)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_EVENTCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CUSTOMERCOMPANYNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_GROUPCHARGES)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_XSD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PARAMLIST)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_METHOD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_NOTIFY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_NOTIFYEMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction_Log>()
                .Property(e => e.X_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction_Log>()
                .Property(e => e.X_SUCCESS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction_Log>()
                .Property(e => e.X_LASTRESULT)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_INVOICED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF1TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF2TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF3TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FileDescription>()
                .Property(e => e.PC_FileName)
                .IsUnicode(false);

            modelBuilder.Entity<FileDescription>()
                .Property(e => e.PC_Delimiter)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FileDescription>()
                .Property(e => e.PC_FirstFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<FileDescription>()
                .Property(e => e.PC_LastFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<ToDo>()
                .Property(e => e.L_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<ToDo>()
                .Property(e => e.L_LASTRESULT)
                .IsUnicode(false);
        }
    }
}
