namespace PCFSatellite.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("FileDescription")]
    public partial class FileDescription
    {
        [Key]
        public Guid PC_ID { get; set; }

        public Guid? PC_P { get; set; }

        [StringLength(50)]
        public string PC_FileName { get; set; }

        [MaxLength(100)]
        public string PC_Description { get; set; }

        public bool? PC_HasHeader { get; set; }

        [StringLength(1)]
        public string PC_Delimiter { get; set; }

        public int? PC_Fieldcount { get; set; }

        [StringLength(50)]
        public string PC_FirstFieldName { get; set; }

        [StringLength(50)]
        public string PC_LastFieldName { get; set; }

        public bool? PC_Quotations { get; set; }

        public int? PC_HeaderStart { get; set; }

        public int? PC_DataStart { get; set; }
    }
}
