﻿using System;
using static PCFSatellite.TransReference;

namespace PCFSatellite
{
    public class ProcessingEventArgs : EventArgs
    {
        public string EventMessage { get; set; }
        public string FileName { get; set; }
        public string Reference1 { get; set; }
        public string Reference2 { get; set; }
        public RefType? RefType1 { get; set; }
        public RefType? RefType2 { get; set; }
    }
}
