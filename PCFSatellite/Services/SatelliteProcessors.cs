﻿using CTCLogging;
using NodeResources;
using PCFSatellite.Models;
using SatelliteData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using XMLLocker;
using XMLLocker.CTC;


namespace PCFSatellite.Resources
{
    public class SatelliteProcessors : ISatelliteProcessors
    {
        #region members
        private string _connString { get; set; }
        private string _appLogPath { get; set; }

        private string _notify { get; set; }
        private IMailServerSettings _mailServerSettings { get; set; }
        private string _senderId;
        private string _recipientId;
        #endregion

        #region properties
        #endregion

        #region constructors
        public SatelliteProcessors(string connString, string logPath, string notify, IMailServerSettings mserver)
        {
            _connString = connString;
            _appLogPath = logPath;
            _notify = notify;
            _mailServerSettings = mserver;

        }
        #endregion

        #region methods
        #endregion

        #region helpers
        private void LogError(string profile, string details, string process, DateTime time, string error, string path)
        {
            var appLogger = typeof(AppLogger);
            var strArgs = new Type[] { typeof(string), typeof(string), typeof(string), typeof(string), typeof(DateTime), typeof(string), typeof(string) };
            ConstructorInfo constructorAppLogger = appLogger.GetConstructor(strArgs);
            var objLog = constructorAppLogger.Invoke(new object[] { this._appLogPath, profile, details, process, time, error, path });
            var logMethod = appLogger.GetMethod("AddLog");
            var addLog = logMethod.Invoke(objLog, new object[] { });
        }
        #endregion







    }
}
