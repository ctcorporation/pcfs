﻿using NodeResources;
using PCFSatellite.Models;
using PCFSatellite.Resources;
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace PCFSatellite.Services
{
    public class FileProcessorService : IFileProcessorService
    {
        #region Members
        private string _outputDir;
        private string _custRef;
        private string _xmlFile;
        private Guid _profID;
        private Transaction _result = new Transaction();
        private string _orderNo;
        private string _connString;
        #endregion

        #region Properties
        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }

        #endregion

        #region Constructors
        public FileProcessorService(string output, string xmlFile, Guid profID)
        {
            _outputDir = output;
            _xmlFile = xmlFile;
            _profID = profID;

        }

        public FileProcessorService(string output, string xmlFile)
        {
            _outputDir = output;
            _xmlFile = xmlFile;
            _result = new Transaction();
        }

        #endregion

        #region Methods
        string CreateCWFile(XMLLocker.Cargowise.XUS.UniversalInterchange cwFile, string recipient, string sender)
        {
            try
            {
                String cwXML = Path.Combine(_outputDir, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(_outputDir, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + "-" + iFileCount + ".xml");
                }
                _orderNo = !string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.Order.OrderNumber) ? cwFile.Body.BodyField.Shipment.Order.OrderNumber : null;
                _custRef = !string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.Order.ClientReference) ? cwFile.Body.BodyField.Shipment.Order.ClientReference : null;
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                using (StringWriter writer = new StringWriter())
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange));
                    var cwNSUniversal = new XmlSerializerNamespaces();
                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                    xSer.Serialize(outputCW, cwFile, cwNSUniversal);
                    outputCW.Flush();
                    outputCW.Close();
                    return _orderNo;
                }
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                _result.T_LOGMSG = "Error Found Creating Cargowise File. " + ex.GetType().Name + ": " + m.Name + ": " + ex.Message;
                _result.T_LOGTYPE = "Error";
                LogService.LogError(recipient + "-" + sender, ex.GetType().Name, ex.Source, DateTime.Now, ex.ToLogString(ex.StackTrace), ex.TargetSite.Name);
                return string.Empty;
            }
        }

        public Transaction ProcessFile(Customer_Profile prof)
        {
            _result.T_ID = Guid.NewGuid();
            _result.T_FILENAME = _xmlFile;
            _result.T_DATETIME = DateTime.Now;
            _result.T_REF1TYPE = "WHSOrder";
            _result.T_DIRECTION = "S";
            if (prof != null)
            {
                _result.T_MSGTYPE = prof.P_MSGTYPE.Trim();
                _result.T_P = prof.P_ID;
                if (!string.IsNullOrEmpty(prof.P_METHOD))
                {
                    Type type = typeof(SatelliteProcessors);
                    var strArgs = new Type[] { typeof(string), typeof(string), typeof(string), typeof(IMailServerSettings) };
                    ConstructorInfo satConstructor = type.GetConstructor(strArgs);
                    object satObject = satConstructor.Invoke(new object[] { _connString, Globals.AppLogPath, !string.IsNullOrEmpty(prof.P_NOTIFYEMAIL) ? prof.P_NOTIFYEMAIL : "csr@ctcorp.com.au", Globals.MailServerSettings });
                    var method = type.GetMethod(prof.P_METHOD);
                    var returnObject = method.Invoke(satObject, new object[] { prof, _xmlFile });
                    if (returnObject == null)
                    {
                        _result.T_LOGMSG = "Error found processing file: " + _xmlFile + ". Missing Products found. Check Error Log for details";
                        _result.T_LOGTYPE = "Error";
                        return _result;
                    }
                    else
                    {
                        if (returnObject.GetType() == typeof(XMLLocker.Cargowise.XUS.UniversalInterchange))
                        {
                            var ordRef = CreateCWFile((XMLLocker.Cargowise.XUS.UniversalInterchange)returnObject, prof.P_RECIPIENTID, prof.P_SENDERID);
                            _result.T_REF1 = ordRef;
                            _result.T_REF2 = string.IsNullOrEmpty(_custRef) ? string.Empty : _custRef;
                            _result.T_REF2TYPE = "CustRef";
                            _result.T_LOGMSG = "File Converted Successfully";
                            _result.T_LOGTYPE = "Success";

                            return _result;
                        }
                        else
                        {
                            _result.T_LOGMSG = "Error converting data " + _xmlFile + ". Check Error logs for details.";
                            _result.T_LOGTYPE = "Error";
                            return _result;
                        }
                    }
                }
            }
            else
            {
                _result.T_LOGMSG = "Unable to process file. No Profile file found: " + _xmlFile;
                _result.T_LOGTYPE = "No Profile";
                return _result;
            }
            if (_orderNo != null)
            {
                _result.T_REF1 = _orderNo;
                _result.T_REF1TYPE = "WHSOrder";
            }
            if (_custRef != null)
            {
                _result.T_REF2 = _custRef;
                _result.T_REF2TYPE = "WHSReceipt";
            }
            return _result;
        }


        #endregion

        #region Helper

        #endregion



    }
}
