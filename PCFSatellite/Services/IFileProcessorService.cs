﻿using PCFSatellite.Models;

namespace PCFSatellite.Services
{
    public interface IFileProcessorService
    {
        string ConnString { get; set; }
        Transaction ProcessFile(Customer_Profile prof);
    }
}