﻿using CTCLogging;
using System;
using System.Reflection;

namespace PCFSatellite.Services
{
    public static class LogService
    {
        public static void LogError(string profile, string details, string process, DateTime time, string error, string path)
        {
            var appLogger = typeof(AppLogger);
            var strArgs = new Type[] { typeof(string), typeof(string), typeof(string), typeof(string), typeof(DateTime), typeof(string), typeof(string) };
            ConstructorInfo constructorAppLogger = appLogger.GetConstructor(strArgs);
            var objLog = constructorAppLogger.Invoke(new object[] { Globals.AppLogPath, profile, details, process, time, error, path });
            var logMethod = appLogger.GetMethod("AddLog");
            var addLog = logMethod.Invoke(objLog, new object[] { });
        }
    }
}
