﻿//using NodeResources;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml.Serialization;
using XUS;

namespace PCFSatellite
{
    class nfsa
    {
        public static Tuple<Guid, TransReference> DespatchOrder(string fileName, Models.Customer_Profile cust)
        {
            Guid result = new Guid();
            TransReference resTrans = new TransReference();
            FileInfo fi = new FileInfo(fileName);
            DataTable dt = NodeResources.ConvertCSVtoDataTable(fi.FullName);
            DataSet dsPCFS = new DataSet();
            dsPCFS.Tables.Add(dt);

            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            //  File.Delete(fi.FullName);
            TransReference tRef = new TransReference();

            string xmlPath = Path.GetDirectoryName(Path.GetDirectoryName(fi.FullName));

            if (Directory.Exists(xmlPath))
            {
                {
                    try
                    {
                        XUS.UniversalInterchange interchange = new XUS.UniversalInterchange();
                        XUS.UniversalInterchangeHeader header = new XUS.UniversalInterchangeHeader();
                        header.SenderID = cust.P_SENDERID;
                        header.RecipientID = cust.P_RECIPIENTID;
                        interchange.Header = header;
                        interchange.version = "1.1";
                        XUS.UniversalInterchangeBody body = new XUS.UniversalInterchangeBody();
                        XUS.UniversalShipmentData bodydata = new XUS.UniversalShipmentData();
                        List<XUS.UniversalShipmentData> PCFSOrderFile = new List<XUS.UniversalShipmentData>();
                        XUS.UniversalShipmentData PCFSOrder = new XUS.UniversalShipmentData();
                        PCFSOrder.version = "1.1";                        
                        XUS.Shipment PCFSShipment = new XUS.Shipment();

                        XUS.DataContext dc = new XUS.DataContext();
                        List<XUS.DataTarget> dsColl = new List<XUS.DataTarget>();
                        XUS.DataTarget ds = new XUS.DataTarget();
                        ds.Type = "WarehouseOrder"; // Need to revisit this - Not to hardocode                       
                        ds.Key = "";
                        dsColl.Add(ds);
                        dc.DataTargetCollection = dsColl.ToArray();
                        //PCFSShipment.DataContext = dc;
                        XUS.Company xmlCompany = new XUS.Company();
                        xmlCompany.Code = "SYD";
                        XUS.Country xmlCountry = new XUS.Country();
                        xmlCountry.Code = "AU";
                        xmlCountry.Name = "Australia";
                        xmlCompany.Country = xmlCountry;
                        xmlCompany.Name = "";
                        XUS.CodeDescriptionPair actionPurpose = new XUS.CodeDescriptionPair
                        {
                            Code = "C2C",
                            Description = "CTC Node to Cargowise"
                        };
                        dc.ActionPurpose = actionPurpose;
                        XUS.CodeDescriptionPair eventType = new XUS.CodeDescriptionPair
                        {
                            Code = "DIM",
                            Description = "Data Imported"
                        };
                        dc.EventType = eventType;
                        List<XUS.RecipientRole> xmlRecipientRoleColl = new List<XUS.RecipientRole>();
                        XUS.RecipientRole xmlRecipientRole = new XUS.RecipientRole();
                        xmlRecipientRole.Code = XUS.RecipientRoleCode.WHS;  // Need to revisit this - Not to hardocode
                        xmlRecipientRoleColl.Add(xmlRecipientRole);
                        XUS.ShipmentOrder xmlOrder = new XUS.ShipmentOrder();
                        XUS.ShipmentOrderWarehouse xmlWarehouse = new XUS.ShipmentOrderWarehouse();
                        xmlOrder.Warehouse = xmlWarehouse;
                        XUS.CodeDescriptionPair orderStatus = new XUS.CodeDescriptionPair
                        {
                            Code = "ENT",
                            Description = "Entered"
                        };

                        xmlOrder.Status = orderStatus;
                        XUS.CodeDescriptionPair orderType = new XUS.CodeDescriptionPair
                        {
                            Code = "ORD",
                            Description = "ORDER"
                        };

                        xmlOrder.Type = orderType;
                        xmlOrder.ClientReference = dt.Rows[0]["CustPONumberComments"].ToString();
                        tRef.Ref1Type = TransReference.RefType.WHSPick;

                        tRef.Reference1 = dt.Rows[0]["BSOrderNo"].ToString();

                        xmlOrder.OrderNumber = dt.Rows[0]["BSOrderNo"].ToString();

                        resTrans.Ref1Type = TransReference.RefType.WHSOrder;
                        resTrans.Reference1 = xmlOrder.OrderNumber;

                        List<ShipmentOrderOrderLineCollection> xmlOrderLineColl = new List<ShipmentOrderOrderLineCollection>();
                        ShipmentOrderOrderLineCollection xmlOrderLine = new ShipmentOrderOrderLineCollection();
                        xmlOrderLine.Content = CollectionContent.Complete;
                        xmlOrderLine.ContentSpecified = true;
                        // For loop to loop through Order lines in Table. 
                        int iline = 0;
                        decimal fTotal = 0;
                        List<ShipmentOrderOrderLineCollectionOrderLine> xmlLines = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                        List<OrganizationAddress> xmlOrgColl = new List<OrganizationAddress>();
                        OrganizationAddress xmlOrg;
                        xmlOrg = new OrganizationAddress
                        {
                            AddressType = "ConsigneeAddress",
                            Address1 = dsPCFS.Tables[0].Rows[0]["StoreAddress1"].ToString().Trim(),
                            Address2 = dsPCFS.Tables[0].Rows[0]["StoreAddress2"].ToString().Trim(),
                            City = dsPCFS.Tables[0].Rows[0]["StoreCity"].ToString().Trim(),
                            AddressOverride = true,
                            Postcode = dsPCFS.Tables[0].Rows[0]["StorePostCode"].ToString().Trim(),
                            //Email = dsPCFS.Tables[0].Rows[0]["EmailAddress"].ToString().Trim(),
                            State = dsPCFS.Tables[0].Rows[0]["StoreState"].ToString().Trim(),
                            CompanyName = dsPCFS.Tables[0].Rows[0]["Store Name"].ToString(),
                            AddressShortCode = dsPCFS.Tables[0].Rows[0]["Store Name"].ToString()
                        };
                        xmlOrgColl.Add(xmlOrg);
                        xmlOrg = new OrganizationAddress
                        {
                            AddressType = "ConsignorDocumentaryAddress",
                            OrganizationCode = cust.P_SENDERID,
                            Country = new Country { Code = "AU", Name = "Australia" }

                        };
                        xmlOrgColl.Add(xmlOrg);
                        List<Date> OrderDateCollection = new List<Date>();
                        DateTime ODate = new DateTime();
                        if (!DateTime.TryParse(dsPCFS.Tables[0].Rows[0]["OrderDate"].ToString().Trim(), out ODate))
                        {
                            ODate = DateTime.Now.AddDays(1);
                        }
                        else
                        {
                            TimeSpan processBy = new TimeSpan(13, 0, 0);
                            TimeSpan currentTime = DateTime.Now.TimeOfDay;
                            if (currentTime > processBy)
                            {
                                ODate = DateTime.Now.Date.AddDays(1);
                            }

                        }
                        Date OrderDate = new Date
                        {
                            IsEstimate = false,
                            IsEstimateSpecified = true,
                            Type = DateType.OrderDate,
                            Value = DateTime.Now.ToString("yyyy-MM-ddThh:mm.ss")
                        };
                        OrderDateCollection.Add(OrderDate);

                        OrderDate = new Date
                        {
                            IsEstimate = false,
                            IsEstimateSpecified = true,
                            Type = DateType.DeliveryRequiredBy,
                            Value = ODate.ToString("yyyy-MM-ddThh:mm.ss")
                        };

                        OrderDateCollection.Add(OrderDate);
                        xmlOrder.DateCollection = OrderDateCollection.ToArray();

                        foreach (DataRow dr in dsPCFS.Tables[0].Rows)
                        {

                            if (decimal.Parse(dr["QtyOrdered"].ToString()) > 0)
                            {
                                iline++;
                                ShipmentOrderOrderLineCollectionOrderLine xmlLine = new ShipmentOrderOrderLineCollectionOrderLine();
                                xmlLine.LineNumber = iline;
                                xmlLine.OrderedQty = decimal.Parse(dr["QtyOrdered"].ToString());
                                xmlLine.PackageQty = decimal.Parse(dr["QtyOrdered"].ToString());
                                xmlLine.PackageQtySpecified = true;
                                xmlLine.OrderedQtySpecified = true;
                                fTotal += decimal.Parse(dr["QtyOrdered"].ToString());

                                Product product = new Product();
                                product.Code = dr["SKU"].ToString();
                                product.Description = dr["Sku Description"].ToString();
                                xmlLine.Product = product;
                                xmlLine.RequiredBy = ODate.ToString("yyyy-MM-ddThh:mm.ss");
                                xmlLines.Add(xmlLine);
                            }

                        }
                        xmlOrder.TotalUnits = fTotal;
                        xmlOrder.TotalUnitsSpecified = true;
                        xmlOrderLine.OrderLine = xmlLines.ToArray();
                        xmlOrder.OrderLineCollection = xmlOrderLine;
                        PCFSShipment.DataContext = dc;
                        PCFSShipment.Order = xmlOrder;
                        PCFSShipment.OrganizationAddressCollection = xmlOrgColl.ToArray();
                        bodydata.Shipment = PCFSShipment;
                        bodydata.version = "1.1";
                        body.BodyField = bodydata;
                        interchange.Body = body;
                        String cwXML = Path.Combine(Globals.glOutputDir, cust.P_SENDERID + "ORD" + xmlOrder.OrderNumber + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(Globals.glOutputDir, cust.P_SENDERID + "ORD" + xmlOrder.OrderNumber + iFileCount + ".xml");
                        }
                        Stream outputCW = File.Open(cwXML, FileMode.Create);
                        resTrans.Ref2Type = TransReference.RefType.Shipment;
                        resTrans.Reference2 = Path.GetFileName(cwXML);
                        StringWriter writer = new StringWriter();
                        XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                        xSer.Serialize(outputCW, interchange);
                        outputCW.Flush();
                        outputCW.Close();

                    }
                    catch (Exception ex)
                    {
                        resTrans.Ref3Type = TransReference.RefType.Error;
                        resTrans.Reference3 = ex.GetType().Name + ": " + ex.Message;
                        //MailModule mailModule = new MailModule(Globals.MailServerSettings);
                        //mailModule.SendMsg(fileName, Globals.glAlertsTo, cust.C_CODE + ": Untrapped Error Message", ex.Message);

                    }

                }
            }

            return new Tuple<Guid, TransReference>(result, resTrans);
        }

    }
}
