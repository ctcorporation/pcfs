﻿
using PCFSatellite.Models;
using System;

namespace PCFSatellite.Classes
{
    public class SatelliteLogs
    {
        #region members
        string _connString;
        string _err;
        #endregion
        #region properties
        public string ErrMsg
        {
            get
            { return _err; }
            set
            { _err = value; }
        }
        #endregion
        #region constructors
        public SatelliteLogs(string connString)
        {
            _connString = connString;


        }
        #endregion
        #region methods
        public void AddTransaction(string fileName, string archive, string ref1, string ref1Type, string ref2, string ref2Type, string logType, string msg)
        {
            using (IUnitOfWork uow = new DTO.UnitOfWork(new Models.SatelliteModel(_connString)))
            {
                var transaction = new Transaction
                {
                    T_ARCHIVE = archive,
                    T_DATETIME = DateTime.Now,
                    T_FILENAME = fileName,
                    T_REF1 = ref1,
                    T_REF1TYPE = ref1Type,
                    T_REF2 = ref2,
                    T_REF2TYPE = ref2Type,
                    T_LOGMSG = msg,
                    T_LOGTYPE = logType

                };
                uow.Transactions.Add(transaction);
                uow.Complete();
            };
        }
        #endregion
        #region helpers
        #endregion


    }
}
