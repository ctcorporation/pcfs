﻿using PCFSatellite.Models;
using PCFSatellite.Repository;
using System;
using System.Data.Entity.Validation;

namespace PCFSatellite.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SatelliteModel _context;
        private bool disposed = false;
        private string _errorMessage = string.Empty;

        public UnitOfWork(SatelliteModel context)
        {
            _context = context;
            AddRepos();
        }

        public void AddRepos()
        {
            Customers = new CustomerRepository(_context);
            Profiles = new ProfileRepository(_context);
            MapOperations = new MapOperationRepository(_context);
            CargowiseEnums = new Cargowise_EnumsRepository(_context);
            Transactions = new TransactionRepository(_context);
            CustomerProfiles = new CustomerProfileRepository(_context);
            DTSs = new DTSRepository(_context);
            CargowiseContexts = new CargowiseContextRepository(_context);
            FileDescriptions = new FileDescriptionRepository(_context);
            Products = new ProductRepository(_context);
            MapEnums = new MapEnumRepository(_context);
        }

        public IProductRepository Products
        {
            get;
            private set;
        }

        public IFileDescriptionRepository FileDescriptions
        {
            get;
            private set;
        }
        public ICargowiseContextRepository CargowiseContexts
        {
            get;
            private set;
        }
        public IDTSRepository DTSs
        {
            get;
            private set;
        }
        public ICustomerProfileRepository CustomerProfiles
        {
            get;
            private set;
        }

        public IMapOperationRepository MapOperations
        {
            get;
            private set;
        }

        public ICustProfileRepository CustProfiles
        {
            get;
            private set;
        }
        public ITransactionRepository Transactions
        {
            get;
            private set;
        }

        public ICargowise_EnumsRepository CargowiseEnums
        {
            get;
            private set;
        }

        public IProfileRepository Profiles
        {
            get;
            private set;
        }

        public ICustomerRepository Customers
        {
            get;
            private set;
        }

        public IMapEnumRepository MapEnums
        {
            get;
            private set;
        }


        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(_errorMessage, dbEx);

            }
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    }
}
