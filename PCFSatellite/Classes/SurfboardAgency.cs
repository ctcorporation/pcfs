﻿using PCFSatellite.CIN7;
using PCFSatellite.Classes;
using PCFSatellite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using XUS;

namespace PCFSatellite.SurfAgency

{


    public class SurfboardAgency
    {
        #region Members
        public EventHandler<ProcessingEventArgs> ProcessedImport;

        public EventHandler<HeartBeatArgs> HeartBeat;
        private readonly string _connString;
        #endregion

        #region Properties

        #endregion

        #region Constructors
        public SurfboardAgency(string connString)
        {
            _connString = connString;
        }
        #endregion

        #region Methods
        public virtual void OnProcessedError(string msg, string fileName, string reference1, string reference2, TransReference.RefType refType1, TransReference.RefType refType2)
        {
            ProcessedImport?.Invoke(this, new ProcessingEventArgs
            {
                EventMessage = msg,
                FileName = fileName,
                Reference1 = reference1,
                Reference2 = reference2,
                RefType1 = refType1,
                RefType2 = refType2
            });
        }
        public TransReference GeneratePickResponse(Shipment cwShipment, Customer_Profile cust)
        {
            var result = new TransReference();
            try
            {
                String senderID = cust.P_SENDERID;
                DataContext dc = new DataContext();
                dc = cwShipment.DataContext;
                DataSource[] dscoll;
                dscoll = dc.DataSourceCollection;
                string activityReply = string.Empty;
                string methodReply = string.Empty;

                string shipmentNo = string.Empty;
                var orderNo = (from d in dscoll
                               where d.Type == "WarehouseOrder"
                               select d.Key).FirstOrDefault();


                if (!string.IsNullOrEmpty(orderNo.ToString()))
                {

                    OrderConfirmationsOrder orderConfirmationsOrder = new OrderConfirmationsOrder();
                    orderConfirmationsOrder.OrderReference = cwShipment.Order.OrderNumber;
                    var org = cwShipment.OrganizationAddressCollection.FirstOrDefault(o => o.AddressType == "TransportCompanyDocumentaryAddress");
                    orderConfirmationsOrder.Carrier = org == null ? string.Empty : org.CompanyName;
                    orderConfirmationsOrder.TrackingReference = cwShipment.Order.TransportReference;
                    List<OrderConfirmationsOrderOrderSkusOrderSku> orderSkus = new List<OrderConfirmationsOrderOrderSkusOrderSku>();

                    foreach (ShipmentOrderOrderLineCollectionOrderLine line in cwShipment.Order.OrderLineCollection.OrderLine)
                    {
                        OrderConfirmationsOrderOrderSkusOrderSku orderSku = new OrderConfirmationsOrderOrderSkusOrderSku
                        {
                            Sku = line.Product.Code,
                            QuantityShipped = line.PackageQty.ToString()

                        };
                        orderSkus.Add(orderSku);

                    }
                    orderConfirmationsOrder.OrderSkus = orderSkus.ToArray();
                    List<OrderConfirmationsOrder> orderConfirmationCollection = new List<OrderConfirmationsOrder>();
                    orderConfirmationCollection.Add(orderConfirmationsOrder);
                    OrderConfirmations pickConfirmation = new OrderConfirmations
                    {
                        Items = orderConfirmationCollection.ToArray()
                    };
                    String cwXML = Path.Combine(Globals.glOutputDir, "SO-PICK-" + cust.P_RECIPIENTID + orderNo.ToString().Trim() + ".xml");
                    int iFileCount = 0;
                    while (File.Exists(cwXML))
                    {
                        iFileCount++;
                        cwXML = Path.Combine(Globals.glOutputDir, "SO-PICK-" + cust.P_RECIPIENTID + orderNo.ToString().Trim() + "-" + iFileCount + ".xml");
                    }
                    Stream outputGLO = File.Open(cwXML, FileMode.Create);
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(OrderConfirmations));
                    var gloNS = new XmlSerializerNamespaces();
                    gloNS.Add("", "");
                    xSer.Serialize(outputGLO, pickConfirmation, gloNS);
                    outputGLO.Flush();
                    outputGLO.Close();
                    string archiveFile = Globals.ArchiveFile(Globals.glArcLocation, cwXML);
                    var log = new SatelliteLogs(_connString);
                    //  NodeResources.AddTransaction(NodeResources.GetCustID(Globals.glCustCode), Guid.Empty, xmlFile, "R", true, result, archiveFile);

                    log.AddTransaction(cwXML, archiveFile, orderNo, orderConfirmationsOrder.OrderReference, "WHSOrder", "CustRef", "Success", cust.P_RECIPIENTID + " Pick Response Generated");
                    result.Ref1Type = TransReference.RefType.WHSOrder;
                    result.Reference1 = orderNo;
                    result.Reference2 = orderConfirmationsOrder.OrderReference;
                    result.Ref2Type = TransReference.RefType.Custom;
                }
            }
            catch (Exception ex)
            {
                var log = new SatelliteLogs(Globals.glConnString);
                log.AddTransaction(string.Empty, string.Empty, cwShipment.Order.OrderNumber, string.Empty, "WHSOrder", string.Empty, "Error", ex.GetType().Name + ": (" + ex.Source + ") " + ex.Message + "." + Environment.NewLine + ex.ToLogString(ex.StackTrace));
                return null;
            }
            return result;
        }
        public void ConvertPickOrder(PCFSatellite.CIN7.Orders orders, string fileName, Customer_Profile cust)
        {
            //SqlConnection sqlConn = new SqlConnection { ConnectionString = Globals.connString() };
            //TransReference result = new TransReference();
            //const string dateFormat = "yyyyMMddHHmmss";
            //if (orders.Items != null)
            //{
            //    OnprocessingImport(cust.C_code, string.Format("{0:g} ", DateTime.Now) + " Now processing Surboard Agency Pick Order");
            //    SqlCommand addPCFSSo = new SqlCommand();
            //    addPCFSSo.Connection = sqlConn;
            //    addPCFSSo.CommandText = "AddPCFS_SO";
            //    addPCFSSo.CommandType = CommandType.StoredProcedure;
            //    SqlParameter orderNo = addPCFSSo.Parameters.Add("@ORDERNO", SqlDbType.NChar, 20);
            //    SqlParameter so_orderdate = addPCFSSo.Parameters.Add("@SO_ORDERDATE", SqlDbType.DateTime);
            //    SqlParameter so_shipdate = addPCFSSo.Parameters.Add("@SO_SHIPDATE", SqlDbType.DateTime);
            //    SqlParameter so_customer = addPCFSSo.Parameters.Add("@SO_CUSTOMER", SqlDbType.VarChar, 100);
            //    SqlParameter so_delstreet1 = addPCFSSo.Parameters.Add("@SO_DELSTREET1", SqlDbType.VarChar, 100);
            //    SqlParameter so_delstreet2 = addPCFSSo.Parameters.Add("@SO_DELSTREET2", SqlDbType.VarChar, 100);
            //    SqlParameter so_delpocode = addPCFSSo.Parameters.Add("@SO_DELPOCODE", SqlDbType.Char, 4);
            //    SqlParameter so_delsuburb = addPCFSSo.Parameters.Add("@SO_DELSUBURB", SqlDbType.VarChar, 100);
            //    SqlParameter so_delstate = addPCFSSo.Parameters.Add("@SO_DELSTATE", SqlDbType.VarChar, 20);
            //    SqlParameter so_deliveryNotes = addPCFSSo.Parameters.Add("@SO_DELIVERYNOTES", SqlDbType.VarChar, -1);
            //    SqlParameter so_pickingNotes = addPCFSSo.Parameters.Add("@SO_PICKINGNOTES", SqlDbType.VarChar, -1);
            //    SqlParameter so_ph = addPCFSSo.Parameters.Add("@SO_PH", SqlDbType.VarChar, 20);
            //    SqlParameter so_company = addPCFSSo.Parameters.Add("@SO_COMPANY", SqlDbType.VarChar, 100);
            //    SqlParameter so_id = addPCFSSo.Parameters.Add("@SO_ID", SqlDbType.UniqueIdentifier);
            //    SqlParameter so_deliveryEntity = addPCFSSo.Parameters.Add("@SO_DELIVERYENTITY", SqlDbType.VarChar, 100);
            //    SqlParameter so_OrgCode = addPCFSSo.Parameters.Add("@SO_ORGCODE", SqlDbType.Char, 15);
            //    so_id.Direction = ParameterDirection.Output;
            //    SqlCommand addPCFSSOL = new SqlCommand("AddPCFS_SOLine", sqlConn);
            //    addPCFSSOL.CommandType = CommandType.StoredProcedure;
            //    SqlParameter sol_id = addPCFSSOL.Parameters.Add("@SO_ID", SqlDbType.UniqueIdentifier);
            //    SqlParameter soqty = addPCFSSOL.Parameters.Add("@QTY", SqlDbType.Int);
            //    SqlParameter itemBarcode = addPCFSSOL.Parameters.Add("@ITEMBARCODE", SqlDbType.VarChar, 20);
            //    SqlParameter lineNo = addPCFSSOL.Parameters.Add("@LINENO", SqlDbType.Int);
            //    SqlParameter partNo = addPCFSSOL.Parameters.Add("@PARTNUM", SqlDbType.VarChar, 50);
            //    SqlParameter productName = addPCFSSOL.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar, 100);
            //    string tempNotes = string.Empty;
            //    foreach (CIN7.OrdersOrder order in orders.Items)
            //    {
            //        if (string.IsNullOrEmpty(order.WarehouseID))
            //        {
            //            result.Ref3Type = TransReference.RefType.Error;
            //            result.Reference3 = "Missing Customer code.";
            //        }
            //        else
            //        {
            //            try
            //            {
            //                orderNo.Value = string.IsNullOrEmpty(order.Reference) ? order.Reference : string.Empty;
            //                result.Ref1Type = TransReference.RefType.Order;
            //                result.Reference1 = orderNo.Value.ToString();
            //                DateTime date = new DateTime();
            //                if (DateTime.TryParseExact(order.CreatedDate, dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out date))
            //                {
            //                    so_orderdate.Value = date;
            //                }
            //                so_OrgCode.Value = order.WarehouseID;
            //                foreach (CIN7.OrdersOrderDestination customer in order.Destination)
            //                {
            //                    so_customer.Value = customer.ShipToName;
            //                    if (!string.IsNullOrEmpty(customer.Address1))
            //                    {
            //                        so_delstreet1.Value = customer.Address1;
            //                        so_delstreet2.Value = customer.Address2;
            //                        if (!string.IsNullOrEmpty(customer.Postcode))
            //                        {
            //                            so_delpocode.Value = customer.Postcode;
            //                        }
            //                        else
            //                        {
            //                            throw new Exception();
            //                        }
            //                        so_company.Value = customer?.Company;
            //                        so_delsuburb.Value = customer?.City;
            //                        so_delstate.Value = customer?.State;
            //                    }
            //                }

            //            }
            //            catch (Exception ex)
            //            {
            //                OnprocessingImport(cust.C_code, string.Format("{0:g} ", DateTime.Now) + "Skipping order - missing Delivery Address");
            //                string errMsg = "Unabled to Process Order: " + orderNo.Value + Environment.NewLine;
            //                errMsg += "Error Message: " + ex.GetType().Name + ": " + ex.Message;
            //                errMsg += "Please fix and resubmit the order again.";
            //                string notify = cust.P_NotifyEmail;
            //                OnProcessedError(errMsg, fileName, orderNo.Value.ToString(), string.Empty, RefType.Order, RefType.None);
            //                MailModule.sendMsg(fileName, notify, "Unable to process Order: " + orderNo.Value, errMsg);
            //                NodeResources.MoveFile(fileName, Globals.glFailPath);
            //                result = new TransReference();
            //                result.Ref3Type = TransReference.RefType.Error;
            //                result.Reference3 = "Error importing Order";
            //                result.Ref2Type = TransReference.RefType.Order;
            //                result.Reference2 = orderNo.Value.ToString();
            //                // return result;
            //            }
            //        }
            //        DateTime dt = new DateTime();
            //        if (DateTime.TryParseExact(order.ShipDate, dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
            //        {
            //            so_shipdate.Value = dt;
            //        }
            //        try
            //        {
            //            if (sqlConn.State == ConnectionState.Open)
            //            {
            //                sqlConn.Close();
            //            }
            //            sqlConn.Open();
            //            addPCFSSo.ExecuteNonQuery();
            //        }
            //        catch (Exception ex)
            //        {
            //            string strEx = ex.GetType().Name;
            //            string errMsg = "Error found when processing " + fileName + Environment.NewLine;
            //            result = new TransReference();
            //            errMsg += "Error Type: " + strEx + Environment.NewLine;
            //            errMsg += "Message: " + ex.Message + Environment.NewLine;
            //            errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
            //            errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
            //            errMsg += "Source: " + ex.Source;
            //            OnProcessedError(string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: " + strEx + ex.Message + ". " + "", fileName, orderNo.Value.ToString(), string.Empty, RefType.Order, RefType.None);
            //            MailModule.sendMsg(fileName, Globals.glAlertsTo, "PCFS Unhandled Exception: ProcessCustomXML", errMsg);
            //            result.Ref3Type = TransReference.RefType.Error;
            //            result.Reference3 = "Error importing Order";
            //            result.Ref2Type = TransReference.RefType.Order;
            //            result.Reference2 = orderNo.Value.ToString();
            //        }
            //        sol_id.Value = so_id.Value;
            //        int iLineNo = 1;
            //        foreach (CIN7.OrdersOrderOrderSkusOrderSku line in order.OrderSkus)
            //        {
            //            lineNo.Value = iLineNo;
            //            itemBarcode.Value = line.Barcode;
            //            partNo.Value = line.Sku;
            //            soqty.Value = line.Quantity;
            //            if (sqlConn.State == ConnectionState.Open)
            //            {
            //                sqlConn.Close();
            //            }
            //            sqlConn.Open();
            //            addPCFSSOL.ExecuteNonQuery();
            //        }
            //        Globals.ArchiveFile(Globals.glArcLocation, fileName);
            //        CargowiseOrders cargowiseOrders = new CargowiseOrders();

            //        result = cargowiseOrders.CreateCWSOXML((Guid)so_id.Value);
            //    }
            //}


            // return result;
        }

        public NodeFile CreateOrder(CIN7.Orders orders, string fileName, Customer_Profile cust)
        {
            var common = new NodeFile();
            const string dateFormat = "yyyyMMddHHmmss";
            if (orders.Items != null)
            {
                string tempNotes = string.Empty;
                var header = new NodeFileIdendtityMatrix();
                header.CustomerId = cust.P_RECIPIENTID;
                header.SenderId = cust.P_SENDERID;
                header.FileDateTime = DateTime.Now.ToString(dateFormat);
                header.DocumentType = "WarehouseOrder";
                header.OriginalFileName = fileName;
                header.PurposeCode = "IMP";
                common.IdendtityMatrix = header;
                foreach (CIN7.OrdersOrder order in orders.Items)
                {
                    var commonOrder = new NodeFileWarehouseOrder();

                    commonOrder.OrderNumber = !string.IsNullOrEmpty(order.Reference) ? order.Reference : string.Empty;
                    //old Hard coded Warehouse
                    // commonOrder.WarehouseCode = "SYD";
                    commonOrder.WarehouseCode = !string.IsNullOrEmpty(order.WarehouseID)? order.WarehouseID: "SYD";
                    commonOrder.ShipVia = order.WarehouseID;
                    DateTime date = new DateTime();
                    var orderDates = new List<DateElement>();
                    if (DateTime.TryParseExact(order.CreatedDate, dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out date))
                    {
                        var _createDate = new DateElement();
                        _createDate.DateType = DateElementDateType.Ordered;
                        _createDate.ActualDate = date.ToString("dd-MMM-yyyy HH:mm");
                        orderDates.Add(_createDate);
                    }
                    if (DateTime.TryParseExact(order.ShipDate, dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out date))
                    {
                        var _shipDate = new DateElement();
                        _shipDate.DateType = DateElementDateType.DeliverBy;
                        _shipDate.ActualDate = date.ToString("dd-MMM-yyyy HH:mm");
                        orderDates.Add(_shipDate);
                    }
                    commonOrder.Dates = orderDates.ToArray();
                    var addresses = new List<CompanyElement>();

                    var customer = (from x in order.Destination select x).FirstOrDefault();
                    var consignor = new CompanyElement
                    {
                        CompanyType = CompanyElementCompanyType.Consignor,
                        CompanyCode = cust.P_SENDERID
                    };
                    var deliverTo = new CompanyElement
                    {
                        CompanyName = customer.ShipToName,
                        Address1 = customer.Address1,
                        Address2 = customer.Address2,
                        PostCode = customer.Postcode,
                        State = customer.State,
                        City = customer.City,
                        PhoneNo = customer.Phone,
                        CompanyType = CompanyElementCompanyType.DeliveryAddress

                    };
                    commonOrder.Companies = new List<CompanyElement> { consignor, deliverTo }.ToArray();


                    int iLineNo = 1;
                    var orderLines = new List<OrderLineElement>();
                    foreach (CIN7.OrdersOrderOrderSkusOrderSku line in order.OrderSkus)
                    {
                        var oline = new OrderLineElement
                        {
                            LineNo = iLineNo,
                            LineNoSpecified = true,
                            Product = new ProductElement
                            {
                                Barcode = line.Barcode,
                                Code = line.Sku

                            },
                            PackageQty = decimal.Parse(line.Quantity),
                            PackageQtySpecified = true,
                            PackageUnit = "EA"

                        };
                        orderLines.Add(oline);
                        iLineNo++;
                    }
                    commonOrder.OrderLines = orderLines.ToArray();
                    common.WarehouseOrders = new List<NodeFileWarehouseOrder> { commonOrder }.ToArray();
                }
                return common;
            }
            return null;
        }
        #endregion

        #region Helpers


        public virtual void OnprocessingImport(string custCode, string message)
        {
            HeartBeat?.Invoke(this, new HeartBeatArgs
            {
                CustCode = custCode,
                Message = message
            });
        }
        #endregion









    }
}
