﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PCFSatellite
{

    public class PhytoTherapy
    {
        #region members
        private readonly string dateformat = "yyyyMMddHHmmss";
        Models.Customer_Profile _cust;
        #endregion
        #region properties
        #endregion
        #region constructors
        public PhytoTherapy(Models.Customer_Profile cust)
        {
            _cust = cust;
        }
        #endregion
        #region methods
        public NodeFile DespatchOrder(string fileName)
        {
            DataTable dt = NodeResources.ConvertCSVtoDataTable(fileName);
            DataSet dsPCFS = new DataSet();
            dsPCFS.Tables.Add(dt);
            string archive = Globals.ArchiveFile(Globals.glArcLocation, fileName);
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            // File.Delete(fileName);
            Guid result = new Guid();
            TransReference resTrans = new TransReference();
            NodeFile common = new NodeFile();
            string tempNotes = string.Empty;

            if (dt.Columns.Count == 11)
            {

                common.IdendtityMatrix = GetHeader();
                string markerDespatch = string.Empty;
                int irow = 0;
                DataRow dr = dt.Rows[0];
                bool bValid = false;
                for (int rowcount = 0; rowcount < dt.Rows.Count; rowcount++)
                {
                    dr = dt.Rows[rowcount];
                    rowcount++;
                    markerDespatch = dr[0].ToString().Replace("\"", string.Empty);
                    if (markerDespatch == "Despatch Note No:")
                    {
                        irow = rowcount;
                        bValid = true;
                        break;
                    }
                }
                var commonOrder = new NodeFileWarehouseOrder();
                if (bValid)
                {
                    commonOrder.OrderNumber = dr[1].ToString().Replace("\"", string.Empty);
                    resTrans.Ref1Type = TransReference.RefType.Order;
                    resTrans.Reference1 = commonOrder.OrderNumber;
                    DateTime dOrderDate;
                    int iAddressEnd = 0;

                    for (int i = 0; i < 8; i++)
                    {
                        if (dt.Rows[i][0].ToString().Replace("\"", string.Empty).Contains("*****"))
                        {
                            iAddressEnd = i - 1;
                            break;
                        }
                    }
                    var delCompany = new CompanyElement();
                    delCompany.CompanyName = dt.Rows[0][0].ToString().Replace("\"", string.Empty);

                    if (iAddressEnd > 4)
                    {
                        delCompany.CompanyName = dt.Rows[1][0].ToString().Replace("\"", string.Empty);
                        delCompany.Address1 = dt.Rows[2][0].ToString().Replace("\"", string.Empty);
                        delCompany.Address2 = dt.Rows[3][0].ToString().Replace("\"", string.Empty);

                    }
                    else
                    {
                        delCompany.Address1 = dt.Rows[1][0].ToString().Replace("\"", string.Empty);
                    }
                    string[] substate = dt.Rows[iAddressEnd][0].ToString().Replace("\"", string.Empty).Split(' ');
                    if (substate.Length == 3)
                    {
                        delCompany.City = substate[0];
                        delCompany.State = substate[1];
                        delCompany.PostCode = substate[2];

                    }
                    string suburb = string.Empty;
                    if (substate.Length > 3)
                    {
                        delCompany.PostCode = substate[substate.Length - 1];
                        delCompany.State = substate[substate.Length - 2];
                        for (int i = 0; i < substate.Length - 2; i++)
                        {
                            suburb += substate[i] + " ";
                        }
                        delCompany.City = suburb;
                    }
                    var companies = new CompanyElement[] { delCompany };
                    commonOrder.Companies = companies;
                    var dates = new List<DateElement>();
                    if (DateTime.TryParse(dr[5].ToString().Replace("\"", string.Empty), out dOrderDate))
                    {
                        var de = new DateElement
                        {
                            ActualDate = dOrderDate.ToString(dateformat),
                            DateType = DateElementDateType.Ordered
                        };
                        dates.Add(de);
                    }
                    DateTime dShipDate;
                    if (DateTime.TryParse(dt.Rows[irow][1].ToString().Replace("\"", string.Empty), out dShipDate))
                    {
                        var de = new DateElement
                        {
                            ActualDate = dShipDate.ToString(dateformat),
                            DateType = DateElementDateType.DeliverBy
                        };
                        dates.Add(de);
                    }
                    commonOrder.Dates = dates.ToArray();
                    int iLineno = 0;
                    for (int i = irow; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i][0].ToString().Replace("\"", string.Empty) == "PRODUCT")
                        {
                            irow = i;
                        }
                    }
                    var orderLines = new List<OrderLineElement>();
                    for (int i = irow + 1; i < dt.Rows.Count; i++)
                    {
                        var oline = new OrderLineElement();

                        if (!string.IsNullOrEmpty(dt.Rows[i][0].ToString().Replace("\"", string.Empty)))
                        {
                            if (!dt.Rows[i][2].ToString().Contains("Freight"))
                            {
                                iLineno++;
                                oline.LineNo = iLineno;
                                var sUom = NodeResources.GetEnum("UOM", dt.Rows[i][5].ToString().Replace("\"", string.Empty).ToUpper());
                                oline.Product = new ProductElement
                                {
                                    Code = dt.Rows[i][0].ToString().Replace("\"", string.Empty),
                                    Description = dt.Rows[i][2].ToString().Replace("\"", string.Empty),
                                    UOM = string.IsNullOrEmpty(sUom) ? "CTN" : sUom,
                                };
                                decimal iqty;
                                oline.OrderQty = decimal.TryParse(dt.Rows[i][4].ToString().Replace("\"", string.Empty), out iqty) ? (int)iqty : 0;
                                oline.OrderQtySpecified = true;
                            }

                        }
                        orderLines.Add(oline);
                    }
                    commonOrder.OrderLines = orderLines.ToArray();
                    common.WarehouseOrders = new NodeFileWarehouseOrder[] { commonOrder };

                }
                else
                {

                }

            }
            return common;
        }

        public NodeFile TransferOrder(string fileName)
        {
            DataTable dt;
            using (IUnitOfWork uow = new DTO.UnitOfWork(new Models.SatelliteModel(Globals.glConnString)))
            {
                var fieldDesc = uow.FileDescriptions.Find(x => x.PC_P == _cust.P_ID).FirstOrDefault();
                if (fieldDesc == null)
                {
                    return null;
                }
                dt = NodeResources.ConvertCSVtoDataTable(fileName, (int)fieldDesc.PC_HeaderStart, (int)fieldDesc.PC_DataStart);
            }
            var common = new NodeFile();
            DataSet dsPCFS = new DataSet();
            dsPCFS.Tables.Add(dt);
            string archive = Globals.ArchiveFile(Globals.glArcLocation, fileName);
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            // File.Delete(fileName);
            Guid result = new Guid();
            TransReference resTrans = new TransReference();
            if (dt.Columns.Count == 10)
            {
                common.IdendtityMatrix = GetHeader();
                DataRow dr = dt.Rows[9];
                var commonOrder = new NodeFileWarehouseOrder();
                commonOrder.OrderNumber = string.Format("ST{0:yyMMddhhmm}", DateTime.Now);
                resTrans.Ref1Type = TransReference.RefType.Order;
                resTrans.Reference1 = commonOrder.OrderNumber;
                int iAddressEnd = 0;
                for (int i = 0; i < 8; i++)
                {
                    if (dt.Rows[i][0].ToString().Replace("\"", string.Empty).Contains("*****"))
                    {
                        iAddressEnd = i - 1;
                        break;
                    }
                }

                var delCompany = new CompanyElement();
                delCompany.CompanyName = dt.Rows[0][0].ToString().Replace("\"", string.Empty);

                if (iAddressEnd > 4)
                {
                    delCompany.CompanyName = dt.Rows[1][0].ToString().Replace("\"", string.Empty);
                    delCompany.Address1 = dt.Rows[2][0].ToString().Replace("\"", string.Empty);
                    delCompany.Address2 = dt.Rows[3][0].ToString().Replace("\"", string.Empty);

                }
                else
                {
                    delCompany.Address1 = dt.Rows[1][0].ToString().Replace("\"", string.Empty);
                }
                string[] substate = dt.Rows[iAddressEnd][0].ToString().Replace("\"", string.Empty).Split(' ');
                if (substate.Length == 3)
                {
                    delCompany.City = substate[0];
                    delCompany.State = substate[1];
                    delCompany.PostCode = substate[2];

                }
                string suburb = string.Empty;
                if (substate.Length > 3)
                {
                    delCompany.PostCode = substate[substate.Length - 1];
                    delCompany.State = substate[substate.Length - 2];
                    for (int i = 0; i < substate.Length - 2; i++)
                    {
                        suburb += substate[i] + " ";
                    }
                    delCompany.City = suburb;
                }
                var companies = new CompanyElement[] { delCompany };
                commonOrder.Companies = companies;
                commonOrder.Dates = new DateElement[]
                {
                    new DateElement
                    {
                        ActualDate=DateTime.Now.ToString(dateformat),
                        DateType = DateElementDateType.Ordered
                    },
                    new DateElement
                    {
                        ActualDate = DateTime.Now.ToString(dateformat),
                        DateType = DateElementDateType.DeliverBy
                    }
                };
                int iLineno = 0;
                var orderlines = new List<OrderLineElement>();
                for (int i = 13; i < dt.Rows.Count; i++)
                {

                    if (!string.IsNullOrEmpty(dt.Rows[i][0].ToString().Replace("\"", string.Empty)))
                    {
                        var oline = new OrderLineElement();
                        iLineno++;
                        oline.LineNo = iLineno;
                        oline.Product = new ProductElement
                        {
                            Code = !string.IsNullOrEmpty(dt.Rows[i][0].ToString().Replace("\"", string.Empty)) ? dt.Rows[i][0].ToString().Replace("\"", string.Empty) : dt.Rows[i][1].ToString().Replace("\"", string.Empty),
                            Description = dt.Rows[i][2].ToString().Replace("\"", string.Empty)
                        };
                        decimal iqty;
                        oline.OrderQty = decimal.TryParse(dt.Rows[i][8].ToString().Replace("\"", string.Empty), out iqty) ? (int)iqty : 0;
                        oline.OrderQtySpecified = true;
                        orderlines.Add(oline);
                    }
                }
                commonOrder.OrderLines = orderlines.ToArray();
                common.WarehouseOrders = new NodeFileWarehouseOrder[] { commonOrder };
            }
            return common;
        }



        #endregion
        #region helpers
        private NodeFileIdendtityMatrix GetHeader()
        {
            return new NodeFileIdendtityMatrix
            {
                CustomerId = _cust.P_RECIPIENTID,
                SenderId = _cust.P_SENDERID,
                DocumentType = "WarehouseOrder",
                FileDateTime = DateTime.Now.ToString(dateformat),
                EventCode = "IMP",
                PurposeCode = "CTC"
            };
        }
        #endregion



    }


}

