﻿using NodeResources;
using PCFSatellite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml.Serialization;
using XUS;

namespace PCFSatellite
{
    public class Horizon
    {

        #region members
        #endregion

        #region Properties

        #endregion

        #region Constructor
        #endregion

        #region Methods

        #endregion

        #region Helpers

        private void GetMapping()
        {

        }
        #endregion



        public static Tuple<Guid, TransReference> DespatchOrder(string fileName, Customer_Profile cust)
        {
            Guid result = new Guid();
            TransReference resTrans = new TransReference();
            FileInfo fi = new FileInfo(fileName);
            DataTable dt = NodeResources.ConvertCSVtoDataTable(fi.FullName);
            DataSet dsPCFS = new DataSet();
            dsPCFS.Tables.Add(dt);

            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            //  File.Delete(fi.FullName);
            TransReference tRef = new TransReference();

            string xmlPath = Path.GetDirectoryName(Path.GetDirectoryName(fi.FullName));

            if (Directory.Exists(xmlPath))
            {
                {
                    try
                    {
                        UniversalInterchange interchange = new UniversalInterchange();
                        UniversalInterchangeHeader header = new UniversalInterchangeHeader();
                        header.SenderID = cust.P_SENDERID;
                        header.RecipientID = cust.P_RECIPIENTID;
                        interchange.Header = header;
                        interchange.version = "1.1";
                        UniversalInterchangeBody body = new UniversalInterchangeBody();
                        UniversalShipmentData bodydata = new UniversalShipmentData();
                        List<UniversalShipmentData> PCFSOrderFile = new List<UniversalShipmentData>();
                        UniversalShipmentData PCFSOrder = new UniversalShipmentData();
                        PCFSOrder.version = "1.1";
                        Shipment PCFSShipment = new Shipment();

                        DataContext dc = new DataContext();
                        List<DataTarget> dsColl = new List<DataTarget>();
                        DataTarget ds = new DataTarget();
                        ds.Type = "WarehouseOrder"; // Need to revisit this - Not to hardocode                       
                        ds.Key = "";
                        dsColl.Add(ds);
                        dc.DataTargetCollection = dsColl.ToArray();
                        //PCFSShipment.DataContext = dc;
                        Company xmlCompany = new Company();
                        xmlCompany.Code = "SYD";
                        Country xmlCountry = new Country();
                        xmlCountry.Code = "AU";
                        xmlCountry.Name = "Australia";
                        xmlCompany.Country = xmlCountry;
                        xmlCompany.Name = "";
                        CodeDescriptionPair actionPurpose = new CodeDescriptionPair
                        {
                            Code = "C2C",
                            Description = "CTC Node to Cargowise"
                        };
                        dc.ActionPurpose = actionPurpose;
                        CodeDescriptionPair eventType = new CodeDescriptionPair
                        {
                            Code = "DIM",
                            Description = "Data Imported"
                        };
                        dc.EventType = eventType;
                        List<RecipientRole> xmlRecipientRoleColl = new List<RecipientRole>();
                        RecipientRole xmlRecipientRole = new RecipientRole();
                        xmlRecipientRole.Code = RecipientRoleCode.WHS;  // Need to revisit this - Not to hardocode
                        xmlRecipientRoleColl.Add(xmlRecipientRole);
                        ShipmentOrder xmlOrder = new ShipmentOrder();
                        ShipmentOrderWarehouse xmlWarehouse = new ShipmentOrderWarehouse();
                        xmlOrder.Warehouse = xmlWarehouse;
                        CodeDescriptionPair orderStatus = new CodeDescriptionPair
                        {
                            Code = "ENT",
                            Description = "Entered"
                        };

                        xmlOrder.Status = orderStatus;
                        CodeDescriptionPair orderType = new CodeDescriptionPair
                        {
                            Code = "ORD",
                            Description = "ORDER"
                        };

                        xmlOrder.Type = orderType;
                        tRef.Ref1Type = TransReference.RefType.WHSPick;
                        if (dt.Rows.Count > 1)
                        {
                            tRef.Reference1 = dt.Rows[1]["Order Reference"].ToString();

                            xmlOrder.OrderNumber = dt.Rows[1]["PO Number"].ToString();
                        }
                        else
                        {
                            tRef.Reference1 = dt.Rows[0]["Order Reference"].ToString();

                            xmlOrder.OrderNumber = dt.Rows[0]["PO Number"].ToString();
                        }
                        resTrans.Ref1Type = TransReference.RefType.WHSOrder;
                        resTrans.Reference1 = xmlOrder.OrderNumber;

                        //List<ShipmentOrderOrderLineCollection> xmlOrderLineColl = new List<ShipmentOrderOrderLineCollection>();
                        ShipmentOrderOrderLineCollection xmlOrderLine = new ShipmentOrderOrderLineCollection();
                        xmlOrderLine.Content = CollectionContent.Complete;
                        xmlOrderLine.ContentSpecified = true;
                        // For loop to loop through Order lines in Table. 
                        int iline = 0;
                        decimal fTotal = 0;
                        List<ShipmentOrderOrderLineCollectionOrderLine> xmlLines = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                        List<OrganizationAddress> xmlOrgColl = new List<OrganizationAddress>();
                        OrganizationAddress xmlOrg;
                        xmlOrg = new OrganizationAddress
                        {
                            AddressType = "ConsigneeAddress",
                            Address1 = dsPCFS.Tables[0].Rows[0]["Shipping Address"].ToString().Trim(),
                            Address2 = dsPCFS.Tables[0].Rows[0]["Shipping Address2"].ToString().Trim(),
                            City = dsPCFS.Tables[0].Rows[0]["Shipping City"].ToString().Trim(),
                            AddressOverride = true,
                            Postcode = dsPCFS.Tables[0].Rows[0]["Shipping Zip"].ToString().Trim(),
                            Email = dsPCFS.Tables[0].Rows[0]["EmailAddress"].ToString().Trim(),
                            State = dsPCFS.Tables[0].Rows[0]["Shipping State/Province"].ToString().Trim(),
                            CompanyName = dsPCFS.Tables[0].Rows[0]["Customer"].ToString(),
                            AddressShortCode = dsPCFS.Tables[0].Rows[0]["Customer"].ToString()
                        };
                        xmlOrgColl.Add(xmlOrg);
                        xmlOrg = new OrganizationAddress
                        {
                            AddressType = "ConsignorDocumentaryAddress",
                            OrganizationCode = cust.P_SENDERID,
                            Country = new Country { Code = "AU", Name = "Australia" }

                        };
                        xmlOrgColl.Add(xmlOrg);

                        foreach (DataRow dr in dsPCFS.Tables[0].Rows)
                        {

                            if (decimal.Parse(dr["Quantity"].ToString()) > 0)
                            {
                                iline++;
                                ShipmentOrderOrderLineCollectionOrderLine xmlLine = new ShipmentOrderOrderLineCollectionOrderLine();
                                xmlLine.LineNumber = iline;
                                xmlLine.OrderedQty = decimal.Parse(dr["Quantity"].ToString());
                                xmlLine.PackageQty = decimal.Parse(dr["Quantity"].ToString());
                                xmlLine.PackageQtySpecified = true;
                                xmlLine.OrderedQtySpecified = true;
                                fTotal += decimal.Parse(dr["Quantity"].ToString());

                                XUS.Product product = new XUS.Product();
                                product.Code = dr["Item"].ToString();
                                product.Description = dr["Description"].ToString();
                                xmlLine.Product = product;
                                xmlLine.RequiredBy = DateTime.Now.Date.ToString("s");
                                //if (!string.IsNullOrEmpty(dr["DueDate"].ToString()))
                                //{
                                //    DateTime requiredByDate = DateTime.Parse(dr["DueDate"].ToString());
                                //    xmlLine.RequiredBy = requiredByDate.ToString("yyyy-MM-ddThh:mm.ss");
                                //}
                                xmlLines.Add(xmlLine);
                            }

                        }
                        xmlOrder.TotalUnits = fTotal;
                        xmlOrder.TotalUnitsSpecified = true;
                        xmlOrderLine.OrderLine = xmlLines.ToArray();
                        xmlOrder.OrderLineCollection = xmlOrderLine;
                        PCFSShipment.DataContext = dc;
                        PCFSShipment.Order = xmlOrder;
                        PCFSShipment.OrganizationAddressCollection = xmlOrgColl.ToArray();
                        bodydata.Shipment = PCFSShipment;
                        bodydata.version = "1.1";
                        body.BodyField = bodydata;
                        interchange.Body = body;
                        String cwXML = Path.Combine(Globals.glOutputDir, cust.P_SENDERID + "ORD" + xmlOrder.OrderNumber + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(Globals.glOutputDir, cust.P_SENDERID + "ORD" + xmlOrder.OrderNumber + iFileCount + ".xml");
                        }
                        Stream outputCW = File.Open(cwXML, FileMode.Create);
                        resTrans.Ref2Type = TransReference.RefType.Shipment;
                        resTrans.Reference2 = Path.GetFileName(cwXML);
                        StringWriter writer = new StringWriter();
                        XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                        xSer.Serialize(outputCW, interchange);
                        outputCW.Flush();
                        outputCW.Close();

                    }
                    catch (Exception ex)
                    {
                        resTrans.Ref3Type = TransReference.RefType.Error;
                        resTrans.Reference3 = ex.GetType().Name + ": " + ex.Message;
                        MailModule mailModule = new MailModule(Globals.MailServerSettings);
                        mailModule.SendMsg(fileName, Globals.glAlertsTo, cust.C_CODE + ": Untrapped Error Message", ex.Message);

                    }

                }
            }

            return new Tuple<Guid, TransReference>(result, resTrans);
        }

    }
}
