﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using static PCFSatellite.SatelliteErrors;

namespace PCFSatellite
{
    class CargowiseOrders
    {

        //private string GetEnum(string enumType, string mapValue)
        //{
            //string result = string.Empty;
            //SqlCommand sqlEnum = new SqlCommand("GetCWEnum", sqlConn);
            //sqlEnum.CommandType = CommandType.StoredProcedure;
            //SqlParameter varEnum = sqlEnum.Parameters.Add("@ENUM", SqlDbType.NChar, 10);
            //SqlParameter varEnumType = sqlEnum.Parameters.Add("@ENUMTYPE", SqlDbType.VarChar, 20);
            //SqlParameter varMapVal = sqlEnum.Parameters.Add("@MAPVALUE", SqlDbType.VarChar, 20);
            //varEnum.Direction = ParameterDirection.Output;
            //varEnumType.Value = enumType;
            //varMapVal.Value = mapValue;
            //try
            //{
            //    if (sqlConn.State == ConnectionState.Open)
            //    {
            //        sqlConn.Close();
            //    }
            //    sqlConn.Open();
            //    sqlEnum.ExecuteNonQuery();
            //    result = varEnum.Value.ToString().Trim();


            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error Accessing Database: " + ex.Message);
            //}
            //return result;

        //}
       // public TransReference CreateCWSOXML(Guid soid)
       // {
            //TransReference tRef = new TransReference();
            //NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Creating Cargowise Warehouse Order (OUT) XML ");
            //try
            //{

            //    SqlDataAdapter daSo = new SqlDataAdapter("SELECT [SO_SOCODE],[SO_ORDERDATE],[SO_SHIPDATE],[SO_CUSTOMER],[SO_DELSTREET1],[SO_DELSTREET2],[SO_DELPOCODE],[SO_DELSUBURB],[SO_DELSTATE],[SO_PH], [SO_COMPANY],"
            //   + "[SO_PICKINGNOTES],[SO_DELIVERYNOTES],[SO_DELIVERYENTITY],[SO_ORGCODE],[SL_QTY],[SL_PARTNUM],[SL_UOM],[SL_LINENO],[SL_BARCODE],[SL_DESCRIPTION],[SL_BATCHNO], [SO_ID] from VW_SalesOrder"
            //   + " WHERE SO_ID ='" + soid + "'  Order by SL_LINENO", sqlConn);
            //    DataSet dsSo = new DataSet();
            //    daSo.Fill(dsSo, "SO");
            //    String cwXML = string.Empty;
            //    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Creating Order: " + dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim());
            //    if (dsSo.Tables["SO"].Rows.Count > 0)
            //    {
            //        try
            //        {
            //            UniversalInterchange interchange = new UniversalInterchange();
            //            UniversalInterchangeHeader header = new UniversalInterchangeHeader();
            //            header.SenderID = dsSo.Tables["SO"].Rows[0]["SO_ORGCODE"].ToString().Trim(); ;
            //            header.RecipientID = "PCFSYDSYD";
            //            interchange.Header = header;
            //            interchange.version = "1.1";
            //            UniversalInterchangeBody body = new UniversalInterchangeBody();
            //            UniversalShipmentData bodydata = new UniversalShipmentData();
            //            List<UniversalShipmentData> PCFSOrderFile = new List<UniversalShipmentData>();
            //            UniversalShipmentData PCFSOrder = new UniversalShipmentData();
            //            PCFSOrder.version = "1.1";
            //            PCFSOrder.xmlnsa = "http://www.cargowise.com/Schemas/Universal/2011/11";
            //            Shipment PCFSShipment = new Shipment();

            //            DataContext dc = new DataContext();
            //            List<DataTarget> dtColl = new List<DataTarget>();
            //            DataTarget dt = new DataTarget();
            //            dt.Type = "WarehouseOrder"; // Need to revisit this - Not to hardocode                       
            //            dt.Key = "";
            //            CodeDescriptionPair eventType = new CodeDescriptionPair
            //            {
            //                Code = "DIM",
            //                Description = "Data Imported"
            //            };
            //            dc.EventType = eventType;
            //            dtColl.Add(dt);
            //            dc.DataTargetCollection = dtColl.ToArray();
            //            PCFSShipment.DataContext = dc;
            //            Company xmlCompany = new Company();
            //            xmlCompany.Code = "SYD";
            //            Country xmlCountry = new Country();
            //            xmlCountry.Code = "AU";
            //            xmlCountry.Name = "Australia";
            //            xmlCompany.Country = xmlCountry;
            //            ShipmentOrder xmlOrder = new ShipmentOrder();
            //            xmlOrder.OrderNumber = dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim();
            //            CodeDescriptionPair fulfillmentRule = new CodeDescriptionPair();
            //            //TODO - Create a Pick Rule option in the Profile List??
            //            fulfillmentRule.Code = "NON";
            //            fulfillmentRule.Description = "None";
            //            xmlOrder.FulfillmentRule = fulfillmentRule;
            //            CodeDescriptionPair pickRule = new CodeDescriptionPair();
            //            pickRule.Code = "MAN";
            //            pickRule.Description = "Manual Pick";
            //            xmlOrder.PickOption = pickRule;
            //            CodeDescriptionPair orderType = new CodeDescriptionPair();
            //            orderType.Code = "ORD";
            //            orderType.Description = "ORDER";
            //            xmlOrder.Type = orderType;
            //            ShipmentOrderWarehouse xmlWarehouse = new ShipmentOrderWarehouse();
            //            xmlWarehouse.Code = "SYD";
            //            xmlWarehouse.Name = "PCFS SYDNEY";
            //            xmlOrder.Warehouse = xmlWarehouse;
            //            List<Date> dateColl = new List<Date>();
            //            DateTime dDelDate;
            //            if (DateTime.TryParse(dsSo.Tables["SO"].Rows[0]["SO_SHIPDATE"].ToString(), out dDelDate))
            //            {
            //                dateColl.Add(new Date { Type = DateType.DeliveryRequiredBy, IsEstimate = true, IsEstimateSpecified = true, Value = DateTime.Parse(dsSo.Tables["SO"].Rows[0]["SO_SHIPDATE"].ToString()).ToString("s") });
            //            }

            //            xmlOrder.DateCollection = dateColl.ToArray();
            //            CodeDescriptionPair orderStatus = new CodeDescriptionPair();
            //            List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
            //            ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
            //            int i = 0;
            //            orderLine.Content = CollectionContent.Complete;
            //            List<ShipmentOrderOrderLineCollectionOrderLine> olColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            //            orderLine.ContentSpecified = true;
            //            foreach (DataRow dr in dsSo.Tables["SO"].Rows)
            //            {
            //                i++;
            //                if (decimal.Parse(dr["SL_QTY"].ToString()) > 0)
            //                {
            //                    ShipmentOrderOrderLineCollectionOrderLine oLine = new ShipmentOrderOrderLineCollectionOrderLine
            //                    {
            //                        PackageQty = decimal.Parse(dr["SL_QTY"].ToString()),
            //                        PackageQtySpecified = true,
            //                        LineNumber = int.Parse(dr["SL_LINENO"].ToString()),// i,                             
            //                        LineNumberSpecified = true
            //                    };
            //                    PackageType packageType;
            //                    if (!string.IsNullOrEmpty(dr["SL_UOM"].ToString()))
            //                    {
            //                        packageType = new PackageType { Code = GetEnum("UOM", dr["SL_UOM"].ToString()) };
            //                    }
            //                    else
            //                    {
            //                        packageType = new PackageType { Code = "EA" };
            //                    }
            //                    oLine.PackageQtyUnit = packageType;
            //                    oLine.PackageQtySpecified = true;
            //                    Product product = new Product
            //                    {
            //                        Code = dr["SL_PARTNUM"].ToString(),
            //                        Description = dr["SL_DESCRIPTION"].ToString()
            //                    };

            //                    oLine.Product = product;
            //                    olColl.Add(oLine);
            //                }

            //            }
            //            orderLine.OrderLine = olColl.ToArray();
            //            xmlOrder.OrderLineCollection = orderLine;
            //            ShipmentNoteCollection notecollection = new ShipmentNoteCollection();
            //            List<Note> orderNotes = new List<Note>();
            //            notecollection.Content = CollectionContent.Partial;
            //            notecollection.ContentSpecified = true;
            //            Note orderNote = new Note();
            //            CodeDescriptionPair nodeVisibility = new CodeDescriptionPair
            //            {
            //                Code = "PUB",
            //                Description = "CLIENT-VISIBLE"
            //            };
            //            orderNote.Visibility = nodeVisibility;
            //            orderNote.Description = "Import Delivery Instructions";
            //            orderNote.IsCustomDescription = false;
            //            orderNote.NoteText = dsSo.Tables["SO"].Rows[0]["SO_DELIVERYNOTES"].ToString();
            //            NoteNoteContext noteContext = new NoteNoteContext();
            //            noteContext.Code = "WAA";
            //            noteContext.Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All";
            //            orderNote.NoteContext = noteContext;
            //            orderNotes.Add(orderNote);
            //            orderNote = new Note();
            //            orderNote.Visibility = nodeVisibility;
            //            orderNote.Description = "Picking Instructions";
            //            orderNote.IsCustomDescription = false;
            //            orderNote.NoteText = dsSo.Tables["SO"].Rows[0]["SO_PICKINGNOTES"].ToString();
            //            noteContext = new NoteNoteContext();
            //            noteContext.Code = "WAA";
            //            noteContext.Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All";
            //            orderNote.NoteContext = noteContext;
            //            orderNotes.Add(orderNote);
            //            //note = orderNotes.ToArray();
            //            notecollection.Note = orderNotes.ToArray();
            //            PCFSShipment.NoteCollection = notecollection;
            //            List<OrganizationAddress> xmlOrgColl = new List<OrganizationAddress>();
            //            OrganizationAddress xmlOrg = new OrganizationAddress
            //            {
            //                AddressType = "ConsignorDocumentaryAddress",
            //                OrganizationCode = header.SenderID
            //            };
            //            xmlOrgColl.Add(xmlOrg);
            //            xmlOrg = new OrganizationAddress
            //            {
            //                AddressType = "ConsigneeAddress",
            //                Address1 = dsSo.Tables["SO"].Rows[0]["SO_DELSTREET1"].ToString().Trim(),
            //                Address2 = dsSo.Tables["SO"].Rows[0]["SO_DELSTREET2"].ToString().Trim(),
            //                City = dsSo.Tables["SO"].Rows[0]["SO_DELSUBURB"].ToString().Trim(),
            //                Postcode = dsSo.Tables["SO"].Rows[0]["SO_DELPOCODE"].ToString().Trim(),
            //                State = dsSo.Tables["SO"].Rows[0]["SO_DELSTATE"].ToString().Trim(),
            //                CompanyName = dsSo.Tables["SO"].Rows[0]["SO_CUSTOMER"].ToString().Trim(),
            //                Phone = dsSo.Tables["SO"].Rows[0]["SO_PH"].ToString().Trim(),
            //                AddressShortCode = (string.IsNullOrEmpty(dsSo.Tables["SO"].Rows[0]["SO_COMPANY"].ToString().Trim()) ? dsSo.Tables["SO"].Rows[0]["SO_COMPANY"].ToString().Trim() : dsSo.Tables["SO"].Rows[0]["SO_CUSTOMER"].ToString().Trim())
            //            };

            //            xmlOrgColl.Add(xmlOrg);
            //            PCFSShipment.OrganizationAddressCollection = xmlOrgColl.ToArray();
            //            PCFSShipment.Order = xmlOrder;
            //            bodydata.Shipment = PCFSShipment;
            //            body.BodyField = bodydata;
            //            bodydata.version = "1.1";

            //            cwXML = Path.Combine(Globals.glOutputDir, header.SenderID + "SO" + xmlOrder.OrderNumber.Trim() + ".xml");
            //            int iFileCount = 0;
            //            while (File.Exists(cwXML))
            //            {
            //                iFileCount++;
            //                cwXML = Path.Combine(Globals.glOutputDir, header.SenderID + "SO" + xmlOrder.OrderNumber.Trim() + iFileCount + ".xml");
            //            }
            //            Stream outputCW = File.Open(cwXML, FileMode.Create);

            //            StringWriter writer = new StringWriter();
            //            interchange.Body = body;
            //            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            //            var cwNSUniversal = new XmlSerializerNamespaces();
            //            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            //            xSer.Serialize(outputCW, interchange, cwNSUniversal);
            //            outputCW.Flush();
            //            outputCW.Close();
            //            string archive = Globals.ArchiveFile(Globals.glArcLocation, cwXML);
            //            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Sales Order Created: " + Path.GetFileName(cwXML));
            //            tRef.Ref1Type = TransReference.RefType.WHSPick;
            //            tRef.Ref2Type = TransReference.RefType.Custom;
            //            tRef.Reference1 = xmlOrder.OrderNumber;
            //            tRef.Reference2 = dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim();
            //            NodeResources.AddTransaction(NodeResources.GetCustID(header.RecipientID), Guid.Empty, cwXML, "S", true, tRef, archive);

            //        }
            //        catch (Exception ex)
            //        {
            //            string strEx = ex.GetType().Name;
            //            ProcessingErrors procerror = new ProcessingErrors();
            //            CTCErrorCode error = new CTCErrorCode
            //            {
            //                Code = NodeError.e104,
            //                Description = "Error Converting file to PCFS Sales Order XML: " + strEx,
            //                Severity = "Error"
            //            };
            //            procerror.ErrorCode = error;
            //            procerror.SenderId = dsSo.Tables["SO"].Rows[0]["SO_ORGCODE"].ToString().Trim(); ;
            //            procerror.RecipientId = "PCFSYDSYD";
            //            procerror.FileName = cwXML;
            //            procerror.ProcId = Guid.Empty;
            //            procerror.Reference = dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim();
            //            procerror.RefType = "Order XML";
            //            AddProcessingError(procerror);
            //            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Error during Conversion. Error was: " + ex.Message);
            //            MailModule.sendMsg("", Globals.glAlertsTo, "Error Converting file to PCFS XML", ex);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MailModule.sendMsg(string.Empty, Globals.glAlertsTo, Globals.glCustCode + ": Unable to process Order", ex);
            //    //   NodeResources.MoveFile(xmlFile, Globals.glFailPath);
            //}
            //return tRef;
      //  }
    }
}
