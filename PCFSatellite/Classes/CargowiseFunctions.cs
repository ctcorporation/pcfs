﻿using PCFSatellite.Classes;
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using XMLLocker.CTC;
using XUS;

namespace PCFSatellite
{
    public class CargowiseFunctions
    {
        #region members
        string _outPath;
        string _err;
        string _cwXML = string.Empty;
        private string _appLogPath = string.Empty;
        #endregion

        #region properties

        public string Ref1
        {
            get; set;
        }
        public string Ref2
        {
            get; set;
        }
        public string Ref1Type
        {
            get; set;
        }
        public string Ref2Type
        {
            get; set;
        }
        public string ErrMsg
        {
            get
            { return _err; }
            set
            { _err = value; }
        }
        #endregion

        #region constructors
        public CargowiseFunctions(string outPath)
        {
            _outPath = outPath;

        }
        #endregion

        #region methods
        public string CWfromCommon(NodeFile common)
        {
            Type type = typeof(ToCargowise);
            ConstructorInfo satConstructor = type.GetConstructor(new Type[] { typeof(string) });
            object satObject = satConstructor.Invoke(new object[] { this._appLogPath });
            var method = type.GetMethod("CWFromCommon");
            try
            {
                var returnObject = method.Invoke(satObject, new object[] { common });
                if (returnObject == null)
                {
                    throw new Exception("Error Creating Cargowise XML File. ");
                }

                if (returnObject.GetType().ToString().Contains(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange).ToString()))
                {
                    var cwFile = (XMLLocker.Cargowise.XUS.UniversalInterchange)returnObject;
                    var cwFunction = new CargowiseFunctions(Globals.glOutputDir)
                    {
                        Ref1 = Ref1,
                        Ref1Type = Ref1Type
                    };

                    var log = new SatelliteLogs(Globals.glConnString);
                    var newfile = CreateCWFile(cwFile, common.IdendtityMatrix.CustomerId, common.IdendtityMatrix.SenderId);
                    if (!string.IsNullOrEmpty(newfile))
                    {

                        log.AddTransaction(newfile, string.Empty, Ref1, Ref2, Ref1Type, Ref2Type, "Success", common.IdendtityMatrix.CustomerId + "Cargowise XML Created");
                    }
                    else
                    {
                        log.AddTransaction(newfile, string.Empty, Ref1, Ref2, Ref1Type, Ref2Type, "Error", _err);
                    }
                    return newfile;
                }


            }
            catch (Exception ex)
            {

                _err += ex.GetType().Name + "Error Found Creating Cargowise File. Check error log for details." + Environment.NewLine;
                var log = new SatelliteLogs(Globals.glConnString);
                log.AddTransaction(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Error", ex.GetType().Name + " in " + method.Name + ": (" + ex.Source + ") " + ex.Message + "." + Environment.NewLine + ex.ToLogString(ex.StackTrace));

            }
            return _err;
        }
        private string CreateCWFile(XMLLocker.Cargowise.XUS.UniversalInterchange cwFile, string recipient, string sender)
        {
            try
            {
                _cwXML = Path.Combine(_outPath, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + ".xml");
                int iFileCount = 0;
                while (File.Exists(_cwXML))
                {
                    iFileCount++;
                    _cwXML = Path.Combine(_outPath, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + "-" + iFileCount + ".xml");
                }
                Stream outputCW = File.Open(_cwXML, FileMode.Create);
                using (StringWriter writer = new StringWriter())
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange));
                    var cwNSUniversal = new XmlSerializerNamespaces();
                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                    xSer.Serialize(outputCW, cwFile, cwNSUniversal);
                    outputCW.Flush();
                    outputCW.Close();
                    return _cwXML;
                }
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                _err += ex.GetType().Name + "Error Found Creating Cargowise File. Check error log for details." + Environment.NewLine; ;
                var log = new SatelliteLogs(Globals.glConnString);
                log.AddTransaction(_cwXML, string.Empty, Ref1, Ref2, Ref1Type, Ref2Type, "Error", ex.GetType().Name + " in " + m.Name + ": (" + ex.Source + ") " + ex.Message + "." + Environment.NewLine + ex.ToLogString(ex.StackTrace));
                return string.Empty;
            }




        }
        #endregion

        #region Helpers
        public XUS.Note CreateNote(string noteContent, string noteDesc)
        {
            Note note = new Note
            {
                Description = noteDesc,
                NoteText = noteContent,
                IsCustomDescription = false,
                Visibility = new CodeDescriptionPair
                {
                    Code = "PUB",
                    Description = "CLIENT-VISIBLE"
                },
                NoteContext = new NoteContext
                {
                    Code = "WAA",
                    Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All"
                }
            };
            return note;
        }
        #endregion







        //public static TransReference CreateCWSOXML(Guid soid)
        //{
        //    TransReference result = new TransReference();
        //    try
        //    {
        //        using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
        //        {
        //            SqlDataAdapter daSo = new SqlDataAdapter("SELECT [SO_SOCODE],[SO_ORDERDATE],[SO_SHIPDATE],[SO_CUSTOMER],[SO_DELSTREET1],[SO_DELSTREET2],[SO_DELPOCODE],[SO_DELSUBURB],[SO_DELSTATE],[SO_PH], [SO_COMPANY],"
        //       + "[SO_PICKINGNOTES],[SO_DELIVERYNOTES],[SO_DELIVERYENTITY],[SO_ORGCODE],[SL_QTY],[SL_PARTNUM],[SL_UOM],[SL_LINENO],[SL_BARCODE],[SL_DESCRIPTION],[SO_ID] from VW_SalesOrder"
        //       + " WHERE SO_ID ='" + soid + "'  Order by SL_LINENO", sqlConn);
        //            DataSet dsSo = new DataSet();
        //            daSo.Fill(dsSo, "SO");
        //            String cwXML = string.Empty;
        //            if (dsSo.Tables["SO"].Rows.Count > 0)
        //            {
        //                try
        //                {
        //                    UniversalInterchange interchange = new UniversalInterchange();
        //                    UniversalInterchangeHeader header = new UniversalInterchangeHeader();
        //                    header.SenderID = dsSo.Tables["SO"].Rows[0]["SO_ORGCODE"].ToString().Trim(); ;
        //                    header.RecipientID = "PCFSYDSYD";
        //                    interchange.Header = header;
        //                    interchange.version = "1.1";
        //                    UniversalInterchangeBody body = new UniversalInterchangeBody();
        //                    UniversalShipmentData bodydata = new UniversalShipmentData();
        //                    List<UniversalShipmentData> PCFSOrderFile = new List<UniversalShipmentData>();
        //                    UniversalShipmentData PCFSOrder = new UniversalShipmentData();
        //                    PCFSOrder.version = "1.1";

        //                    Shipment PCFSShipment = new Shipment();

        //                    DataContext dc = new DataContext();
        //                    List<DataTarget> dtColl = new List<DataTarget>();
        //                    DataTarget dt = new DataTarget();
        //                    dt.Type = "WarehouseOrder"; // Need to revisit this - Not to hardocode                       
        //                    dt.Key = "";
        //                    CodeDescriptionPair eventType = new CodeDescriptionPair
        //                    {
        //                        Code = "DIM",
        //                        Description = "Data Imported"
        //                    };
        //                    dc.EventType = eventType;
        //                    dtColl.Add(dt);
        //                    dc.DataTargetCollection = dtColl.ToArray();
        //                    PCFSShipment.DataContext = dc;
        //                    Company xmlCompany = new Company();
        //                    xmlCompany.Code = "SYD";
        //                    Country xmlCountry = new Country();
        //                    xmlCountry.Code = "AU";
        //                    xmlCountry.Name = "Australia";
        //                    xmlCompany.Country = xmlCountry;
        //                    ShipmentOrder xmlOrder = new ShipmentOrder();
        //                    xmlOrder.OrderNumber = dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim();
        //                    CodeDescriptionPair fulfillmentRule = new CodeDescriptionPair();
        //                    //TODO - Create a Pick Rule option in the Profile List??
        //                    fulfillmentRule.Code = "NON";
        //                    fulfillmentRule.Description = "None";
        //                    xmlOrder.FulfillmentRule = fulfillmentRule;
        //                    CodeDescriptionPair pickRule = new CodeDescriptionPair();
        //                    pickRule.Code = "MAN";
        //                    pickRule.Description = "Manual Pick";
        //                    xmlOrder.PickOption = pickRule;
        //                    CodeDescriptionPair orderType = new CodeDescriptionPair();
        //                    orderType.Code = "ORD";
        //                    orderType.Description = "ORDER";
        //                    xmlOrder.Type = orderType;
        //                    ShipmentOrderWarehouse xmlWarehouse = new ShipmentOrderWarehouse();
        //                    xmlWarehouse.Code = "SYD";
        //                    xmlWarehouse.Name = "PCFS SYDNEY";
        //                    xmlOrder.Warehouse = xmlWarehouse;
        //                    List<Date> dateColl = new List<Date>();
        //                    DateTime dDelDate;
        //                    if (DateTime.TryParse(dsSo.Tables["SO"].Rows[0]["SO_SHIPDATE"].ToString(), out dDelDate))
        //                    {
        //                        dateColl.Add(new Date { Type = DateType.DeliveryRequiredBy, IsEstimate = true, IsEstimateSpecified = true, Value = DateTime.Parse(dsSo.Tables["SO"].Rows[0]["SO_SHIPDATE"].ToString()).ToString("s") });
        //                    }

        //                    xmlOrder.DateCollection = dateColl.ToArray();
        //                    CodeDescriptionPair orderStatus = new CodeDescriptionPair();
        //                    List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
        //                    ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
        //                    int i = 0;
        //                    orderLine.Content = CollectionContent.Complete;
        //                    List<ShipmentOrderOrderLineCollectionOrderLine> olColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
        //                    orderLine.ContentSpecified = true;
        //                    foreach (DataRow dr in dsSo.Tables["SO"].Rows)
        //                    {
        //                        i++;
        //                        if (decimal.Parse(dr["SL_QTY"].ToString()) > 0)
        //                        {
        //                            ShipmentOrderOrderLineCollectionOrderLine oLine = new ShipmentOrderOrderLineCollectionOrderLine
        //                            {
        //                                PackageQty = decimal.Parse(dr["SL_QTY"].ToString()),
        //                                PackageQtySpecified = true,
        //                                LineNumber = int.Parse(dr["SL_LINENO"].ToString()),// i,                             
        //                                LineNumberSpecified = true
        //                            };
        //                            PackageType packageType;
        //                            if (!string.IsNullOrEmpty(dr["SL_UOM"].ToString()))
        //                            {
        //                                packageType = new PackageType { Code = NodeResources.GetEnum("UOM", dr["SL_UOM"].ToString()) };
        //                            }
        //                            else
        //                            {
        //                                packageType = new PackageType { Code = "EA" };
        //                            }
        //                            oLine.PackageQtyUnit = packageType;
        //                            oLine.PackageQtySpecified = true;
        //                            Product product = new Product
        //                            {
        //                                Code = dr["SL_PARTNUM"].ToString(),
        //                                Description = dr["SL_DESCRIPTION"].ToString()
        //                            };
        //                            oLine.Product = product;
        //                            olColl.Add(oLine);
        //                        }

        //                    }
        //                    orderLine.OrderLine = olColl.ToArray();
        //                    xmlOrder.OrderLineCollection = orderLine;
        //                    ShipmentNoteCollection notecollection = new ShipmentNoteCollection();
        //                    List<Note> orderNotes = new List<Note>();
        //                    notecollection.Content = CollectionContent.Partial;
        //                    notecollection.ContentSpecified = true;
        //                    Note orderNote = new Note();
        //                    CodeDescriptionPair nodeVisibility = new CodeDescriptionPair
        //                    {
        //                        Code = "PUB",
        //                        Description = "CLIENT-VISIBLE"
        //                    };
        //                    orderNote.Visibility = nodeVisibility;
        //                    orderNote.Description = "Import Delivery Instructions";
        //                    orderNote.IsCustomDescription = false;
        //                    orderNote.NoteText = dsSo.Tables["SO"].Rows[0]["SO_DELIVERYNOTES"].ToString();
        //                    XMLLocker.Cargowise.XUS.NoteNoteContext noteContext = new NoteNoteContext();
        //                    noteContext.Code = "WAA";
        //                    noteContext.Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All";
        //                    orderNote.NoteContext = noteContext;
        //                    orderNotes.Add(orderNote);
        //                    orderNote = new Note();
        //                    orderNote.Visibility = nodeVisibility;
        //                    orderNote.Description = "Picking Instructions";
        //                    orderNote.IsCustomDescription = false;
        //                    orderNote.NoteText = dsSo.Tables["SO"].Rows[0]["SO_PICKINGNOTES"].ToString();
        //                    noteContext = new NoteNoteContext();
        //                    noteContext.Code = "WAA";
        //                    noteContext.Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All";
        //                    orderNote.NoteContext = noteContext;
        //                    orderNotes.Add(orderNote);
        //                    //note = orderNotes.ToArray();
        //                    notecollection.Note = orderNotes.ToArray();
        //                    PCFSShipment.NoteCollection = notecollection;
        //                    List<OrganizationAddress> xmlOrgColl = new List<OrganizationAddress>();
        //                    OrganizationAddress xmlOrg = new OrganizationAddress
        //                    {
        //                        AddressType = "ConsignorDocumentaryAddress",
        //                        OrganizationCode = header.SenderID
        //                    };
        //                    xmlOrgColl.Add(xmlOrg);
        //                    xmlOrg = new OrganizationAddress
        //                    {
        //                        AddressType = "ConsigneeAddress",
        //                        Address1 = dsSo.Tables["SO"].Rows[0]["SO_DELSTREET1"].ToString().Trim(),
        //                        Address2 = dsSo.Tables["SO"].Rows[0]["SO_DELSTREET2"].ToString().Trim(),
        //                        City = dsSo.Tables["SO"].Rows[0]["SO_DELSUBURB"].ToString().Trim(),
        //                        Postcode = dsSo.Tables["SO"].Rows[0]["SO_DELPOCODE"].ToString().Trim(),
        //                        State = dsSo.Tables["SO"].Rows[0]["SO_DELSTATE"].ToString().Trim(),
        //                        CompanyName = dsSo.Tables["SO"].Rows[0]["SO_CUSTOMER"].ToString().Trim(),
        //                        Phone = dsSo.Tables["SO"].Rows[0]["SO_PH"].ToString().Trim(),
        //                        AddressShortCode = (string.IsNullOrEmpty(dsSo.Tables["SO"].Rows[0]["SO_COMPANY"].ToString().Trim()) ? dsSo.Tables["SO"].Rows[0]["SO_COMPANY"].ToString().Trim() : dsSo.Tables["SO"].Rows[0]["SO_CUSTOMER"].ToString().Trim())
        //                    };

        //                    xmlOrgColl.Add(xmlOrg);
        //                    PCFSShipment.OrganizationAddressCollection = xmlOrgColl.ToArray();
        //                    PCFSShipment.Order = xmlOrder;
        //                    bodydata.Shipment = PCFSShipment;
        //                    body.BodyField = bodydata;
        //                    bodydata.version = "1.1";

        //                    cwXML = Path.Combine(Globals.glOutputDir, header.SenderID + "SO" + xmlOrder.OrderNumber.Trim() + ".xml");
        //                    int iFileCount = 0;
        //                    while (File.Exists(cwXML))
        //                    {
        //                        iFileCount++;
        //                        cwXML = Path.Combine(Globals.glOutputDir, header.SenderID + "SO" + xmlOrder.OrderNumber.Trim() + iFileCount + ".xml");
        //                    }
        //                    Stream outputCW = File.Open(cwXML, FileMode.Create);

        //                    StringWriter writer = new StringWriter();
        //                    interchange.Body = body;
        //                    XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
        //                    var cwNSUniversal = new XmlSerializerNamespaces();
        //                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
        //                    xSer.Serialize(outputCW, interchange, cwNSUniversal);
        //                    outputCW.Flush();
        //                    outputCW.Close();
        //                    string archive = Globals.ArchiveFile(Globals.glArcLocation, cwXML);
        //                    result.Ref1Type = TransReference.RefType.WHSPick;
        //                    result.Ref2Type = TransReference.RefType.Custom;
        //                    result.Reference1 = xmlOrder.OrderNumber;
        //                    result.Reference2 = dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim();
        //                    NodeResources.AddTransaction(NodeResources.GetCustID(header.RecipientID), Guid.Empty, cwXML, "S", true, result, archive);

        //                }
        //                catch (Exception ex)
        //                {
        //                    string strEx = ex.GetType().Name;
        //                    string errMsg = "Error found when processing " + dsSo.Tables["SO"].Rows[0]["SO_SOCODE"].ToString().Trim() + Environment.NewLine;

        //                    errMsg += "Error Type: " + strEx + Environment.NewLine;
        //                    errMsg += "Message: " + ex.Message + Environment.NewLine;
        //                    errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
        //                    errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
        //                    errMsg += "Source: " + ex.Source;

        //                    MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //                    mailModule.SendMsg("", Globals.glAlertsTo, "PCFS Unhandled Exception: ProcessCustomXML", ex.Message);
        //                    result.Ref3Type = TransReference.RefType.Error;
        //                    result.Reference3 = strEx + ": " + errMsg;

        //                    mailModule.SendMsg("", Globals.glAlertsTo, "Error Converting file to PCFS XML", ex.Message);
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //        mailModule.SendMsg(string.Empty, Globals.glAlertsTo, Globals.glCustCode + ": Unable to process Officeworks Order", ex.Message);
        //        //   NodeResources.MoveFile(xmlFile, Globals.glFailPath);
        //    }
        //    return result;
        //}
    }
}
