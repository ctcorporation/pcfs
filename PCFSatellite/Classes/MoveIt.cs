﻿using PCFSatellite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using XUS;

namespace PCFSatellite
{
    public class MoveIt
    {
        public static TransReference CreateMoveItFile(UniversalInterchange toConvert, Customer_Profile custProfileRecord)
        {
            TransReference result = new TransReference();


            try
            {
                Shipment cwShipment = new Shipment();
                String senderID = toConvert.Header.SenderID;
                cwShipment = toConvert.Body.BodyField.Shipment;
                DataContext dc = new DataContext();
                dc = cwShipment.DataContext;

                DataSource[] dscoll;
                dscoll = dc.DataSourceCollection;
                string activityReply = string.Empty;
                string methodReply = string.Empty;
                string moveitJourney = string.Empty;

                moveitJourney = custProfileRecord.P_RECIPIENTID + " - ";



                //use CW1 job number as Ref
                string shipmentNo = "";
                string otherShipmentNo = "";
                //bool pickFound = false;
                foreach (DataSource ds in dscoll)
                {
                    if (ds.Type == "WarehouseOrder")
                    {
                        shipmentNo = ds.Key;
                    }
                    else
                    {
                        otherShipmentNo = ds.Key;
                    }
                }
                if ( String.IsNullOrEmpty(shipmentNo) && otherShipmentNo != "")
                {
                    shipmentNo = otherShipmentNo;
                }

                OrganizationAddress pickupAdd = GetAddress(cwShipment.OrganizationAddressCollection, "ConsignorPickupDeliveryAddress");
                address pickupAddress = new address();
                List<address> pickupAddresses = new List<address>();
                if (pickupAdd != null)
                {
                    pickupAddress.addressCode = pickupAdd.State == "NSW" ? "Pauls Customs Botany" : "Pauls Customs Laverton";
                    pickupAddress.addToDropdown = address.addToDropdownType.Y;
                    pickupAddress.addToDropdownSpecified = true;
                    pickupAddress.line1 = pickupAdd.CompanyName;
                    pickupAddress.line2 = pickupAdd.Address1;
                    pickupAddress.line3 = pickupAdd.Address2;
                    pickupAddress.town = pickupAdd.City;
                    pickupAddress.state = pickupAdd.State;
                    pickupAddress.postcode = pickupAdd.Postcode;
                    pickupAddresses.Add(pickupAddress);
                }
                address deliveryAddress = new address();
                List<address> deliveryAddresses = new List<address>();
                OrganizationAddress deliveryAdd = GetAddress(cwShipment.OrganizationAddressCollection, "ConsigneeAddress");
                if (deliveryAdd != null)
                {
                    deliveryAddress.addressCode = deliveryAdd.CompanyName;
                    deliveryAddress.addToDropdown = address.addToDropdownType.Y;
                    deliveryAddress.addToDropdownSpecified = true;
                    deliveryAddress.line1 = deliveryAdd.CompanyName;
                    deliveryAddress.line2 = deliveryAdd.Address1;
                    deliveryAddress.line3 = deliveryAdd.Address2;
                    deliveryAddress.town = deliveryAdd.City;
                    deliveryAddress.state = deliveryAdd.State;
                    deliveryAddress.postcode = deliveryAdd.Postcode;
                    deliveryAddresses.Add(deliveryAddress);

                }
                OrganizationAddress transportAdd = GetAddress(cwShipment.OrganizationAddressCollection, "TransportCompanyDocumentaryAddress");
                if (transportAdd != null)
                {
                    moveitJourney = moveitJourney + transportAdd.CompanyName;
                    // manual code to add express type on journey till better way
                    switch (transportAdd.OrganizationCode)
                    {
                        case "DXTSYD":
                            moveitJourney = moveitJourney + " - " + "Express Service";
                            break;
                        case "TOLIPEJFM":
                            moveitJourney = moveitJourney + " - " + "Road Express Service";
                            break;
                        default:
                            break;
                    }
                }

                List<consignmentsConsignmentContainer> containers = new List<consignmentsConsignmentContainer>();
                foreach (ShipmentOrderOrderLineCollectionOrderLine orderline in cwShipment.Order.OrderLineCollection.OrderLine)
                {
                    consignmentsConsignmentContainer container = new consignmentsConsignmentContainer();
                    //container.orderReference = cwShipment.Order.OrderNumber;
                    //use CW1 job number as Ref
                    container.orderReference = shipmentNo;
                    //container.unit = orderline.PackageQtyUnit.Description == "Each" ? "Carton" : orderline.PackageQtyUnit.Code;
                    switch (orderline.PackageQtyUnit.Code)
                    {
                        case "CTN":
                            container.unit = "Carton";
                            break;
                        case "PLT":
                            container.unit = "Pallet";
                            break;
                        default:
                            container.unit = "UNT";
                            break;
                    }
                    container.quantity = decimal.ToInt16(orderline.PackageQty).ToString();
                    container.unitDescription = orderline.Product.Description;
                    containers.Add(container);
                }
                List<consignmentsConsignment> consignmentList = new List<consignmentsConsignment>();
                consignmentsConsignment conNote = new consignmentsConsignment();
                
                //use CW1 Job numbere
                //conNote.shipperReference = cwShipment.Order.ClientReference;
                conNote.shipperReference = shipmentNo;

                //List<consignmentsConsignment> consignmentList = new List<consignmentsConsignment>();
                //consignmentsConsignment consignmentNumber = new consignmentsConsignment();

                //Do not use consignment number if want vnumber generated
                //conNote.consignmentNumber = cwShipment.Order.OrderNumber;
                //Use the line of Business - is the Customer in Moveit
                conNote.lineOfBusiness = "Pauls Customs";

                conNote.journey = moveitJourney;
                // not in xsd schema conNote.numberOfPallets = cwShipment.Order.PalletsSent.ToString();
                DateTime dispatchDate;

                conNote.despatchDate = DateTime.TryParse(cwShipment.LocalProcessing.DeliveryRequiredBy, out dispatchDate) ? dispatchDate.ToString("yyyy-MM-dd") : string.Empty;
                conNote.pickupAddress = pickupAddresses.ToArray();
                conNote.deliveryAddress = deliveryAddresses.ToArray();
                conNote.container = containers.ToArray();
                consignmentList.Add(conNote);
                consignments conNotes = new consignments { Items = consignmentList.ToArray() };

                //was using cwShipment.Order.OrderNumber.Trim()
                String moveitXML = Path.Combine(Globals.glOutputDir, "MOVEIT" + "CONNOTE" + shipmentNo.Trim() + ".xml");
                int iFileCount = 0;
                while (File.Exists(moveitXML))
                {
                    iFileCount++;
                    moveitXML = Path.Combine(Globals.glOutputDir, "MOVEIT" + "CONNOTE" + shipmentNo.Trim() + iFileCount + ".xml");
                }
                Stream outputMOVEIT = File.Open(moveitXML, FileMode.Create);
                //StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(consignments));
                var moveNS = new XmlSerializerNamespaces();
                moveNS.Add("", "http://www.moveit.com.au/schema/consignments.xsd");

                xSer.Serialize(outputMOVEIT, conNotes, moveNS);
                outputMOVEIT.Flush();
                outputMOVEIT.Close();
                string archiveFile = Globals.ArchiveFile(Globals.glArcLocation, moveitXML);

                archiveFile = Globals.ArchiveFile(Globals.glArcLocation, moveitXML);
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }


            return result;

        }

        public static OrganizationAddress GetAddress(OrganizationAddress[] organizationAddressCollection, string AddressType)
        {

            OrganizationAddress Address = organizationAddressCollection.ToList().Find(x => x.AddressType == AddressType);
            return Address;
        }


    }
}
