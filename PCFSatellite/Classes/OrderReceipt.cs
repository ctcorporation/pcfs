﻿using NodeResources;
using PCFSatellite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using XUS;

namespace PCFSatellite
{
    public class OrderReceipt
    {
        #region Members
        string _senderID;
        string _receipientID;
        DataTable _dt;

        #endregion
        private List<MapOperation> _map;
        private DataTable _receiptTable;
        private string _errSummary = string.Empty;
        private string _connString;
        private MailServerSettings _mailSettings;

        #region Properties
        public string SenderId
        {
            get
            { return _senderID; }
            set
            {
                _senderID = value;
            }
        }

        public MailServerSettings MailSettings
        {
            get
            {
                return _mailSettings;
            }
            set
            {
                _mailSettings = value;
            }
        }


        public string ConnnString
        {
            get
            { return _connString; }
            set
            {
                _connString = value;
            }
        }
        public string LogPath
        {
            get;
        }

        public string RecipientId
        {
            get
            { return _receipientID; }
            set
            {
                _receipientID = value;
            }

        }
        #endregion

        #region Constructors
        public OrderReceipt(string connString, string senderID, string recipientid, string logPath)
        {
            _connString = connString;
            _senderID = senderID;
            _receipientID = recipientid;
            LogPath = logPath;

        }

        #endregion
        public OrderReceipt(string connString, DataTable tb, List<MapOperation> map)
        {
            _map = map;
            _dt = tb;
            _connString = connString;
        }

        #region Methods

        public NodeFile AddReceiptOrder(DataTable tb)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            string lo = LogPath;
            string cp = string.Empty;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            string conversionResult = string.Empty;
            NodeFile nodeFile = new NodeFile();
            NodeFileIdendtityMatrix id = new NodeFileIdendtityMatrix
            {
                CustomerId = RecipientId,
                SenderId = SenderId,
                DocumentType = "WarehouseReceipt"
            };
            nodeFile.IdendtityMatrix = id;
            NodeFileWarehouseReceipt receipt = new NodeFileWarehouseReceipt();

            return nodeFile;
        }

        public void ImportPO(DataTable tb, string refPO, string senderCode, string recipCode)
        {

                UniversalInterchange uInt = new UniversalInterchange();
                UniversalInterchangeHeader uHeader = new UniversalInterchangeHeader();
                uHeader.RecipientID = recipCode;
                uHeader.SenderID = senderCode;
                uInt.Header = uHeader;
                UniversalInterchangeBody uBody = new UniversalInterchangeBody();
                UniversalShipmentData uShipData = new UniversalShipmentData();
                Shipment shipment = new Shipment();
                DataContext dc = new DataContext();
                List<DataTarget> dataTargetColl = new List<DataTarget>();
                DataTarget dataTarget = new DataTarget();
                dataTarget.Type = "WarehouseReceive";
                dataTargetColl.Add(dataTarget);
                dc.DataTargetCollection = dataTargetColl.ToArray();
                ShipmentOrder pOrder = new ShipmentOrder();
                pOrder.OrderNumber = refPO;
                pOrder.ClientReference = refPO;
                shipment.DataContext = dc;
                //TODO 
                //Create Variable to allow for Return Authority
                CodeDescriptionPair codePair = new CodeDescriptionPair();
                codePair.Code = "REC";
                codePair.Description = "GOODS RECEIPT";
                pOrder.Type = codePair;
                ShipmentOrderWarehouse warehouse = new ShipmentOrderWarehouse();
                warehouse.Code = "SYD";
                warehouse.Name = "PCFS SYDNEY";
                pOrder.Warehouse = warehouse;
                List<Date> dateColl = new List<Date>();
                Date orderDate = new Date();
                orderDate.Type = DateType.BookingConfirmed;
                orderDate.Value = DateTime.Now.ToString("s");
                orderDate.IsEstimate = false;
                orderDate.IsEstimateSpecified = true;
                dateColl.Add(orderDate);
                pOrder.DateCollection = dateColl.ToArray();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                int i = 0;
                orderLine.Content = CollectionContent.Complete;
                List<ShipmentOrderOrderLineCollectionOrderLine> olColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                orderLine.ContentSpecified = true;
                foreach (DataRow dr in tb.Rows)
                {
                    i++;
                    ShipmentOrderOrderLineCollectionOrderLine oLine = new ShipmentOrderOrderLineCollectionOrderLine();
                    oLine.ExpectedQuantity = decimal.Parse(dr["QTY"].ToString());
                    oLine.ExpectedQuantitySpecified = true;

                    oLine.LineNumber = i;
                    oLine.LineNumberSpecified = true;
                    CodeDescriptionPair cdUnit = new CodeDescriptionPair();
                    cdUnit.Code = "UNT";
                    oLine.OrderedQtyUnit = cdUnit;
                    XUS.Product product = new XUS.Product();
                    product.Code = dr["CODE"].ToString(); ;
                    product.Description = dr["DESC"].ToString();
                    oLine.Product = product;
                    olColl.Add(oLine);
                }
                orderLine.OrderLine = olColl.ToArray();
                pOrder.OrderLineCollection = orderLine;
                List<OrganizationAddress> OrgAddColl = new List<OrganizationAddress>();
                OrganizationAddress oa = new OrganizationAddress();
                oa.AddressType = "ConsignorDocumentaryAddress";
                oa.OrganizationCode = senderCode;
                OrgAddColl.Add(oa);
                shipment.OrganizationAddressCollection = OrgAddColl.ToArray();
                shipment.Order = pOrder;
                uShipData.Shipment = shipment;
                uBody.BodyField = uShipData;
                uShipData.version = "1.1";
                uShipData.Shipment = shipment;
                String cwXML = Path.Combine(Globals.glOutputDir, senderCode + "PO" + pOrder.OrderNumber.Trim() + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(Globals.glOutputDir, senderCode + "PO" + pOrder.OrderNumber.Trim() + iFileCount + ".xml");
                }
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                StringWriter writer = new StringWriter();
                uInt.Body = uBody;
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                xSer.Serialize(outputCW, uInt, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
        }

        #endregion
        public NodeFile ConvertToCommonReceipt(string alertEmail)
        {
            NodeFile convertedReceipt = new NodeFile();

            convertedReceipt.IdendtityMatrix = new NodeFileIdendtityMatrix
            {
                CustomerId = Globals.glCustCode,
                SenderId = _senderID,
                DocumentType = "Receipt"
            };
            var receipt = new NodeFileWarehouseReceipt();
            receipt.Companies = GetCompaniesFromRow(_dt.Rows[0]).ToArray();
            receipt.Dates = GetDatesFromRow(_dt.Rows[0]).ToArray();
            var custRefField = ReturnStringFromList(_map, "RCPT", "CustomerReferences");
            receipt.CustomerReferences = !string.IsNullOrEmpty(ReturnValueFromTable(_dt.Rows[0], custRefField).ToString()) ? ReturnValueFromTable(_dt.Rows[0], custRefField).ToString() : string.Empty;
            var receiveRefField = ReturnStringFromList(_map, "RCPT", "ReceiveReference");
            receipt.ReceiveReference = !string.IsNullOrEmpty(ReturnValueFromTable(_dt.Rows[0], receiveRefField).ToString()) ? ReturnValueFromTable(_dt.Rows[0], receiveRefField).ToString() : string.Empty;
            List<OrderLineElement> receiptLines = new List<OrderLineElement>();
            var whouseField = ReturnStringFromList(_map, "RCPT", "WarehouseCode");
            if (!string.IsNullOrEmpty(whouseField))
            {
                //Hard coded temporarily
                //     receipt.WarehouseCode = ReturnValueFromTable(_dt.Rows[0], whouseField);
                receipt.WarehouseCode = "BNE";
            }
            int lineNo = 0;
            var prodCodeField = ReturnStringFromList(_map, "RCPT", "ProductCode");
            //if (!validProducts(_dt.Rows, prodCodeField))
            //{
            //    MailModule mm = new MailModule(_mailSettings);
            //    string msg = "Products Missing:" + Environment.NewLine + _errSummary;
            //    mm.SendMsg("", alertEmail, "Products Missing while Importing Order Receipt " + receipt.ReceiveReference, msg);
            //    return null;
            //}

            foreach (DataRow row in _dt.Rows)
            {

                OrderLineElement line = new OrderLineElement();
                lineNo++;
                line.LineNo = lineNo;
                line.LineNoSpecified = true;
                int qty = 0;
                var qtyField = ReturnStringFromList(_map, "RCPT", "ExpectedQuantity");
                if (int.TryParse(row[qtyField].ToString(), out qty))
                {
                    line.ExpectedQuantity = qty;
                }

                line.Product = new ProductElement
                {
                    Code = row[prodCodeField].ToString()
                };
                receiptLines.Add(line);
            }
            receipt.OrderLines = receiptLines.ToArray();
            convertedReceipt.WarehouseReceipt = receipt;
            return convertedReceipt;
        }

       

        #region Helpers
        private string ReturnValueFromTable(DataRow dataRow, string custRefField)
        {
            return dataRow[custRefField].ToString().Trim();
        }
        private List<DateElement> GetDatesFromRow(DataRow dataRow)
        {
            var etaField = ReturnStringFromList(_map, "RCPT", "ETA");
            if (!string.IsNullOrEmpty(etaField))
            {
                List<DateElement> Dates = new List<DateElement>();
                DateElement receiptDate = new DateElement();
                DateTime date = new DateTime();
                if (DateTime.TryParse(dataRow[etaField].ToString(), out date))
                {
                    receiptDate.EstimateDate = date.ToString("yyyy-MM-ddTHH\\:mm\\:ss");
                    receiptDate.DateType = DateElementDateType.Arrival;
                }
                Dates.Add(receiptDate);
                return Dates;

            }
            return null;

        }
        private static string ReturnStringFromList(List<MapOperation> map, string operation, string stringToFind)
        {
            var value = (from x in map
                         where x.MD_Type.Trim() == operation
                         && x.MD_FromField.Trim() == stringToFind
                         select x.MD_ToField.Trim()).FirstOrDefault();
            return value;
        }

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow)
        {
            List<CompanyElement> companies = new List<CompanyElement>{
                new CompanyElement{
                     CompanyOrgCode =SenderId,
                 CompanyType = CompanyElementCompanyType.Consignor} };
            //var supplierField = ReturnStringFromList(_map, "RCPT", "Supplier");
            //if (!string.IsNullOrEmpty(supplierField))
            //{
            //    CompanyElement supplier = new CompanyElement();
            //    supplier.CompanyCode = dataRow[supplierField].ToString();
            //    companies.Add(supplier);
            //}
            return companies;
        }

        #endregion
    }
}
