﻿using GLOSURF;
using PCFSatellite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace PCFSatellite
{
    public class GlobalSurf
    {
        #region members
        string _connString;
        string _err;

        #endregion
        #region properties
        public string ErrMsg
        {
            get
            { return _err; }
            set
            { _err = value; }
        }

        #endregion
        #region constructors
        public GlobalSurf(string connString)
        {
            _connString = connString;
        }
        #endregion
        #region methods

        public GLOSURF.Orders ReadFile(string fileName)
        {
            GLOSURF.Orders gsi_order = new GLOSURF.Orders();
            FileInfo processingFile = new FileInfo(fileName);
            using (FileStream fStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                XmlSerializer pcfsImport = new XmlSerializer(typeof(GLOSURF.Orders), "http://www.spscommerce.com/RSX");
                gsi_order = (GLOSURF.Orders)pcfsImport.Deserialize(fStream);
                fStream.Close();
            }
            return gsi_order;
        }
        public NodeFile ConvertNetSuiteOrder(string fileName, Customer_Profile cust)
        {
            var orders = ReadFile(fileName);
            var common = new NodeFile();
            const string dateFormat = "dd-MMM-yyyy HH:mm";
            if (orders.Order != null)
            {

                string tempNotes = string.Empty;
                var header = new NodeFileIdendtityMatrix();
                header.CustomerId = cust.P_RECIPIENTID;
                header.SenderId = cust.P_SENDERID;
                header.FileDateTime = DateTime.Now.ToString(dateFormat);
                header.DocumentType = "WarehouseOrder";
                header.OriginalFileName = fileName;
                header.PurposeCode = "IMP";
                common.IdendtityMatrix = header;
                var order = (from o in orders.Order
                             select o).FirstOrDefault();

                var commonOrder = new NodeFileWarehouseOrder();
                commonOrder.OrderNumber = order.Header.OrderHeader.PurchaseOrderNumber;
                commonOrder.WarehouseCode = !string.IsNullOrEmpty(order.Header.OrderHeader.Division)? order.Header.OrderHeader.Division : "SYD";
                DateTime date = new DateTime();
                var orderDates = (GetDatesFromOrder(order.Header.Dates.ToList()));
                var _createDate = new DateElement();
                _createDate.DateType = DateElementDateType.Ordered;
                _createDate.ActualDate = order.Header.OrderHeader.PurchaseOrderDate.ToString(dateFormat);
                orderDates.Add(_createDate);
                commonOrder.Dates = orderDates.ToArray();
                var compList = GetCompaniesFromOrder(order.Header.Address.ToList(), order.Header.Notes[0].Note);
                if (compList.Count == 0)
                {
                    return null;
                }
                compList.Add(new CompanyElement
                {
                    CompanyType = CompanyElementCompanyType.Consignor,
                    CompanyCode = cust.P_SENDERID
                });

                commonOrder.Companies = compList.ToArray();
                commonOrder.SpecialInstructions = GetNotesFromOrder(order.Header.Notes.ToList(), OrderTypeHeaderNotesNoteCode.SHP);
                commonOrder.OrderLines = GetorderLinesFromOrder(order.LineItem.ToList()).ToArray();
                common.WarehouseOrders = new List<NodeFileWarehouseOrder> { commonOrder }.ToArray();
            }
            return common;
        }
        #endregion
        #region helpers
        private string[] GetAddressFromText(string address, ref int counter)
        {
            if (counter > 10)
            {
                return null;
            }

            var addTemp = address.Replace(',', ' ');

            string[] add = RemoveBlanks(addTemp.Split(' '));
            counter++;

            try
            {
                //  add = address.Replace(',', ' ').Split(' ');
                //add = add.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                if (add.Length == 1)
                {
                    add = address.Split(',');
                }
                if (add.Length == 2)
                {
                    if (!add[1].All(Char.IsDigit))
                    {
                        add = GetAddressFromText(address.Replace(',', ' '), ref counter);
                    }


                }
            }
            catch (StackOverflowException)
            {
                add = null;
            }


            return add;

        }

        private string[] RemoveBlanks(string[] val)
        {
            List<string> valList = val.ToList();
            try
            {
                for (int i = 0; i < valList.Count; i++)
                {

                    if (string.IsNullOrEmpty(valList[i]))
                    {
                        valList.RemoveAt(i);
                    }

                }
            }
            catch (Exception)
            {

            }
            return valList.ToArray();
        }
        private List<DateElement> GetDatesFromOrder(List<GLOSURF.OrderTypeHeaderDates> dates)
        {
            const string dateFormat = "dd-MMM-yyyy HH:mm";
            List<DateElement> ordDates = new List<DateElement>();
            foreach (var d in dates)
            {
                var de = new DateElement
                {
                    ActualDate = d.Date.ToString(dateFormat)
                };
                switch (d.DateTimeQualifier)
                {
                    case GLOSURF.OrderTypeHeaderDatesDateTimeQualifier.Item002:
                        de.DateType = DateElementDateType.DeliverBy;
                        break;
                        //Can add more dates as needed
                }
                ordDates.Add(de);
            }

            return ordDates;

        }

        private List<CompanyElement> GetCompaniesFromOrder(List<GLOSURF.OrderTypeHeaderAddress> addresses, string notes)
        {
            List<CompanyElement> companies = new List<CompanyElement>();
            foreach (var address in addresses)
            {
                var comp = new CompanyElement();
                if (address.AddressTypeCode == OrderTypeHeaderAddressAddressTypeCode.ST)
                {
                    if (string.IsNullOrEmpty(address.AddressName) && string.IsNullOrEmpty(notes))
                    {
                        _err += "Delivery Address is missing and no Address in Notes";
                    }
                    if (address.AddressName.ToUpper().Contains("CASH SALE"))
                    {
                        comp = GetAddressFromNotes(notes);
                    }
                    else
                    {
                        comp.CompanyName = address.AddressName;
                        if (!string.IsNullOrEmpty(address.Address1))
                        {
                            comp.Address1 = address.Address1;
                            comp.Address2 = address.Address2;
                            comp.PostCode = !string.IsNullOrEmpty(address.PostalCode) ? address.PostalCode : string.Empty;
                            comp.City = !string.IsNullOrEmpty(address.City) ? address.City : string.Empty;
                        }
                    }
                    comp.CompanyType = CompanyElementCompanyType.DeliveryAddress;
                    companies.Add(comp);

                }

            }

            return companies;
        }

        private CompanyElement GetAddressFromNotes(string notes)
        {
            string[] cashAddress = notes.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
            string suburb = string.Empty;
            string[] substate = null;
            var comp = new CompanyElement();

            switch (cashAddress.Length)

            {
                case 3:
                    comp.CompanyName = cashAddress[0];
                    comp.Address1 = cashAddress[1];
                    try
                    {
                        int iCount = 0;
                        substate = GetAddressFromText(cashAddress[2], ref iCount);
                    }
                    catch (StackOverflowException)
                    {
                        substate = null;
                    }

                    break;
                case 4:
                    comp.CompanyName = cashAddress[0];
                    comp.Address1 = cashAddress[1];
                    if (!cashAddress[3].Replace(" ", string.Empty).All(Char.IsDigit))
                    {
                        if (cashAddress[3].ToUpper().Contains("PH:") || cashAddress[3].ToUpper().Contains("MOB:"))
                        {
                            comp.PhoneNo = cashAddress[3];
                            try
                            {
                                int iCount = 0;
                                substate = GetAddressFromText(cashAddress[2], ref iCount);
                            }
                            catch (StackOverflowException)
                            {
                                substate = null;
                            }
                        }
                        else
                        {
                            comp.Address2 = cashAddress[2];
                            try
                            {
                                int iCount = 0;
                                substate = GetAddressFromText(cashAddress[3], ref iCount);
                            }
                            catch (StackOverflowException)
                            {
                                substate = null;
                            }
                        }
                    }
                    else
                    {
                        comp.Address2 = cashAddress[2];
                        try
                        {
                            int iCount = 0;
                            substate = GetAddressFromText(cashAddress[3], ref iCount);
                        }
                        catch (StackOverflowException)
                        {
                            substate = null;
                        }
                    }

                    break;
                case 5:
                    comp.ContactName = cashAddress[0];
                    comp.CompanyName = cashAddress[1];
                    comp.Address1 = cashAddress[2];
                    comp.Address2 = string.Empty;
                    if (cashAddress[4].Replace(" ", string.Empty).All(Char.IsDigit))
                    {
                        comp.PhoneNo = cashAddress[4];
                    }
                    else
                    {
                        try
                        {
                            int iCount = 0;
                            substate = GetAddressFromText(cashAddress[4], ref iCount);
                            if (substate[substate.Length - 1].All(Char.IsDigit))
                            {
                                comp.City = cashAddress[3];
                            }
                        }
                        catch (StackOverflowException)
                        {
                            substate = null;
                        }

                    }

                    break;
                case 6:
                    comp.ContactName = cashAddress[0];
                    comp.CompanyName = cashAddress[1];
                    comp.Address1 = cashAddress[2];
                    comp.Address2 = string.Empty;
                    comp.City = cashAddress[3];
                    string[] substate6 = cashAddress[4].Split(',');
                    comp.PhoneNo = cashAddress[5];
                    break;
            }

            if (substate != null)
            {
                if (substate.Length == 2)
                {
                    comp.PostCode = substate[1];
                    comp.State = substate[0];

                }
                if (substate.Length == 3)
                {
                    comp.PostCode = substate[2];
                    comp.State = substate[1];
                    comp.City = substate[0];
                }
                if (substate.Length > 3)
                {
                    comp.PostCode = substate[substate.Length - 1];
                    comp.State = substate[substate.Length - 2];
                    for (int i = 0; i < substate.Length - 2; i++)
                    {
                        suburb += substate[i] + " ";
                    }
                    comp.City = suburb;
                }
            }
            return comp;
        }

        private List<OrderLineElement> GetorderLinesFromOrder(List<OrderTypeLineItem> lines)
        {
            List<OrderLineElement> ordLines = new List<OrderLineElement>();
            foreach (var line in lines)
            {
                var newLine = new OrderLineElement();
                newLine.LineNo = int.Parse(line.OrderLine.LineSequenceNumber);
                newLine.Product = new ProductElement
                {
                    Barcode = line.OrderLine.EAN,
                    Code = line.OrderLine.VendorPartNumber,
                    Description = (from d in line.ProductOrItemDescription
                                   where d.ProductCharacteristicCode == OrderTypeLineItemProductOrItemDescriptionProductCharacteristicCode.Item08
                                   select d.ProductDescription).FirstOrDefault()
                };
                newLine.PackageQty = line.OrderLine.OrderQty;
                newLine.PackageQtySpecified = true;
                newLine.PackageUnit = "EA";
                ordLines.Add(newLine);
            }
            return ordLines;
        }
        private string GetNotesFromOrder(List<OrderTypeHeaderNotes> notes, OrderTypeHeaderNotesNoteCode code)
        {
            if (notes.Count > 0)
            {
                var note = (from n in notes
                            where n.NoteCode == code
                            select n).FirstOrDefault();
                if (note != null)
                {
                    return note.Note;
                }
            }
            return null;
        }
        #endregion



        //public TransReference GSI_Orders(string xmlFile, CustProfileRecord cust)
        //{
        //    TransReference result = new TransReference();
        //    result = null;
        //    GLOSURF.Orders gsi_order = new GLOSURF.Orders();
        //    FileInfo processingFile = new FileInfo(xmlFile);
        //    ProcessResult thisResult = new ProcessResult();
        //    thisResult.Processed = true;
        //    using (FileStream fStream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        //    {
        //        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
        //        ns.Add("", "");
        //        XmlSerializer pcfsImport = new XmlSerializer(typeof(GLOSURF.Orders), "http://www.spscommerce.com/RSX");


        //        gsi_order = (GLOSURF.Orders)pcfsImport.Deserialize(fStream);
        //        fStream.Close();
        //    }

        //    NodeFile common = new NodeFile();
        //    foreach (GLOSURF.OrderType order in gsi_order.Order)
        //    {
        //        var commonOrder = new NodeFileWarehouseOrder();
        //        commonOrder.OrderNumber = order.Header.OrderHeader.PurchaseOrderNumber;
        //        List<DateElement> orderDates = new List<DateElement>();
        //        orderDates.Add(new DateElement
        //        {
        //            ActualDate = order.Header.OrderHeader.date
        //        })
        //        try
        //        {

        //            so_orderdate.Value = order.Header.OrderHeader.PurchaseOrderDate;
        //            foreach (GLOSURF.OrderTypeHeaderAddress customer in order.Header.Address)
        //            {
        //                if (customer.AddressTypeCode == OrderTypeHeaderAddressAddressTypeCode.ST)
        //                {
        //                    so_customer.Value = customer.AddressName;
        //                    if (!string.IsNullOrEmpty(customer.Address1))
        //                    {
        //                        so_delstreet1.Value = customer.Address1;
        //                        so_delstreet2.Value = customer.Address2;
        //                        if (!string.IsNullOrEmpty(customer.PostalCode))
        //                        {
        //                            so_delpocode.Value = customer.PostalCode;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception();
        //                        }

        //                        if (!string.IsNullOrEmpty(customer.City))
        //                        {
        //                            so_delsuburb.Value = customer.City;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (string.IsNullOrEmpty(order.Header.Notes[0].Note))
        //                        {
        //                            throw new Exception();
        //                        }
        //                        else
        //                        {
        //                            tempNotes = order.Header.Notes[0].Note;
        //                            string[] address = order.Header.Notes[0].Note.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
        //                            string suburb = string.Empty;
        //                            string[] substate = null;
        //                            switch (address.Length)
        //                            {
        //                                case 3:
        //                                    so_customer.Value = address[0];
        //                                    so_delstreet1.Value = address[1];
        //                                    substate = GetAddressFromText(address[2]);
        //                                    break;
        //                                case 4:
        //                                    so_customer.Value = address[0];
        //                                    so_delstreet1.Value = address[1];
        //                                    if (!address[3].Replace(" ", string.Empty).All(Char.IsDigit))
        //                                    {
        //                                        if (address[3].ToUpper().Contains("PH:") || address[3].ToUpper().Contains("MOB:"))
        //                                        {
        //                                            so_ph.Value = address[3];
        //                                            substate = GetAddressFromText(address[2]);
        //                                        }
        //                                        else
        //                                        {
        //                                            so_delstreet2.Value = address[2];
        //                                            substate = GetAddressFromText(address[3]);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        so_delstreet2.Value = address[2];
        //                                        substate = GetAddressFromText(address[3]);
        //                                    }

        //                                    break;
        //                                case 5:
        //                                    so_customer.Value = address[0];
        //                                    so_company.Value = address[1];
        //                                    so_delstreet1.Value = address[2];
        //                                    so_delstreet2.Value = string.Empty;
        //                                    if (address[4].Replace(" ", string.Empty).All(Char.IsDigit))
        //                                    {
        //                                        so_ph.Value = address[4];
        //                                    }
        //                                    else
        //                                    {
        //                                        substate = GetAddressFromText(address[4]);
        //                                        if (substate[substate.Length - 1].All(Char.IsDigit))
        //                                        {
        //                                            so_delsuburb.Value = address[3];
        //                                        }
        //                                    }

        //                                    break;
        //                                case 6:
        //                                    so_customer.Value = address[0];
        //                                    so_company.Value = address[1];
        //                                    so_delstreet1.Value = address[2];
        //                                    so_delstreet2.Value = string.Empty;
        //                                    so_delsuburb.Value = address[3];
        //                                    string[] substate6 = address[4].Split(',');
        //                                    so_ph.Value = address[5];
        //                                    break;
        //                            }
        //                            if (substate.Length == 2)
        //                            {
        //                                so_delpocode.Value = substate[1];
        //                                so_delstate.Value = substate[0];

        //                            }
        //                            if (substate.Length == 3)
        //                            {
        //                                so_delpocode.Value = substate[2];
        //                                so_delstate.Value = substate[1];
        //                                so_delsuburb.Value = substate[0];
        //                            }
        //                            if (substate.Length > 3)
        //                            {
        //                                so_delpocode.Value = substate[substate.Length - 1];
        //                                so_delstate.Value = substate[substate.Length - 2];
        //                                for (int i = 0; i < substate.Length - 2; i++)
        //                                {
        //                                    suburb += substate[i] + " ";
        //                                }
        //                                so_delsuburb.Value = suburb;
        //                            }

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            string strEx = ex.GetType().Name;
        //            string errMsg = "Unabled to Process Order: " + orderNo.Value + Environment.NewLine;
        //            errMsg += "Customer delivery details needed for: " + so_customer.Value + Environment.NewLine;
        //            errMsg += tempNotes + Environment.NewLine;
        //            errMsg += "Please fix and resubmit the order again.";
        //            string notify = cust.P_NotifyEmail;
        //            MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //            mailModule.SendMsg(string.Empty, notify, "Unable to process Order: " + orderNo.Value, errMsg);
        //            NodeResources.MoveFile(xmlFile, Globals.glFailPath);
        //            result = new TransReference();
        //            result.Ref3Type = TransReference.RefType.Error;
        //            result.Reference3 = strEx + ": " + errMsg;
        //            result.Ref2Type = TransReference.RefType.Order;
        //            result.Reference2 = orderNo.Value.ToString();
        //            return result;
        //        }


        //        so_OrgCode.Value = "GLOSURSYD";
        //        if (order.Header.Notes != null)
        //        {
        //            foreach (GLOSURF.OrderTypeHeaderNotes note in order.Header.Notes)
        //            {
        //                switch (note.NoteCode)
        //                {
        //                    case OrderTypeHeaderNotesNoteCode.PCK:
        //                        if (!string.IsNullOrEmpty(note.Note))
        //                        {
        //                            so_pickingNotes.Value = note.Note;
        //                        }

        //                        break;
        //                    case OrderTypeHeaderNotesNoteCode.SHP:
        //                        if (!string.IsNullOrEmpty(note.Note))
        //                        {
        //                            so_deliveryNotes.Value = note.Note;
        //                        }

        //                        break;
        //                    case OrderTypeHeaderNotesNoteCode.GEN:
        //                        break;

        //                }
        //            }
        //        }
        //        foreach (OrderTypeHeaderDates date in order.Header.Dates)
        //        {
        //            if (date.DateTimeQualifier == OrderTypeHeaderDatesDateTimeQualifier.Item002)
        //            {
        //                so_shipdate.Value = date.Date;
        //            }

        //        }
        //        try
        //        {
        //            using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
        //            {
        //                addPCFSSo.Connection = sqlConn;
        //                if (sqlConn.State == ConnectionState.Open)
        //                {
        //                    sqlConn.Close();
        //                }
        //                sqlConn.Open();

        //                addPCFSSo.ExecuteNonQuery();
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            string strEx = ex.GetType().Name;

        //            string errMsg = "Error found when processing " + xmlFile + Environment.NewLine;

        //            errMsg += "Error Type: " + strEx + Environment.NewLine;
        //            errMsg += "Message: " + ex.Message + Environment.NewLine;
        //            errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
        //            errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
        //            errMsg += "Source: " + ex.Source;
        //            MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //            mailModule.SendMsg(xmlFile, Globals.glAlertsTo, "PCFS Unhandled Exception: ProcessCustomXML", ex.Message);
        //            result.Ref3Type = TransReference.RefType.Error;
        //            result.Reference3 = strEx + ": " + errMsg;
        //            result.Ref2Type = TransReference.RefType.Order;
        //            result.Reference2 = orderNo.Value.ToString();

        //            return result;
        //        }

        //        sol_id.Value = so_id.Value;
        //        foreach (GLOSURF.OrderTypeLineItem line in order.LineItem)
        //        {
        //            lineNo.Value = line.OrderLine.LineSequenceNumber;
        //            itemBarcode.Value = line.OrderLine.EAN;
        //            partNo.Value = line.OrderLine.VendorPartNumber;
        //            soqty.Value = line.OrderLine.OrderQty;
        //            foreach (OrderTypeLineItemProductOrItemDescription itemDetail in line.ProductOrItemDescription)
        //            {
        //                if (itemDetail.ProductCharacteristicCode == OrderTypeLineItemProductOrItemDescriptionProductCharacteristicCode.Item08)
        //                {
        //                    productName.Value = itemDetail.ProductDescription;
        //                }
        //            }
        //            using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
        //            {
        //                if (sqlConn.State == ConnectionState.Open)
        //                {
        //                    sqlConn.Close();
        //                }
        //                sqlConn.Open();
        //                addPCFSSOL.Connection = sqlConn;
        //                addPCFSSOL.ExecuteNonQuery();

        //            }
        //        }
        //        Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
        //        result = CargowiseFunctions.CreateCWSOXML((Guid)so_id.Value);
        //    }

        //    common.IdendtityMatrix = new NodeFileIdendtityMatrix
        //    {
        //        CustomerId = cust.P_Recipientid,
        //        SenderId = cust.P_Senderid,
        //        DocumentType = "PickingOrder",
        //        FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm"),
        //        OriginalFileName = xmlFile,
        //         EventCode= "IMP"
        //    };

        //    SqlCommand addPCFSSo = new SqlCommand();

        //    addPCFSSo.CommandText = "AddPCFS_SO";
        //    addPCFSSo.CommandType = CommandType.StoredProcedure;
        //    SqlParameter orderNo = addPCFSSo.Parameters.Add("@ORDERNO", SqlDbType.NChar, 20);
        //    SqlParameter so_orderdate = addPCFSSo.Parameters.Add("@SO_ORDERDATE", SqlDbType.DateTime);
        //    SqlParameter so_shipdate = addPCFSSo.Parameters.Add("@SO_SHIPDATE", SqlDbType.DateTime);
        //    SqlParameter so_customer = addPCFSSo.Parameters.Add("@SO_CUSTOMER", SqlDbType.VarChar, 100);
        //    SqlParameter so_delstreet1 = addPCFSSo.Parameters.Add("@SO_DELSTREET1", SqlDbType.VarChar, 100);
        //    SqlParameter so_delstreet2 = addPCFSSo.Parameters.Add("@SO_DELSTREET2", SqlDbType.VarChar, 100);
        //    SqlParameter so_delpocode = addPCFSSo.Parameters.Add("@SO_DELPOCODE", SqlDbType.Char, 4);
        //    SqlParameter so_delsuburb = addPCFSSo.Parameters.Add("@SO_DELSUBURB", SqlDbType.VarChar, 100);
        //    SqlParameter so_delstate = addPCFSSo.Parameters.Add("@SO_DELSTATE", SqlDbType.VarChar, 20);
        //    SqlParameter so_deliveryNotes = addPCFSSo.Parameters.Add("@SO_DELIVERYNOTES", SqlDbType.VarChar, -1);
        //    SqlParameter so_pickingNotes = addPCFSSo.Parameters.Add("@SO_PICKINGNOTES", SqlDbType.VarChar, -1);
        //    SqlParameter so_ph = addPCFSSo.Parameters.Add("@SO_PH", SqlDbType.VarChar, 20);
        //    SqlParameter so_company = addPCFSSo.Parameters.Add("@SO_COMPANY", SqlDbType.VarChar, 100);
        //    SqlParameter so_id = addPCFSSo.Parameters.Add("@SO_ID", SqlDbType.UniqueIdentifier);
        //    SqlParameter so_deliveryEntity = addPCFSSo.Parameters.Add("@SO_DELIVERYENTITY", SqlDbType.VarChar, 100);
        //    SqlParameter so_OrgCode = addPCFSSo.Parameters.Add("@SO_ORGCODE", SqlDbType.Char, 15);
        //    so_id.Direction = ParameterDirection.Output;

        //    SqlCommand addPCFSSOL = new SqlCommand("AddPCFS_SOLine");
        //    addPCFSSOL.CommandType = CommandType.StoredProcedure;
        //    SqlParameter sol_id = addPCFSSOL.Parameters.Add("@SO_ID", SqlDbType.UniqueIdentifier);
        //    SqlParameter soqty = addPCFSSOL.Parameters.Add("@QTY", SqlDbType.Int);
        //    SqlParameter itemBarcode = addPCFSSOL.Parameters.Add("@ITEMBARCODE", SqlDbType.VarChar, 20);
        //    SqlParameter lineNo = addPCFSSOL.Parameters.Add("@LINENO", SqlDbType.Int);
        //    SqlParameter partNo = addPCFSSOL.Parameters.Add("@PARTNUM", SqlDbType.VarChar, 50);
        //    SqlParameter productName = addPCFSSOL.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar, 100);
        //    string tempNotes = string.Empty;
        //    foreach (GLOSURF.OrderType order in gsi_order.Order)
        //    {
        //        try
        //        {
        //            orderNo.Value = order.Header.OrderHeader.PurchaseOrderNumber;
        //            so_orderdate.Value = order.Header.OrderHeader.PurchaseOrderDate;
        //            foreach (GLOSURF.OrderTypeHeaderAddress customer in order.Header.Address)
        //            {
        //                if (customer.AddressTypeCode == OrderTypeHeaderAddressAddressTypeCode.ST)
        //                {
        //                    so_customer.Value = customer.AddressName;
        //                    if (!string.IsNullOrEmpty(customer.Address1))
        //                    {
        //                        so_delstreet1.Value = customer.Address1;
        //                        so_delstreet2.Value = customer.Address2;
        //                        if (!string.IsNullOrEmpty(customer.PostalCode))
        //                        {
        //                            so_delpocode.Value = customer.PostalCode;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception();
        //                        }

        //                        if (!string.IsNullOrEmpty(customer.City))
        //                        {
        //                            so_delsuburb.Value = customer.City;
        //                        }
        //                        else
        //                        {
        //                            throw new Exception();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (string.IsNullOrEmpty(order.Header.Notes[0].Note))
        //                        {
        //                            throw new Exception();
        //                        }
        //                        else
        //                        {
        //                            tempNotes = order.Header.Notes[0].Note;
        //                            string[] address = order.Header.Notes[0].Note.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
        //                            string suburb = string.Empty;
        //                            string[] substate = null;
        //                            switch (address.Length)
        //                            {
        //                                case 3:
        //                                    so_customer.Value = address[0];
        //                                    so_delstreet1.Value = address[1];
        //                                    substate = GetAddressFromText(address[2]);
        //                                    break;
        //                                case 4:
        //                                    so_customer.Value = address[0];
        //                                    so_delstreet1.Value = address[1];
        //                                    if (!address[3].Replace(" ", string.Empty).All(Char.IsDigit))
        //                                    {
        //                                        if (address[3].ToUpper().Contains("PH:") || address[3].ToUpper().Contains("MOB:"))
        //                                        {
        //                                            so_ph.Value = address[3];
        //                                            substate = GetAddressFromText(address[2]);
        //                                        }
        //                                        else
        //                                        {
        //                                            so_delstreet2.Value = address[2];
        //                                            substate = GetAddressFromText(address[3]);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        so_delstreet2.Value = address[2];
        //                                        substate = GetAddressFromText(address[3]);
        //                                    }

        //                                    break;
        //                                case 5:
        //                                    so_customer.Value = address[0];
        //                                    so_company.Value = address[1];
        //                                    so_delstreet1.Value = address[2];
        //                                    so_delstreet2.Value = string.Empty;
        //                                    if (address[4].Replace(" ", string.Empty).All(Char.IsDigit))
        //                                    {
        //                                        so_ph.Value = address[4];
        //                                    }
        //                                    else
        //                                    {
        //                                        substate = GetAddressFromText(address[4]);
        //                                        if (substate[substate.Length - 1].All(Char.IsDigit))
        //                                        {
        //                                            so_delsuburb.Value = address[3];
        //                                        }
        //                                    }

        //                                    break;
        //                                case 6:
        //                                    so_customer.Value = address[0];
        //                                    so_company.Value = address[1];
        //                                    so_delstreet1.Value = address[2];
        //                                    so_delstreet2.Value = string.Empty;
        //                                    so_delsuburb.Value = address[3];
        //                                    string[] substate6 = address[4].Split(',');
        //                                    so_ph.Value = address[5];
        //                                    break;
        //                            }
        //                            if (substate.Length == 2)
        //                            {
        //                                so_delpocode.Value = substate[1];
        //                                so_delstate.Value = substate[0];

        //                            }
        //                            if (substate.Length == 3)
        //                            {
        //                                so_delpocode.Value = substate[2];
        //                                so_delstate.Value = substate[1];
        //                                so_delsuburb.Value = substate[0];
        //                            }
        //                            if (substate.Length > 3)
        //                            {
        //                                so_delpocode.Value = substate[substate.Length - 1];
        //                                so_delstate.Value = substate[substate.Length - 2];
        //                                for (int i = 0; i < substate.Length - 2; i++)
        //                                {
        //                                    suburb += substate[i] + " ";
        //                                }
        //                                so_delsuburb.Value = suburb;
        //                            }

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            string strEx = ex.GetType().Name;
        //            string errMsg = "Unabled to Process Order: " + orderNo.Value + Environment.NewLine;
        //            errMsg += "Customer delivery details needed for: " + so_customer.Value + Environment.NewLine;
        //            errMsg += tempNotes + Environment.NewLine;
        //            errMsg += "Please fix and resubmit the order again.";
        //            string notify = cust.P_NotifyEmail;
        //            MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //            mailModule.SendMsg(string.Empty, notify, "Unable to process Order: " + orderNo.Value, errMsg);
        //            NodeResources.MoveFile(xmlFile, Globals.glFailPath);
        //            result = new TransReference();
        //            result.Ref3Type = TransReference.RefType.Error;
        //            result.Reference3 = strEx + ": " + errMsg;
        //            result.Ref2Type = TransReference.RefType.Order;
        //            result.Reference2 = orderNo.Value.ToString();
        //            return result;
        //        }


        //        so_OrgCode.Value = "GLOSURSYD";
        //        if (order.Header.Notes != null)
        //        {
        //            foreach (GLOSURF.OrderTypeHeaderNotes note in order.Header.Notes)
        //            {
        //                switch (note.NoteCode)
        //                {
        //                    case OrderTypeHeaderNotesNoteCode.PCK:
        //                        if (!string.IsNullOrEmpty(note.Note))
        //                        {
        //                            so_pickingNotes.Value = note.Note;
        //                        }

        //                        break;
        //                    case OrderTypeHeaderNotesNoteCode.SHP:
        //                        if (!string.IsNullOrEmpty(note.Note))
        //                        {
        //                            so_deliveryNotes.Value = note.Note;
        //                        }

        //                        break;
        //                    case OrderTypeHeaderNotesNoteCode.GEN:
        //                        break;

        //                }
        //            }
        //        }
        //        foreach (OrderTypeHeaderDates date in order.Header.Dates)
        //        {
        //            if (date.DateTimeQualifier == OrderTypeHeaderDatesDateTimeQualifier.Item002)
        //            {
        //                so_shipdate.Value = date.Date;
        //            }

        //        }
        //        try
        //        {
        //            using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
        //            {
        //                addPCFSSo.Connection = sqlConn;
        //                if (sqlConn.State == ConnectionState.Open)
        //                {
        //                    sqlConn.Close();
        //                }
        //                sqlConn.Open();

        //                addPCFSSo.ExecuteNonQuery();
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            string strEx = ex.GetType().Name;

        //            string errMsg = "Error found when processing " + xmlFile + Environment.NewLine;

        //            errMsg += "Error Type: " + strEx + Environment.NewLine;
        //            errMsg += "Message: " + ex.Message + Environment.NewLine;
        //            errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
        //            errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
        //            errMsg += "Source: " + ex.Source;
        //            MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //            mailModule.SendMsg(xmlFile, Globals.glAlertsTo, "PCFS Unhandled Exception: ProcessCustomXML", ex.Message);
        //            result.Ref3Type = TransReference.RefType.Error;
        //            result.Reference3 = strEx + ": " + errMsg;
        //            result.Ref2Type = TransReference.RefType.Order;
        //            result.Reference2 = orderNo.Value.ToString();

        //            return result;
        //        }

        //        sol_id.Value = so_id.Value;
        //        foreach (GLOSURF.OrderTypeLineItem line in order.LineItem)
        //        {
        //            lineNo.Value = line.OrderLine.LineSequenceNumber;
        //            itemBarcode.Value = line.OrderLine.EAN;
        //            partNo.Value = line.OrderLine.VendorPartNumber;
        //            soqty.Value = line.OrderLine.OrderQty;
        //            foreach (OrderTypeLineItemProductOrItemDescription itemDetail in line.ProductOrItemDescription)
        //            {
        //                if (itemDetail.ProductCharacteristicCode == OrderTypeLineItemProductOrItemDescriptionProductCharacteristicCode.Item08)
        //                {
        //                    productName.Value = itemDetail.ProductDescription;
        //                }
        //            }
        //            using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
        //            {
        //                if (sqlConn.State == ConnectionState.Open)
        //                {
        //                    sqlConn.Close();
        //                }
        //                sqlConn.Open();
        //                addPCFSSOL.Connection = sqlConn;
        //                addPCFSSOL.ExecuteNonQuery();

        //            }
        //        }
        //        Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
        //        result = CargowiseFunctions.CreateCWSOXML((Guid)so_id.Value);
        //    }

        //    return result;
        //}

        //public TransReference GSI_Process_Pick(string xmlFile, CustProfileRecord cust)
        //{
        //    TransReference result = new PCFSatellite.TransReference();
        //    UniversalInterchange toConvert;
        //    try
        //    {
        //        string shipmentNo = "";
        //        using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
        //        {
        //            XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
        //            toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
        //            fStream.Close();
        //        }
        //        XUS.Shipment cwShipment = new XUS.Shipment();
        //        String senderID = toConvert.Header.SenderID;
        //        cwShipment = toConvert.Body.BodyField.Shipment;
        //        DataContext dc = new DataContext();
        //        dc = cwShipment.DataContext;

        //        DataSource[] dscoll;
        //        dscoll = dc.DataSourceCollection;
        //        string activityReply = string.Empty;
        //        string methodReply = string.Empty;
        //        List<GLOSURF.Shipments> gloShipmentColl = new List<GLOSURF.Shipments>();
        //        bool pickFound = false;
        //        foreach (DataSource ds in dscoll)
        //        {
        //            if (ds.Type == "WarehouseOrder")
        //            {
        //                pickFound = true;
        //                shipmentNo = ds.Key;
        //            }
        //        }
        //        if (pickFound)
        //        {


        //            GLOSURF.ShipmentHeader header = new ShipmentHeader();
        //            GLOSURF.ShipmentHeaderShipmentHeader shipmentHeader = new ShipmentHeaderShipmentHeader();
        //            shipmentHeader.TradingPartnerId = "362ALLGLOBALSUR";
        //            shipmentHeader.BillOfLadingNumber = shipmentNo;
        //            shipmentHeader.ShipDate = DateTime.Now;
        //            shipmentHeader.ShipmentIdentification = shipmentNo;

        //            shipmentHeader.TsetPurposeCode = ShipmentHeaderShipmentHeaderTsetPurposeCode.Item06;
        //            shipmentHeader.TsetPurposeCodeSpecified = true;
        //            header.ShipmentHeader1 = shipmentHeader;
        //            List<GLOSURF.ShipmentHeaderDates> datesColl = new List<ShipmentHeaderDates>();
        //            GLOSURF.ShipmentHeaderDates dates;

        //            foreach (var mileStone in cwShipment.MilestoneCollection)
        //            {
        //                dates = new ShipmentHeaderDates();

        //                switch (mileStone.EventCode)
        //                {
        //                    case "WHE":  //Warehouse Entered Date
        //                        dates.Date = DateTime.Parse(mileStone.ActualDate);
        //                        dates.DateSpecified = true;
        //                        dates.DateTimeQualifier = ShipmentHeaderDatesDateTimeQualifier.Item006;
        //                        dates.DateTimeQualifierSpecified = true;

        //                        datesColl.Add(dates);
        //                        break;


        //                    case "FIN":  // Warehouse Picking Finalized
        //                        if (mileStone.ActualDate != null)
        //                        {
        //                            dates.Date = DateTime.Parse(mileStone.ActualDate);
        //                            dates.DateSpecified = true;
        //                            dates.DateTimeQualifier = ShipmentHeaderDatesDateTimeQualifier.Item037;
        //                            dates.DateTimeQualifierSpecified = true;
        //                            datesColl.Add(dates);
        //                        }
        //                        break;
        //                }



        //            }
        //            header.Dates = datesColl.ToArray();

        //            List<GLOSURF.ShipmentHeaderAddress> addressColl = new List<ShipmentHeaderAddress>();
        //            foreach (OrganizationAddress org in cwShipment.OrganizationAddressCollection)
        //            {
        //                GLOSURF.ShipmentHeaderAddress address = new ShipmentHeaderAddress();
        //                try
        //                {
        //                    switch (org.AddressType)
        //                    {
        //                        case "ConsignorDocumentaryAddress":
        //                            address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.BOWN;

        //                            break;

        //                        case "ConsigneeAddress":
        //                            address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.CN;

        //                            break;
        //                        case "TransportCompanyDocumentaryAddress":
        //                            address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.TP;

        //                            break;
        //                        case "ConsignorPickupDeliveryAddress":
        //                            address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.WH;

        //                            break;
        //                    }
        //                    address.AddressTypeCodeSpecified = true;
        //                    address.AddressName = org.CompanyName;
        //                    if (org.Address1 != null)
        //                    {
        //                        address.Address1 = org.Address1;
        //                    }

        //                    if (org.Address2 != null)
        //                    {
        //                        address.Address2 = org.Address2;
        //                    }

        //                    if (org.City != null)
        //                    {
        //                        address.City = org.City;
        //                    }

        //                    if (org.Postcode != null)
        //                    {
        //                        address.PostalCode = org.Postcode;
        //                    }

        //                    if (org.State != null)
        //                    {
        //                        address.State = org.State;
        //                    }

        //                    if (org.Country != null)
        //                    {
        //                        address.Country = org.Country.Name;
        //                    }

        //                    addressColl.Add(address);
        //                }
        //                catch (Exception ex)
        //                {
        //                    string strEx = ex.GetType().Name;
        //                    string errMsg = "Unabled to Add Addresses: ";
        //                    MailModule mailModule = new MailModule(Globals.MailServerSettings);
        //                    mailModule.SendMsg(xmlFile, Globals.glAlertsTo, cust.C_code + ": Unable to Add Addresses: ", errMsg);


        //                }
        //            }
        //            header.Address = addressColl.ToArray();
        //            List<GLOSURF.ShipmentHeaderQuantityAndWeight> quantColl = new List<ShipmentHeaderQuantityAndWeight>();
        //            GLOSURF.ShipmentHeaderQuantityAndWeight quant = new ShipmentHeaderQuantityAndWeight();
        //            switch (cwShipment.TotalNoOfPacksPackageType.Code)
        //            {
        //                case "PCE":
        //                    quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCS;
        //                    break;
        //                default:
        //                    quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCS;
        //                    break;

        //            }
        //            quant.LadingQuantity = cwShipment.TotalNoOfPacks;
        //            quant.LadingQuantitySpecified = true;
        //            quant.Volume = cwShipment.TotalVolume;
        //            quant.VolumeSpecified = true;
        //            quant.VolumeUOM = ShipmentHeaderQuantityAndWeightVolumeUOM.CR;
        //            quant.VolumeUOMSpecified = true;
        //            quant.Weight = cwShipment.TotalWeight;
        //            quant.WeightSpecified = true;
        //            quant.WeightUOM = ShipmentHeaderQuantityAndWeightWeightUOM.KG;
        //            quant.WeightUOMSpecified = true;
        //            switch (cwShipment.TotalNoOfPacksPackageType.Code)
        //            {
        //                case "PCE":
        //                    quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCS;
        //                    break;
        //                case "PAK":
        //                    quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCK;
        //                    break;
        //                case "CTN":
        //                    quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.CTN;
        //                    break;
        //                case "PLT":
        //                    quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PLT;
        //                    break;
        //            }

        //            quant.PackingMediumSpecified = true;
        //            quantColl.Add(quant);
        //            header.QuantityAndWeight = quantColl.ToArray();
        //            GLOSURF.ShipmentOrderLevel orderLevel = new ShipmentOrderLevel();
        //            GLOSURF.ShipmentOrderLevelOrderHeader orderHeader = new ShipmentOrderLevelOrderHeader();
        //            orderHeader.PurchaseOrderNumber = cwShipment.Order.OrderNumber;
        //            List<GLOSURF.ShipmentHeaderReferences> referencesColl = new List<ShipmentHeaderReferences>();
        //            GLOSURF.ShipmentHeaderReferences references = new ShipmentHeaderReferences();
        //            references.ReferenceID = shipmentNo;
        //            references.ReferenceQual = ShipmentHeaderReferencesReferenceQual.Item2I;
        //            references.ReferenceQualSpecified = true;
        //            referencesColl.Add(references);
        //            references = new ShipmentHeaderReferences();
        //            references.ReferenceID = cwShipment.Order.TransportReference;
        //            references.ReferenceQual = ShipmentHeaderReferencesReferenceQual.CI;
        //            references.ReferenceQualSpecified = true;
        //            referencesColl.Add(references);
        //            header.References = referencesColl.ToArray();
        //            if (!string.IsNullOrEmpty(cwShipment.LocalProcessing.DeliveryRequiredBy))
        //            {
        //                orderHeader.DeliveryDate = DateTime.Parse(cwShipment.LocalProcessing.DeliveryRequiredBy);
        //                orderHeader.DeliveryDateSpecified = true;
        //            }
        //            else
        //            {
        //                orderHeader.DeliveryDateSpecified = false;
        //            }
        //            orderLevel.OrderHeader = orderHeader;
        //            GLOSURF.ShipmentOrderLevelPackLevel packLevel = new ShipmentOrderLevelPackLevel();
        //            GLOSURF.ShipmentOrderLevelPackLevelPack pack = new ShipmentOrderLevelPackLevelPack();
        //            pack.PackLevelType = ShipmentOrderLevelPackLevelPackPackLevelType.P;
        //            pack.PackLevelTypeSpecified = true;
        //            packLevel.Pack = pack;
        //            List<GLOSURF.ShipmentOrderLevelPackLevelItemLevel> itemLevelColl = new List<ShipmentOrderLevelPackLevelItemLevel>();
        //            bool partShip = false;
        //            foreach (ShipmentOrderOrderLineCollectionOrderLine line in cwShipment.Order.OrderLineCollection.OrderLine)
        //            {
        //                GLOSURF.ShipmentOrderLevelPackLevelItemLevel item = new ShipmentOrderLevelPackLevelItemLevel();
        //                ShipmentOrderLevelPackLevelItemLevelShipmentLine sline = new ShipmentOrderLevelPackLevelItemLevelShipmentLine();
        //                sline.LineSequenceNumber = line.LineNumber.ToString();

        //                sline.BuyerPartNumber = line.Product.Code;
        //                sline.ShipQty = line.QuantityMet;
        //                sline.ShipQtySpecified = true;
        //                sline.OrderQty = line.OrderedQty;
        //                sline.OrderQtySpecified = true;
        //                sline.ShipQtyUOM = ShipmentOrderLevelPackLevelItemLevelShipmentLineShipQtyUOM.PC;
        //                sline.ShipQtyUOMSpecified = true;
        //                item.ShipmentLine = sline;
        //                GLOSURF.ShipmentOrderLevelPackLevelItemLevelCarrierInformation carrier = new ShipmentOrderLevelPackLevelItemLevelCarrierInformation();

        //                if (line.ShortfallQuantity > 0)
        //                {
        //                    partShip = true;
        //                    carrier.StatusCode = ShipmentOrderLevelPackLevelItemLevelCarrierInformationStatusCode.PR;

        //                }
        //                else
        //                {
        //                    carrier.StatusCode = ShipmentOrderLevelPackLevelItemLevelCarrierInformationStatusCode.CL;

        //                }

        //                carrier.StatusCodeSpecified = true;
        //                List<ShipmentOrderLevelPackLevelItemLevelCarrierInformation> carrierColl = new List<ShipmentOrderLevelPackLevelItemLevelCarrierInformation>();
        //                carrierColl.Add(carrier);
        //                item.CarrierInformation = carrierColl.ToArray();
        //                List<GLOSURF.ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription> productDescColl = new List<ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription>();
        //                GLOSURF.ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription prodDescr = new ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription();
        //                prodDescr.ProductDescription = line.Product.Description;
        //                productDescColl.Add(prodDescr);
        //                item.ProductOrItemDescription = productDescColl.ToArray();
        //                itemLevelColl.Add(item);
        //            }

        //            List<GLOSURF.ShipmentHeaderCarrierInformation> headerCarrierInformation = new List<ShipmentHeaderCarrierInformation>();
        //            GLOSURF.ShipmentHeaderCarrierInformation hc = new ShipmentHeaderCarrierInformation();
        //            if (partShip)
        //            {
        //                hc.StatusCode = ShipmentHeaderCarrierInformationStatusCode.PR;
        //            }
        //            else
        //            {
        //                hc.StatusCode = ShipmentHeaderCarrierInformationStatusCode.CL;
        //            }
        //            hc.CarrierTransMethodCode = ShipmentHeaderCarrierInformationCarrierTransMethodCode.L;
        //            hc.CarrierTransMethodCodeSpecified = true;
        //            hc.StatusCodeSpecified = true;
        //            headerCarrierInformation.Add(hc);
        //            header.CarrierInformation = headerCarrierInformation.ToArray();
        //            packLevel.Items = itemLevelColl.ToArray();
        //            List<GLOSURF.ShipmentOrderLevelPackLevel> plColl = new List<ShipmentOrderLevelPackLevel>();
        //            plColl.Add(packLevel);
        //            orderLevel.Items = plColl.ToArray();
        //            List<GLOSURF.ShipmentOrderLevel> ordColl = new List<ShipmentOrderLevel>();
        //            ordColl.Add(orderLevel);
        //            List<GLOSURF.Shipment> shipmentColl = new List<GLOSURF.Shipment>();
        //            GLOSURF.Shipment gloShipment = new GLOSURF.Shipment();
        //            gloShipment.Items = ordColl.ToArray();
        //            gloShipment.Header = header;
        //            shipmentColl.Add(gloShipment);
        //            GLOSURF.Shipments ship = new Shipments();
        //            ship.Shipment = shipmentColl.ToArray();
        //            String cwXML = Path.Combine(Globals.glOutputDir, "GLOSUR" + "PICK" + cwShipment.Order.OrderNumber.Trim() + ".xml");
        //            int iFileCount = 0;
        //            while (File.Exists(cwXML))
        //            {
        //                iFileCount++;
        //                cwXML = Path.Combine(Globals.glOutputDir, "GLOSUR" + "PICK" + cwShipment.Order.OrderNumber.Trim() + iFileCount + ".xml");
        //            }
        //            Stream outputGLO = File.Open(cwXML, FileMode.Create);
        //            StringWriter writer = new StringWriter();
        //            XmlSerializer xSer = new XmlSerializer(typeof(GLOSURF.Shipments));
        //            var gloNS = new XmlSerializerNamespaces();
        //            gloNS.Add("", "http://www.spscommerce.com/RSX");

        //            xSer.Serialize(outputGLO, ship, gloNS);
        //            outputGLO.Flush();
        //            outputGLO.Close();
        //            string archiveFile = Globals.ArchiveFile(Globals.glArcLocation, cwXML);
        //            NodeResources.AddTransaction(NodeResources.GetCustID(Globals.glCustCode), Guid.Empty, xmlFile, "R", true, result, archiveFile);
        //            archiveFile = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
        //        }
        //    }
        //    catch (InvalidOperationException iop)
        //    {
        //        string strEx = iop.Message;

        //    }
        //    catch (Exception ex)
        //    {
        //        string strEx = ex.GetType().Name;

        //    }

        //    return result;
        //}

        private string[] GetAddressFromText(string address)
        {
            address.Replace(',', ' ');
            string[] add = address.Replace(',', ' ').Split(' ');
            add = add.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (add.Length == 1)
            {
                add = address.Split(',');
            }
            if (add.Length == 2)
            {
                if (!add[1].All(Char.IsDigit))
                {
                    add = GetAddressFromText(address.Replace(',', ' '));
                }


            }

            return add;

        }

    }
}
