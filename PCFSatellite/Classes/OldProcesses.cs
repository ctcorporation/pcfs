﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCFSatellite.Classes
{
    class OldProcesses
    {
        //public string GetPCFSWSFArticleid(string partNum)
        //{
        //    string result = string.Empty;
        //    SqlCommand sqlArticleId = new SqlCommand("GetArticleId", sqlConn);
        //    sqlArticleId.CommandType = CommandType.StoredProcedure;
        //    if (sqlConn.State == ConnectionState.Open)
        //    {
        //        sqlConn.Close();
        //    }
        //    sqlConn.Open();
        //    SqlParameter partnum = sqlArticleId.Parameters.Add("@PARTNUM", SqlDbType.VarChar, 20);
        //    partnum.Value = partNum;
        //    SqlParameter articleId = sqlArticleId.Parameters.Add("@ARTICLEID", SqlDbType.NChar, 10);
        //    articleId.Direction = ParameterDirection.Output;
        //    sqlArticleId.ExecuteNonQuery();
        //    result = articleId.Value.ToString();
        //    return result;
        //}

        //public string GetPCFSWSFArticleid(string partNum)
        //{
        //    string result = string.Empty;
        //    SqlCommand sqlArticleId = new SqlCommand("GetArticleId", sqlConn);
        //    sqlArticleId.CommandType = CommandType.StoredProcedure;
        //    if (sqlConn.State == ConnectionState.Open)
        //    {
        //        sqlConn.Close();
        //    }
        //    sqlConn.Open();
        //    SqlParameter partnum = sqlArticleId.Parameters.Add("@PARTNUM", SqlDbType.VarChar, 20);
        //    partnum.Value = partNum;
        //    SqlParameter articleId = sqlArticleId.Parameters.Add("@ARTICLEID", SqlDbType.NChar, 10);
        //    articleId.Direction = ParameterDirection.Output;
        //    sqlArticleId.ExecuteNonQuery();
        //    result = articleId.Value.ToString();
        //    return result;
        //}

        //public string GetSOArticleID(string soNumber)
        //{
        //    string result = string.Empty;
        //    SqlCommand sqlArticleId = new SqlCommand("GetSOId", sqlConn);
        //    sqlArticleId.CommandType = CommandType.StoredProcedure;
        //    if (sqlConn.State == ConnectionState.Open)
        //    {
        //        sqlConn.Close();
        //    }
        //    sqlConn.Open();
        //    SqlParameter sonum = sqlArticleId.Parameters.Add("@SONUM", SqlDbType.VarChar, 20);
        //    sonum.Value = soNumber;
        //    SqlParameter articleId = sqlArticleId.Parameters.Add("@ARTICLEID", SqlDbType.NChar, 10);
        //    articleId.Direction = ParameterDirection.Output;
        //    sqlArticleId.ExecuteNonQuery();
        //    result = articleId.Value.ToString();

        //    return result;
        //}

        //public string GetPOArticleID(string poNumber)
        //{
        //    string result = string.Empty;
        //    SqlCommand sqlArticleId = new SqlCommand("GetPOId", sqlConn);
        //    sqlArticleId.CommandType = CommandType.StoredProcedure;
        //    if (sqlConn.State == ConnectionState.Open)
        //    {
        //        sqlConn.Close();
        //    }
        //    sqlConn.Open();
        //    SqlParameter ponum = sqlArticleId.Parameters.Add("@PONUM", SqlDbType.VarChar, 20);
        //    ponum.Value = poNumber;
        //    SqlParameter articleId = sqlArticleId.Parameters.Add("@ARTICLEID", SqlDbType.NChar, 10);
        //    articleId.Direction = ParameterDirection.Output;
        //    sqlArticleId.ExecuteNonQuery();
        //    result = articleId.Value.ToString();

        //    return result;
        //}

        //private ProcessResult ProcessXUE(string xmlfile)
        //{
        //    MethodBase m = MethodBase.GetCurrentMethod();

        //    HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
        //    ProcessResult thisResult = new ProcessResult();
        //    FileInfo processingFile = new FileInfo(xmlfile);
        //    thisResult.Processed = true;
        //    String senderID = String.Empty;
        //    String recipientID = String.Empty;
        //    String reasonCode = String.Empty;
        //    String eventCode = String.Empty;
        //    totfilesRx++;
        //    NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
        //    cwRx++;
        //    NodeResources.AddLabel(lblCwRx, cwRx.ToString());
        //    TransReference trRef = new TransReference();
        //    List<Customer_Profile> custProfile = new List<Customer_Profile>();
        //    XUE.UniversalInterchange ue = new XUE.UniversalInterchange();
        //    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise Universal Event XML File Found");
        //    XUE.DataSource[] ds;

        //    try
        //    {
        //        using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
        //        {
        //            XmlSerializer cwConvert = new XmlSerializer(typeof(XUE.UniversalInterchange));
        //            ue = (XUE.UniversalInterchange)cwConvert.Deserialize(fStream);
        //            fStream.Close();
        //        }
        //        senderID = ue.Header.SenderID;
        //        recipientID = ue.Header.RecipientID;
        //        try
        //        {
        //            reasonCode = ue.Body.BodyField.Event.DataContext.ActionPurpose.Code.ToString();
        //        }
        //        catch (Exception)
        //        {
        //            reasonCode = string.Empty;
        //        }
        //        eventCode = ue.Body.BodyField.Event.DataContext.EventType.Code;
        //        ds = ue.Body.BodyField.Event.DataContext.DataSourceCollection;
        //        String msg = string.Empty;
        //        String idString = string.Empty;
        //        string methodReply = string.Empty;
        //        string activityReply = string.Empty;
        //        string idAttributename = string.Empty;
        //        string actionString = string.Empty;
        //        bool msgResult = false;
        //        XUE.Context[] cx;
        //        cx = ue.Body.BodyField.Event.ContextCollection;
        //        switch (ds[0].Type)
        //        {
        //            case "WarehouseReceive":
        //                msg = "Purchase Order Import Notification";
        //                if (cx[0].Type.Value == "ReceiveReference")
        //                {
        //                    idString = GetPOArticleID(cx[0].Value).Trim();
        //                }
        //                activityReply = "ERP.PurchaseOrder";
        //                methodReply = "replyFromExport";
        //                msgResult = true;
        //                idAttributename = "orderId";
        //                actionString = "confirm";
        //                break;
        //            case "WarehouseOrder":
        //                msg = "Sales Order Import Notification";
        //                if (cx[0].Type.Value == "OrderNumber")
        //                {
        //                    idString = GetSOArticleID(cx[0].Value).Trim();
        //                }
        //                activityReply = "ERP.SalesOrder";
        //                methodReply = "replyFromExport";
        //                msgResult = true;
        //                idAttributename = "orderId";
        //                switch (reasonCode)
        //                {
        //                    case "ORD":
        //                        actionString = "confirm";
        //                        break;
        //                    case "PRO":
        //                        actionString = "picking";
        //                        break;
        //                    case "PIK":
        //                        actionString = "picked";
        //                        break;
        //                }

        //                break;
        //            case "Product": break;
        //        }
        //        XElement genieESA = new XElement("ESA");
        //        XElement activityElement = new XElement("Activity", new XAttribute("id", activityReply));
        //        XElement methodElement = new XElement("Method",
        //                                        new XAttribute("name", "replyFromExport"),
        //                                        new XAttribute(idAttributename.Trim(), idString.Trim()),
        //                                        new XAttribute("action", actionString),
        //                                        new XAttribute("result", msgResult),
        //                                        new XAttribute("message", msg),
        //                                        new XAttribute("conNote", "")
        //                                        );

        //        activityElement.Add(methodElement);
        //        genieESA.Add(activityElement);
        //        string str = NodeResources.GenieMessage(genieESA);

        //        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + str);
        //        NodeResources.GenieMessage(NodeResources.GenerateArticleXML(activityReply, methodReply, idAttributename, idString, msgResult, msg));

        //        //AddTransaction((Guid)custProfile[0].C_id, (Guid)custProfile[0].P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
        //        //addSummRecord(DateTime.Now, recipientID, senderID, custProfile[0].P_MsgType, trRef, xmlfile, result);

        //        thisResult.Processed = true;
        //        thisResult.FileName = xmlfile;
        //        System.GC.Collect();
        //        System.GC.WaitForPendingFinalizers();
        //        File.Delete(xmlfile);
        //    }
        //    catch (Exception ex)
        //    {
        //        string strEx = ex.GetType().Name;
        //        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found while processing " + xmlfile + " Error: " + ex.Message);
        //        NodeResources.MoveFile(xmlfile, Globals.glFailPath);
        //        thisResult.Processed = false;
        //        thisResult.FileName = null;
        //    }


        //    return thisResult;


        //    //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ". Moving to Folder");
        //    //msgType = "Original";
        //    //File.Move(xmlfile, Path.Combine(custProfile[0].P_server, Path.GetFileName(xmlfile)));
        //    //AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfile[0].P_BillTo, recipientID);
        //    //result = "File Saved";
        //    //thisResult.Processed = true;
        //    //break;
        //}

        //public void PCFS_ProcessReceipt(string xmlFile, Customer_Profile cust)
        //{
        //    MethodBase m = MethodBase.GetCurrentMethod();

        //    HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
        //    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Order Receipt");
        //    UniversalInterchange toConvert;
        //    using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
        //    {
        //        XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
        //        toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
        //        fStream.Close();
        //    }
        //    XUS.Shipment consol = new XUS.Shipment();
        //    String senderID = toConvert.Header.SenderID;
        //    consol = toConvert.Body.BodyField.Shipment;
        //    DataContext dc = new DataContext();
        //    dc = consol.DataContext;

        //    DataSource[] dscoll;
        //    dscoll = dc.DataSourceCollection;
        //    string activityReply = string.Empty;
        //    string methodReply = string.Empty;
        //    Boolean msgResult = false;
        //    foreach (DataSource ds in dscoll)
        //    {
        //        if (ds.Type == "WarehouseReceive")
        //        {
        //            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Processing WH PO Receive");
        //            ShipmentOrder po = new ShipmentOrder();
        //            po = consol.Order;
        //            string clientOrderNumber = po.OrderNumber;
        //            //  string poID = GetPOArticleID(clientOrderNumber);
        //            if (!string.IsNullOrEmpty(poID))
        //            {
        //                msgResult = true;

        //                ShipmentOrderOrderLineCollection lineColl = new ShipmentOrderOrderLineCollection();
        //                lineColl = po.OrderLineCollection;
        //                ShipmentOrderOrderLineCollectionOrderLine[] lines;
        //                lines = lineColl.OrderLine;
        //                XElement genieESA = new XElement("ESA");
        //                XElement activityElement = new XElement("Activity", new XAttribute("id", "ERP.PurchaseOrder"));
        //                XElement methodElement = new XElement("Method",
        //                                               new XAttribute("name", "replyFromExport"),
        //                                               new XAttribute("orderId", poID.Trim()),
        //                                               new XAttribute("action", "receipt"),
        //                                               new XAttribute("result", msgResult),
        //                                               new XAttribute("message", "Purchase Order Receipt Results"),
        //                                               new XAttribute("conNote", ""),
        //                                               new XAttribute("deliveryNotes", "")
        //                                               );
        //                List<WSFItem> wsfitemColl = new List<WSFItem>();

        //                foreach (ShipmentOrderOrderLineCollectionOrderLine line in lines)
        //                {
        //                    WSFItem wsfitem = new WSFItem();
        //                    wsfitem.ArticleId = GetPCFSWSFArticleid(line.Product.Code).Trim();
        //                    wsfitem.ProductNo = line.Product.Code;
        //                    wsfitem.Qty = line.PackageQty;
        //                    if (wsfitemColl.Count > 0)
        //                    {
        //                        WSFItem splitLine = wsfitemColl.Find(x => x.ProductNo == wsfitem.ProductNo);
        //                        if (splitLine != null)
        //                        {

        //                            wsfitemColl.Where(item => item.ProductNo == wsfitem.ProductNo)
        //                                      .Select(item => { item.Qty = item.Qty + wsfitem.Qty; return item; })
        //                                      .ToList();
        //                        }
        //                        else
        //                        {
        //                            wsfitemColl.Add(wsfitem);
        //                        }
        //                        //WSFItem splitItem = wsfitemColl.Find(x =>x.ProductNo== wsfitem.ProductNo);
        //                        //if (splitItem!=null)
        //                        //{
        //                        //    splitItem.Qty += wsfitem.Qty;
        //                        //}
        //                    }
        //                    else
        //                    {
        //                        wsfitemColl.Add(wsfitem);
        //                    }
        //                    //   XElement itemElement = new XElement("Item", new XAttribute("articleId", GetPCFSWSFArticleid(line.Product.Code).Trim()), new XAttribute("actualQty", line.PackageQty.ToString()));
        //                    //   methodElement.Add(itemElement);
        //                }
        //                foreach (WSFItem item in wsfitemColl)
        //                {
        //                    XElement itemElement = new XElement("Item", new XAttribute("articleId", item.ArticleId), new XAttribute("actualQty", item.Qty.ToString()));
        //                    methodElement.Add(itemElement);
        //                }
        //                activityElement.Add(methodElement);
        //                genieESA.Add(activityElement);

        //                string msg = NodeResources.GenieMessage(genieESA);
        //                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + msg);
        //            }
        //        }
        //        if (ds.Type == "WarehouseOrder")
        //        {
        //            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Processing WH Sales Order Results.");
        //            ShipmentOrder so = new ShipmentOrder();
        //            so = consol.Order;
        //            string clientOrderNumber = so.OrderNumber;
        //            // string soID = GetSOArticleID(clientOrderNumber);
        //            if (!string.IsNullOrEmpty(soID))
        //            {
        //                msgResult = true;
        //                ShipmentOrderOrderLineCollection lineColl = new ShipmentOrderOrderLineCollection();
        //                lineColl = so.OrderLineCollection;
        //                ShipmentOrderOrderLineCollectionOrderLine[] lines;
        //                lines = lineColl.OrderLine;
        //                XElement genieESA = new XElement("ESA");
        //                XElement activityElement = new XElement("Activity", new XAttribute("id", "ERP.SalesOrder"));
        //                XElement methodElement = new XElement("Method",
        //                                                new XAttribute("name", "replyFromExport"),
        //                                                new XAttribute("orderId", soID.Trim()),
        //                                                new XAttribute("action", "picked"),
        //                                                new XAttribute("result", msgResult),
        //                                                new XAttribute("message", "Sales Order Pick Results"),
        //                                                new XAttribute("conNote", ""),
        //                                                new XAttribute("deliveryNotes", "")
        //                                                );
        //                foreach (ShipmentOrderOrderLineCollectionOrderLine line in lines)
        //                {
        //                    SqlCommand sqlArticleId = new SqlCommand("GetSalesOrderDetailsId", sqlConn);
        //                    sqlArticleId.CommandType = CommandType.StoredProcedure;
        //                    if (sqlConn.State == ConnectionState.Open)
        //                    {
        //                        sqlConn.Close();
        //                    }
        //                    sqlConn.Open();
        //                    SqlParameter partnum = sqlArticleId.Parameters.Add("@PARTNUM", SqlDbType.VarChar, 20);
        //                    partnum.Value = line.Product.Code;
        //                    SqlParameter uom = sqlArticleId.Parameters.Add("@UOM", SqlDbType.Char, 3);
        //                    uom.Value = line.PackageQtyUnit.Code;
        //                    SqlParameter soId = sqlArticleId.Parameters.Add("@SOID", SqlDbType.Int);
        //                    soId.Value = soID;
        //                    SqlParameter articleId = sqlArticleId.Parameters.Add("@ARTICLEID", SqlDbType.Int);
        //                    articleId.Direction = ParameterDirection.Output;
        //                    SqlParameter packSize = sqlArticleId.Parameters.Add("@PACKSIZE", SqlDbType.Int);
        //                    packSize.Direction = ParameterDirection.Output;
        //                    sqlArticleId.ExecuteNonQuery();
        //                    var soQty = 0;

        //                    soQty = Decimal.ToInt16(line.OrderedQty) / (int)packSize.Value;
        //                    XElement itemElement = new XElement("Item", new XAttribute("articleId", articleId.Value), new XAttribute("pickQty", soQty.ToString()));

        //                    methodElement.Add(itemElement);
        //                }
        //                activityElement.Add(methodElement);
        //                genieESA.Add(activityElement);
        //                string msg = NodeResources.GenieMessage(genieESA);
        //                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + msg);
        //            }
        //        }
        //    }
        //}
    }
}
