﻿using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CNodeBE
{
    public static class XmlNodeExtensions
    {
        public static XmlDocument AsXmlDocument<T>(this T o, XmlSerializerNamespaces ns = null, XmlSerializer serializer = null)
        {
            XmlDocument doc = new XmlDocument();
            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
                new XmlSerializer(o.GetType()).Serialize(writer, o, ns ?? NoStandardXmlNamespaces());

            //    doc = RemoveNameSpace(doc,ns);

            return doc;
        }

        public static string AsString(this XmlDocument xmlDoc)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    string strXmlText = sw.ToString();
                    return strXmlText;
                }
            }
        }



        public static XmlElement AsXmlElement<T>(this T o, XmlSerializerNamespaces ns = null, XmlSerializer serializer = null)
        {
            return o.AsXmlDocument(ns, serializer).DocumentElement;
        }

        public static T Deserialize<T>(this XmlElement element, XmlSerializer serializer = null)
        {
            using (var reader = new ProperXmlNodeReader(element))
                return (T)(serializer ?? new XmlSerializer(typeof(T))).Deserialize(reader);
        }

        public static XmlSerializerNamespaces NoStandardXmlNamespaces()
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", ""); // Disable the xmlns:xsi and xmlns:xsd lines.
            return ns;
        }
        public class ProperXmlNodeReader : XmlNodeReader
        {
            // Bug fix from http://stackoverflow.com/questions/30102275/deserialize-object-property-with-stringreader-vs-xmlnodereader
            public ProperXmlNodeReader(XmlNode node)
                : base(node)
            {
            }

            public override string LookupNamespace(string prefix)
            {
                return NameTable.Add(base.LookupNamespace(prefix));
            }
        }

        
        public static XmlDocument RemoveNameSpace(XmlDocument doc, XmlSerializerNamespaces xs)
        {
            XDocument xDoc;
            using (var nodeReader = new XmlNodeReader(doc))
            {
                nodeReader.MoveToContent();
                xDoc = XDocument.Load(nodeReader);
            }
            xDoc.Descendants().Attributes().Where(a => a.IsNamespaceDeclaration).Remove();
            foreach (var element in xDoc.Descendants())
            {
                element.Name = element.Name.LocalName;
            }
            doc.LoadXml(xDoc.ToString());
            return doc;

        }

    }




}

