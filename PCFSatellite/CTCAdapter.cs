﻿
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace CNodeBE
{
    class CTCAdapter
    {


        public static string GetMessageNamespace(string messageFilePath)
        {
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    ReadToNextElement(reader);
                    fileStream.Close();
                    return reader.NamespaceURI;
                }
            }
        }

        public static void ReadToNextElement(XmlReader reader)
        {
            while (reader.Read()) if (reader.NodeType == XmlNodeType.Element) break;
        }

        public static string GetApplicationCode(string messageNamespace)
        {
            string appCode = string.Empty;
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    appCode = "UDM";
                    break;

                case "http://www.cargowise.com/Schemas/Native":
                    appCode = "NDM";
                    break;

                case "http://www.edi.com.au/EnterpriseService/":
                    appCode = "XMS";
                    break;

                default:
                    appCode = "";
                    break;
            }

            return appCode;


        }

        public static string GetXMLType(String messageFilePath)
        {
            string result = string.Empty;
            XDocument xDoc;
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    xDoc = XDocument.Load(reader);
                }
            }
            XNamespace ns = "http://www.cargowise.com/Schemas/Universal/2011/11";
            var xmlType = xDoc.Descendants().Where(n => n.Name == ns + "UniversalEvent").FirstOrDefault();
            if (xmlType != null)
            {

                result = xmlType.Name.LocalName;
            }
            return result;

        }

        public static string GetSchemaName(string messageNamespace)
        {
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Native":
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    return messageNamespace + "#UniversalInterchange";

                case "http://www.edi.com.au/EnterpriseService/":
                    return messageNamespace + "#XmlInterchange";

                default:
                    return messageNamespace;
            }
        }

        static Stream GetFileAsStream(string fileName)
        {
            return new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
        }

    }
}
