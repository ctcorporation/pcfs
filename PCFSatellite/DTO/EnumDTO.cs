﻿
using PCFSatellite.Models;
using System.Linq;

namespace PCFSatellite.DTO
{
    public class EnumDTO : IEnumDTO
    {
        public SatelliteModel Context { get; set; }

        public EnumDTO(string connstring)
        {
            Context = new SatelliteModel(connstring);
        }
        public string GetMapValue(string enumType, string eNum)
        {
            using (IUnitOfWork uow = new UnitOfWork(Context))
            {
                var value = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == enumType && x.CW_ENUM == eNum).FirstOrDefault();
                if (value != null)
                {
                    return value.CW_MAPVALUE.Trim();
                }
            }
            return null;
        }

        public string GetEnum(string enumType, string mapValue)
        {
            using (IUnitOfWork uow = new UnitOfWork(Context))
            {
                var value = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == enumType && x.CW_MAPVALUE == mapValue).FirstOrDefault();
                if (value != null)
                {
                    return value.CW_ENUM.Trim();
                }
            }
            return null;
        }

    }
}
