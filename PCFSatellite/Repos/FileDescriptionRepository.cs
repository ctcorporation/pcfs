﻿using PCFSatellite.Models;

namespace PCFSatellite.Repository
{
    public class FileDescriptionRepository : GenericRepository<FileDescription>, IFileDescriptionRepository
    {
        public FileDescriptionRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel SatelliteModel
        {
            get
            {
                return Context as SatelliteModel;
            }
        }
    }
}
