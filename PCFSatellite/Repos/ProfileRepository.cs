﻿
using PCFSatellite.Models;

namespace PCFSatellite
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        public ProfileRepository(SatelliteModel context)
            : base(context)
        {

        }

        public SatelliteModel NodeDataContext
        {
            get { return Context as SatelliteModel; }
        }
    }
}
