﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace PCFSatellite
{

    public partial class frmCSVDetails : Form
    {
        class CSVProfile : ICSVProfile
        {
            public string csvName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public char csvDelimiter { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public int fieldCount { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string fieldNameFirst { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string fieldNameLast { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public bool hasHeaderRow { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        }

        public Guid profileID
        {
            get;

            set;
        }
        private Guid fileID
        {
            get; set;
        }
        SqlConnection sqlConn;

        public frmCSVDetails()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCSVDetails_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            fileID = Guid.Empty;
            GetCSVDetails(profileID);
        }

        private void GetCSVDetails(Guid profileID)
        {
            using (sqlConn)
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                SqlCommand sqlCSV = new SqlCommand("GetFileDescription", sqlConn);
                sqlCSV.CommandType = CommandType.StoredProcedure;
                sqlCSV.Parameters.AddWithValue("@PC_P", profileID);
                sqlConn.Open();
                SqlDataReader dr = sqlCSV.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    dr.Read();
                    edCsvName.Text = dr["PC_FILENAME"].ToString();
                    edDelimiter.Text = dr["PC_DELIMITER"].ToString();
                    edFieldCount.Text = dr["PC_FIELDCOUNT"].ToString();
                    edFirstFieldNAme.Text = dr["PC_FIRSTFIELDNAME"].ToString();
                    edLastFieldNames.Text = dr["PC_LASTFIELDNAME"].ToString();
                    cbQuotations.Checked = (bool)dr["PC_QUOTATIONS"];
                    cbHasHeaders.Checked = (bool)dr["PC_HASHEADER"];
                    edDataStart.Text = dr["PC_DATAStart"].ToString();
                    edHeaderStart.Text = dr["PC_HEADERSTART"].ToString();
                    fileID = (Guid)dr["PC_ID"];

                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (sqlConn)
            {
                sqlConn.ConnectionString = Globals.connString();
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                SqlCommand sqlCSV;

                if (fileID != Guid.Empty)
                {
                    sqlCSV = new SqlCommand("EditFileDescription", sqlConn);
                    sqlCSV.Parameters.AddWithValue("@PC_ID", fileID);
                }
                else
                {
                    sqlCSV = new SqlCommand("AddFileDescription", sqlConn);
                    SqlParameter pc_id = sqlCSV.Parameters.Add("@PC_ID", SqlDbType.UniqueIdentifier);
                    pc_id.Direction = ParameterDirection.Output;
                }
                sqlCSV.CommandType = CommandType.StoredProcedure;
                sqlCSV.Parameters.AddWithValue("@PC_P", profileID);
                sqlCSV.Parameters.AddWithValue("@PC_FILENAME", edCsvName.Text);
                sqlCSV.Parameters.AddWithValue("@PC_DELIMITER", edDelimiter.Text);
                sqlCSV.Parameters.AddWithValue("@PC_FIELDCOUNT", edFieldCount.Text);
                sqlCSV.Parameters.AddWithValue("@PC_FIRSTFIELDNAME", edFirstFieldNAme.Text);
                sqlCSV.Parameters.AddWithValue("@PC_LASTFIELDNAME", edLastFieldNames.Text);
                sqlCSV.Parameters.AddWithValue("@PC_QUOTATIONS", cbQuotations.CheckState);
                sqlCSV.Parameters.AddWithValue("@PC_HASHEADER", cbHasHeaders.CheckState);
                sqlCSV.Parameters.AddWithValue("@PC_DATASTART", edDataStart.Text);
                sqlCSV.Parameters.AddWithValue("@PC_HEADERSTART", edHeaderStart.Text);
                sqlConn.Open();
                sqlCSV.ExecuteNonQuery();
            }
            MessageBox.Show("File Description updated", "CSV File Description updated.");

        }

        private void btnEvaluate_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdEval = new OpenFileDialog();
            fdEval.ShowDialog();
            string headerText = string.Empty;
            try
            {
                if (File.Exists(fdEval.FileName))
                {
                    StreamReader sr = new StreamReader(fdEval.FileName);
                    if (string.IsNullOrEmpty(edHeaderStart.Text))
                    {
                        headerText = sr.ReadLine();
                    }
                    else
                    {
                        int headerStart = 0;
                        if (int.TryParse(edHeaderStart.Text, out headerStart))
                        {
                            for (int i = 0; i <= headerStart; i++)
                            {
                                sr.ReadLine();
                            }
                        }
                        headerText = sr.ReadLine();


                    }

                    string cDelimiter = NodeResources.GetDelimiter(headerText);
                    try
                    {
                        if (!string.IsNullOrEmpty(cDelimiter))
                        {

                            string[] headers = headerText.Split(Convert.ToChar(cDelimiter));
                            if (headers.Length > 0)
                            {
                                edDelimiter.Text = cDelimiter;
                                edFieldCount.Text = headers.Length.ToString();
                                edFirstFieldNAme.Text = headers[0];
                                edLastFieldNames.Text = headers[headers.Length - 1];
                                if (headers[0].Substring(0, 1) == "\"")
                                {
                                    cbQuotations.Checked = true;
                                }
                                else
                                {
                                    cbQuotations.Checked = false;
                                }
                                DialogResult dr = MessageBox.Show("Do the following look to be Field Names ?" + Environment.NewLine + "(" + headerText + ")", "Field Name Testing", MessageBoxButtons.YesNo);
                                if (dr == DialogResult.Yes)
                                {
                                    cbHasHeaders.Checked = true;
                                }
                                else
                                {
                                    cbHasHeaders.Checked = false;
                                }
                            }

                        }
                    }
                    catch (Exception)
                    {


                    }


                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error evaluating file: " + ex.InnerException, ex.GetType().Name + " Error occured", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
           




        }


    }
}
