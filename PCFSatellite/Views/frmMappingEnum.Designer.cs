﻿namespace PCFSatellite
{
    partial class frmMappingEnum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.cmbItems = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgMappings = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.edFieldName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbDataType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edLength = new System.Windows.Forms.TextBox();
            this.bbAdd = new System.Windows.Forms.Button();
            this.bbNew = new System.Windows.Forms.Button();
            this.edDataOperation = new System.Windows.Forms.TextBox();
            this.FldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fldType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fldLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgMappings)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(514, 415);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // cmbItems
            // 
            this.cmbItems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItems.FormattingEnabled = true;
            this.cmbItems.Location = new System.Drawing.Point(382, 343);
            this.cmbItems.Name = "cmbItems";
            this.cmbItems.Size = new System.Drawing.Size(121, 21);
            this.cmbItems.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Data Operation";
            // 
            // dgMappings
            // 
            this.dgMappings.AllowUserToAddRows = false;
            this.dgMappings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMappings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FldName,
            this.fldType,
            this.fldLength});
            this.dgMappings.Location = new System.Drawing.Point(31, 60);
            this.dgMappings.Name = "dgMappings";
            this.dgMappings.Size = new System.Drawing.Size(541, 267);
            this.dgMappings.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Field Name";
            // 
            // edFieldName
            // 
            this.edFieldName.Location = new System.Drawing.Point(89, 343);
            this.edFieldName.Name = "edFieldName";
            this.edFieldName.Size = new System.Drawing.Size(224, 20);
            this.edFieldName.TabIndex = 5;
            this.edFieldName.TextChanged += new System.EventHandler(this.edFieldName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(320, 346);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Field Type";
            // 
            // cmbDataType
            // 
            this.cmbDataType.FormattingEnabled = true;
            this.cmbDataType.Location = new System.Drawing.Point(143, 17);
            this.cmbDataType.Name = "cmbDataType";
            this.cmbDataType.Size = new System.Drawing.Size(317, 21);
            this.cmbDataType.TabIndex = 1;
            this.cmbDataType.SelectedIndexChanged += new System.EventHandler(this.cmbDataType_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(509, 347);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Length";
            // 
            // edLength
            // 
            this.edLength.Location = new System.Drawing.Point(555, 344);
            this.edLength.Name = "edLength";
            this.edLength.Size = new System.Drawing.Size(34, 20);
            this.edLength.TabIndex = 7;
            // 
            // bbAdd
            // 
            this.bbAdd.Location = new System.Drawing.Point(514, 383);
            this.bbAdd.Name = "bbAdd";
            this.bbAdd.Size = new System.Drawing.Size(75, 23);
            this.bbAdd.TabIndex = 8;
            this.bbAdd.Text = "&Add";
            this.bbAdd.UseVisualStyleBackColor = true;
            this.bbAdd.Click += new System.EventHandler(this.bbAdd_Click);
            // 
            // bbNew
            // 
            this.bbNew.Location = new System.Drawing.Point(512, 15);
            this.bbNew.Name = "bbNew";
            this.bbNew.Size = new System.Drawing.Size(75, 23);
            this.bbNew.TabIndex = 3;
            this.bbNew.Text = "&New";
            this.bbNew.UseVisualStyleBackColor = true;
            this.bbNew.Click += new System.EventHandler(this.bbNew_Click);
            // 
            // edDataOperation
            // 
            this.edDataOperation.Location = new System.Drawing.Point(144, 17);
            this.edDataOperation.Name = "edDataOperation";
            this.edDataOperation.Size = new System.Drawing.Size(316, 20);
            this.edDataOperation.TabIndex = 2;
            this.edDataOperation.Visible = false;
            // 
            // FldName
            // 
            this.FldName.DataPropertyName = "E_FieldName";
            this.FldName.HeaderText = "FieldName";
            this.FldName.Name = "FldName";
            this.FldName.Width = 300;
            // 
            // fldType
            // 
            this.fldType.DataPropertyName = "FIELDTYPE";
            this.fldType.HeaderText = "Field Type";
            this.fldType.Name = "fldType";
            this.fldType.Width = 140;
            // 
            // fldLength
            // 
            this.fldLength.DataPropertyName = "E_Length";
            this.fldLength.HeaderText = "Length";
            this.fldLength.Name = "fldLength";
            this.fldLength.Width = 50;
            // 
            // MappingEnum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 450);
            this.Controls.Add(this.edDataOperation);
            this.Controls.Add(this.bbNew);
            this.Controls.Add(this.bbAdd);
            this.Controls.Add(this.edLength);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbDataType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edFieldName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgMappings);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbItems);
            this.Controls.Add(this.bbClose);
            this.Name = "MappingEnum";
            this.Text = "Mapping MasterFile";
            this.Load += new System.EventHandler(this.MappingEnum_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMappings)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.ComboBox cmbItems;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgMappings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edFieldName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbDataType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edLength;
        private System.Windows.Forms.Button bbAdd;
        private System.Windows.Forms.Button bbNew;
        private System.Windows.Forms.TextBox edDataOperation;
        private System.Windows.Forms.DataGridViewTextBoxColumn FldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn fldType;
        private System.Windows.Forms.DataGridViewTextBoxColumn fldLength;
    }
}