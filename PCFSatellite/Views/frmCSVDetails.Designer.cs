﻿using System;

namespace PCFSatellite
{
    partial class frmCSVDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /// 
       
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edCsvName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edDelimiter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edFieldCount = new System.Windows.Forms.TextBox();
            this.cbHasHeaders = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.edFirstFieldNAme = new System.Windows.Forms.TextBox();
            this.edLastFieldNames = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEvaluate = new System.Windows.Forms.Button();
            this.cbQuotations = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edHeaderStart = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edDataStart = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(667, 93);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "CSV File Name default";
            // 
            // edCsvName
            // 
            this.edCsvName.Location = new System.Drawing.Point(140, 12);
            this.edCsvName.Name = "edCsvName";
            this.edCsvName.Size = new System.Drawing.Size(283, 20);
            this.edCsvName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(457, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "CSV Delimiter";
            // 
            // edDelimiter
            // 
            this.edDelimiter.Location = new System.Drawing.Point(534, 12);
            this.edDelimiter.Name = "edDelimiter";
            this.edDelimiter.Size = new System.Drawing.Size(21, 20);
            this.edDelimiter.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Number of Fields in CSV File.";
            // 
            // edFieldCount
            // 
            this.edFieldCount.Location = new System.Drawing.Point(170, 44);
            this.edFieldCount.Name = "edFieldCount";
            this.edFieldCount.Size = new System.Drawing.Size(100, 20);
            this.edFieldCount.TabIndex = 6;
            // 
            // cbHasHeaders
            // 
            this.cbHasHeaders.AutoSize = true;
            this.cbHasHeaders.Location = new System.Drawing.Point(306, 47);
            this.cbHasHeaders.Name = "cbHasHeaders";
            this.cbHasHeaders.Size = new System.Drawing.Size(108, 17);
            this.cbHasHeaders.TabIndex = 7;
            this.cbHasHeaders.Text = "Has Header Row";
            this.cbHasHeaders.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Name of First Field";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Name of Last Field";
            // 
            // edFirstFieldNAme
            // 
            this.edFirstFieldNAme.Location = new System.Drawing.Point(140, 73);
            this.edFirstFieldNAme.Name = "edFirstFieldNAme";
            this.edFirstFieldNAme.Size = new System.Drawing.Size(205, 20);
            this.edFirstFieldNAme.TabIndex = 10;
            // 
            // edLastFieldNames
            // 
            this.edLastFieldNames.Location = new System.Drawing.Point(140, 100);
            this.edLastFieldNames.Name = "edLastFieldNames";
            this.edLastFieldNames.Size = new System.Drawing.Size(205, 20);
            this.edLastFieldNames.TabIndex = 11;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(667, 64);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEvaluate
            // 
            this.btnEvaluate.Location = new System.Drawing.Point(667, 35);
            this.btnEvaluate.Name = "btnEvaluate";
            this.btnEvaluate.Size = new System.Drawing.Size(75, 23);
            this.btnEvaluate.TabIndex = 13;
            this.btnEvaluate.Text = "Evaluate";
            this.btnEvaluate.UseVisualStyleBackColor = true;
            this.btnEvaluate.Click += new System.EventHandler(this.btnEvaluate_Click);
            // 
            // cbQuotations
            // 
            this.cbQuotations.AutoSize = true;
            this.cbQuotations.Location = new System.Drawing.Point(420, 47);
            this.cbQuotations.Name = "cbQuotations";
            this.cbQuotations.Size = new System.Drawing.Size(135, 17);
            this.cbQuotations.TabIndex = 14;
            this.cbQuotations.Text = "Enclosed in Quotations";
            this.cbQuotations.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(372, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Header Starts at";
            // 
            // edHeaderStart
            // 
            this.edHeaderStart.Location = new System.Drawing.Point(469, 73);
            this.edHeaderStart.Name = "edHeaderStart";
            this.edHeaderStart.Size = new System.Drawing.Size(37, 20);
            this.edHeaderStart.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(372, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Data Starts at";
            // 
            // edDataStart
            // 
            this.edDataStart.Location = new System.Drawing.Point(469, 100);
            this.edDataStart.Name = "edDataStart";
            this.edDataStart.Size = new System.Drawing.Size(37, 20);
            this.edDataStart.TabIndex = 18;
            // 
            // frmCSVDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 128);
            this.Controls.Add(this.edDataStart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.edHeaderStart);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbQuotations);
            this.Controls.Add(this.btnEvaluate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.edLastFieldNames);
            this.Controls.Add(this.edFirstFieldNAme);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbHasHeaders);
            this.Controls.Add(this.edFieldCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edDelimiter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edCsvName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Name = "frmCSVDetails";
            this.Text = "CSV File Details";
            this.Load += new System.EventHandler(this.frmCSVDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edCsvName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edDelimiter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edFieldCount;
        private System.Windows.Forms.CheckBox cbHasHeaders;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edFirstFieldNAme;
        private System.Windows.Forms.TextBox edLastFieldNames;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEvaluate;
        private System.Windows.Forms.CheckBox cbQuotations;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edHeaderStart;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edDataStart;
    }
}