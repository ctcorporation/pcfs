﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace PCFSatellite

{


    public partial class frmMappingEnum : Form
    {
        public SqlConnection sqlConn;

        public frmMappingEnum()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MappingEnum_Load(object sender, EventArgs e)
        {
            FillOperations();
            //cmbItems.Items.Add("(none selected}");

            var items = new[] {
                new {Text = "String", Value = "STR" } ,
                new {Text = "Integer", Value = "INT" },
                new { Text = "Real Number", Value = "DEC" },
                new { Text = "DateTime", Value="DAT" }
            };
            cmbItems.DataSource = items;
            // cmbItems.Items.Clear();
            cmbItems.DisplayMember = "Text";
            cmbItems.ValueMember = "Value";



        }

        private void FillOperations()
        {
            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            SqlDataAdapter da = new SqlDataAdapter("Select E_ProcessName from Vw_MappingOperations order by E_ProcessName", sqlConn);
            DataSet ds = new DataSet();
            da.Fill(ds, "Enums");
            DataRow dr = ds.Tables["Enums"].NewRow();
            dr["E_PROCESSNAME"] = "(none selected)";
            ds.Tables["Enums"].Rows.InsertAt(dr, 0);

            cmbDataType.DataSource = ds.Tables["Enums"];
            cmbDataType.DisplayMember = "E_ProcessName";

            cmbDataType.ValueMember = "E_ProcessName";
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            switch (bbNew.Text)
            {
                case "&New":
                    edDataOperation.Visible = true;
                    cmbDataType.Visible = false;
                    bbNew.Text = "C&ancel";
                    break;
                case "C&ancel":
                    edDataOperation.Visible = false;
                    cmbDataType.Visible = true;
                    bbNew.Text = "&New";
                    break;
            }
        }

        private void bbAdd_Click(object sender, EventArgs e)
        {
            SqlCommand AddMappingEnums = new SqlCommand();
            AddMappingEnums.CommandType = CommandType.StoredProcedure;
            AddMappingEnums.Connection = sqlConn;
            AddMappingEnums.CommandText = "Add_MappingEnum";
            SqlParameter procName = AddMappingEnums.Parameters.Add("@ProcName", SqlDbType.NVarChar, 20);
            SqlParameter fieldName = AddMappingEnums.Parameters.Add("@FieldName", SqlDbType.NVarChar, 50);
            SqlParameter length = AddMappingEnums.Parameters.Add("@Length", SqlDbType.Int);
            SqlParameter fieldType = AddMappingEnums.Parameters.Add("@FieldType", SqlDbType.Char, 3);
            SqlParameter eid = AddMappingEnums.Parameters.Add("@EID", SqlDbType.UniqueIdentifier);
            eid.Direction = ParameterDirection.Output;

            if (edDataOperation.Visible)
            {
                procName.Value = edDataOperation.Text;
            }
            else
            {
                procName.Value = cmbDataType.SelectedValue;

            }
            fieldName.Value = edFieldName.Text;
            fieldType.Value = cmbItems.SelectedValue;
            int iLength = 0;
            if (int.TryParse(edLength.Text, out iLength))
            {
                length.Value = iLength;

            }
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            AddMappingEnums.ExecuteNonQuery();
            FillOperations();
            cmbDataType.Text = procName.Value.ToString();
            if (bbNew.Text == "C&ancel")
            {
                bbNew.PerformClick();
            }

            GetEnums(procName.Value.ToString());
        }

        private void cmbDataType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDataType.SelectedText != "(none selected)")
            {
                GetEnums(cmbDataType.Text);
            }
        }

        private void GetEnums(string selectedText)
        {
            SqlDataAdapter da = new SqlDataAdapter("Select E_ID, E_PROCESSNAME, E_FIELDNAME, Case when E_FIELDTYPE='STR' then 'String' when E_FIELDTYPE='DAT' then 'Date/Time' when E_FIELDTYPE = 'DEC' then 'Real Number' " +
                "when E_FieldType ='INT' then 'Integer' end as FIELDTYPE, E_LENGTH from Mapping_Enums where E_ProcessName = '" + selectedText + "' order by E_FieldName", sqlConn);
            DataSet ds = new DataSet();
            da.Fill(ds, "EnumList");
            dgMappings.AutoGenerateColumns = false;
            dgMappings.DataSource = ds;
            dgMappings.DataMember = "EnumList";


        }

        private void edFieldName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
