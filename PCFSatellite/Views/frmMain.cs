﻿using CNodeBE;
using GLOSURF;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using PCFSatellite.Models;
using PCFSatellite.Services;
using PCFSatellite.SurfAgency;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using XMLLocker.CTC;
using XUS;
using static PCFSatellite.SatelliteErrors;
//using CWShipment = XUS.Shipment;

namespace PCFSatellite
{

    public partial class frmMain : Form
    {
        #region Properties

        #endregion

        #region Members
        string _errMsg = string.Empty;
        string _appLogPath = string.Empty;
        int totfilesRx;
        int totfilesTx;
        int cwRx;
        int cwTx;
        int filesRx;
        int filesTx;
        System.Timers.Timer tmrMain;
        System.Timers.Timer tmrHighTide;
        private string _connString;
        Stopwatch eventStopwatch, noEventStopWatch;

        #endregion

        #region helpers
        delegate void StatusbarUpdateCallback(string text);
        private void UpdateSb(string text)
        {
            if (statusStrip1.InvokeRequired)
            {
                var d = new StatusbarUpdateCallback(UpdateSb);
                this.Invoke(d, new object[] { text });
            }
            else
                tslMain.Text = text;

        }
        #endregion
        public frmMain()
        {
            InitializeComponent();
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            tmrMain = new System.Timers.Timer();
            tmrHighTide = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(7).TotalMilliseconds,
                Enabled = true
            };
            tmrHighTide.Elapsed += new ElapsedEventHandler(HighTideTimer_Elapsed);
            tmrHighTide.Start();
            eventStopwatch = new Stopwatch();
            noEventStopWatch = new Stopwatch();

            var assembly = typeof(Program).Assembly;
            var attribute = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0];
            var id = attribute.Value;
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);


        }

        private void HighTideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (eventStopwatch.IsRunning)
            {
                if (eventStopwatch.Elapsed.TotalMinutes >= 5)
                {
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " HighTide Timer Activated. " + eventStopwatch.Elapsed.TotalMinutes.ToString("mm.sss") + " Minutes from EventStopWatch", Color.Orange); ;
                }
                UpdateSb("Timed Processes Started");
                //GetFiles("Testing");
                GetFiles(Globals.glPickupPath);
                if (!string.IsNullOrEmpty(Globals.glCustomFilesPath))
                {
                    GetFiles(Globals.glCustomFilesPath);
                }
                //  PrintJobs("");
                tmrMain.Start();

            }
            if (noEventStopWatch.IsRunning)
            {
                if (noEventStopWatch.Elapsed.TotalMinutes >= 7)
                {
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " HighTide Timer Activated. " + eventStopwatch.Elapsed.TotalMinutes + " Minutes from No-EventStopWatch", Color.Orange);
                }
                UpdateSb("Timed Processes Started");
                //GetFiles("Testing");
                GetFiles(Globals.glPickupPath);
                if (!string.IsNullOrEmpty(Globals.glCustomFilesPath))
                {
                    GetFiles(Globals.glCustomFilesPath);
                }
                //  PrintJobs("");
                tmrMain.Start();
            }
        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop") // The Start button will say stop while running
            {
                tmrMain.Stop();

                GetFiles(Globals.glPickupPath);
                if (!string.IsNullOrEmpty(Globals.glCustomFilesPath))
                {
                    GetFiles(Globals.glCustomFilesPath);
                }

                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
                UpdateSb("Timed Process Stopped");
            }


        }



        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = System.Drawing.Color.LightCoral;
                UpdateSb("Timed Processes Started");
                GetFiles(Globals.glPickupPath);
                if (!string.IsNullOrEmpty(Globals.glCustomFilesPath))
                {
                    GetFiles(Globals.glCustomFilesPath);
                }
                //  PrintJobs("");
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = System.Drawing.Color.LightGreen;
                UpdateSb("Timed Processes Stopped");
                tmrMain.Stop();
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();

        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            using (NodeData.IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.glConnString)))
            {
                if (!uow.Profiles.CheckConnection())
                {
                    MessageBox.Show("Error Connecting to Satellite Database. Please confirm settings", "Database Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ShowSettings();
                }
            }
            try
            {
                HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Connecting to CTC Node Database. Please confirm settings", "Database Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ShowSettings();
            }

            productionToolStripMenuItem.Checked = true;
            tslMode.Text = "Production";
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);


            }
            tmrMain.Interval = (1000 * 5) * 60;
            string appLog = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-Log.XML";
            Globals.AppLogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, appLog);
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    // Configuration Section DataBase
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }


                    ConnectionManager connMgr = new ConnectionManager(Globals.glDbServer, Globals.glDbInstance, Globals.glDbUserName, Globals.glDbPassword);
                    Globals.glConnString = connMgr.ConnString;
                    _connString = connMgr.ConnString;
                    nodeList = null;
                    //Configuration Section Customer
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");

                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        Globals.Customer cust = new Globals.Customer();
                        cust = Globals.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.Code;
                            Globals.gl_CustId = cust.Id;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTestPath = nodeList[0].SelectSingleNode("TestPath");
                    if (xTestPath != null)
                    {
                        Globals.glTestLocation = xTestPath.InnerText;
                    }
                    XmlNode xArchive = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArchive != null)
                    {
                        Globals.glArcLocation = xArchive.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xCustom = nodeList[0].SelectSingleNode("CustomFilesPath");
                    if (xCustom != null)
                    {
                        Globals.glCustomFilesPath = xCustom.InnerText;
                    }
                    // Configuration Section Communications
                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");

                    XmlNode xmlglAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlglAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlglAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    Globals.MailServerSettings = new MailServerSettings
                    {
                        Server = SMTPServer.Server,
                        UserName = SMTPServer.Username,
                        Password = SMTPServer.Password,
                        Port = Convert.ToInt16(SMTPServer.Port),
                        Email = SMTPServer.Email
                    };
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }


                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }
                }

                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ShowSettings();
                }
                var Cust = Globals.GetCustomer(Globals.glCustCode);
                if (Cust != null)
                {
                    this.Text = "CTC Satellite - " + Cust.CustomerName;
                }
                else
                {
                    MessageBox.Show("There was an error loading the Customer Data File. Please update the Customer settings.", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");

            }
        }

        private void ShowSettings()
        {

            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private TransReference PhytoOrder(string fileName, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            var result = new TransReference();
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            var po = new PhytoTherapy(cust);
            var common = po.DespatchOrder(fileName);
            if (common != null)
            {
                string orderno = common.WarehouseOrders.FirstOrDefault().OrderNumber;
                var cw = new CargowiseFunctions(Globals.AppLogPath)
                {
                    Ref1 = orderno,
                    Ref1Type = "WHSOrder"

                };
                var outFile = cw.CWfromCommon(common);

                result.Ref1Type = TransReference.RefType.WHSPick;
                result.Reference1 = orderno;
                Globals.ArchiveFile(Globals.glArcLocation, fileName);

                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New Order created: " + cust.P_SENDERID + ": " + orderno, Color.Green);
            }
            Globals.ArchiveFile(Globals.glArcLocation, fileName);

            return result;

        }

        private TransReference PhytoTransfer(string fileName, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            var result = new TransReference();
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            var pt = new PhytoTherapy(cust);
            var common = pt.TransferOrder(fileName);
            if (common != null)
            {
                string orderno = common.WarehouseOrders.FirstOrDefault().OrderNumber;
                var cw = new CargowiseFunctions(Globals.AppLogPath)
                {
                    Ref1 = orderno,
                    Ref1Type = "WHSOrder"

                };
                var outFile = cw.CWfromCommon(common);

                result.Ref1Type = TransReference.RefType.WHSPick;
                result.Reference1 = orderno;
                Globals.ArchiveFile(Globals.glArcLocation, fileName);

                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New Order created: " + cust.P_SENDERID + ": " + orderno, Color.Green);
            }
            Globals.ArchiveFile(Globals.glArcLocation, fileName);
            return result;
        }


        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        public TransReference SurfboardAgencyOrder(string fileName, Customer_Profile cust)
        {
            FileInfo fi = new FileInfo(fileName);
            MethodBase m = MethodBase.GetCurrentMethod();
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new TransReference();
            switch (fi.Extension.ToUpper())
            {
                case ".XML":
                    CIN7.Orders orders = new CIN7.Orders();
                    using (FileStream fStream = new FileStream(fileName, FileMode.Open))
                    {
                        XmlSerializer ordConvert = new XmlSerializer(typeof(CIN7.Orders));
                        orders = (CIN7.Orders)ordConvert.Deserialize(fStream);
                        fStream.Close();
                    }
                    var surfAgency = new SurfboardAgency(_connString);
                    var common = surfAgency.CreateOrder(orders, fileName, cust);
                    if (common != null)
                    {
                        string orderno = common.WarehouseOrders.FirstOrDefault().OrderNumber;
                        var cw = new CargowiseFunctions(Globals.glOutputDir)
                        {
                            Ref1 = orderno,
                            Ref1Type = "WHSOrder"

                        };
                        var outFile = cw.CWfromCommon(common);

                        result.Ref1Type = TransReference.RefType.WHSPick;
                        result.Reference1 = orderno;
                        Globals.ArchiveFile(Globals.glArcLocation, fileName);

                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New Order created: " + cust.P_SENDERID + ": " + orderno, Color.Green);
                    }

                    break;

                case ".CSV":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Converting CSV Data to Cargowise XML: " + cust.P_SENDERID + ": " + cust.P_RECIPIENTID + ": ", System.Drawing.Color.Green);
                    var v = nfsa.DespatchOrder(fileName, cust);
                    result = v.Item2;
                    if (result.Ref3Type == null)
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Error found in Converting Surfboard Agency file " + fileName + ": " + result.Reference3, System.Drawing.Color.Red);
                        NodeResources.MoveFile(fileName, Globals.glFailPath);
                    }
                    else
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Conversion complete. " + result.Reference2 + " created. ", System.Drawing.Color.Green);
                        string archive = Globals.ArchiveFile(Globals.glArcLocation, fileName);
                        //NodeResources.AddTransaction(NodeResources.GetCustID(cust.P_SENDERID), Guid.Empty, fileName, "S", true, result, archive);
                    }
                    break;
            }
            return result;
        }


        private TransReference PCFSHorizon(string fileName, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new TransReference();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Converting CSV Data to Cargowise XML: " + cust.P_SENDERID + ": " + cust.P_RECIPIENTID + ": ", System.Drawing.Color.Green);
            var v = Horizon.DespatchOrder(fileName, cust);
            result = v.Item2;
            if (result.Ref3Type == TransReference.RefType.Error)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Error found in Converting Horizon file " + fileName + ": " + result.Reference3, System.Drawing.Color.Red);
                NodeResources.MoveFile(fileName, Globals.glFailPath);
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Conversion complete. " + result.Reference2 + " created. ", System.Drawing.Color.Green);
                string archive = Globals.ArchiveFile(Globals.glArcLocation, fileName);
                // NodeResources.AddTransaction(NodeResources.GetCustID(cust.P_SENDERID), Guid.Empty, fileName, "S", true, result, archive);
            }
            return result;

        }

        private void GetFiles(string folder)
        {
            noEventStopWatch.Stop();
            noEventStopWatch.Reset();

            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.glPickupPath;
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }

            DirectoryInfo diTodo;
            diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            eventStopwatch = new Stopwatch();
            eventStopwatch.Start();
            var list = diTodo.GetFiles();

            if (list.Length == 0)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files from " + folder);
            }

            foreach (var fileName in diTodo.GetFiles())
            {
                iCount++;
                ProcessResult thisResult = new ProcessResult();
                thisResult = ProcessCustomFiles(fileName.FullName);
                //return;
                if (thisResult.Processed)
                {
                    try
                    {
                        if (File.Exists(fileName.FullName))
                        {
                            System.GC.Collect();
                            System.GC.WaitForPendingFinalizers();
                            File.Delete(fileName.FullName);
                        }


                    }
                    catch (Exception ex)
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                    }

                }

            }
            eventStopwatch.Stop();
            double t = eventStopwatch.Elapsed.TotalSeconds;
            if (iCount == 0 )
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + iCount + " files");
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + iCount + " files processed in " + t + " seconds");
            }
            noEventStopWatch.Start();
        }

        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;

            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " Is not a Cargowise File. Checking for Against Customer.");
            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();
            ProcessResult pr = new ProcessResult();
            switch (processingFile.Extension.ToUpper())
            {
                case ".XML":
                    ProcessCargowiseFiles(xmlFile);
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New XML file found " + Path.GetFileName(processingFile.FullName) + ". Processing" + "");
                    break;

                case ".CSV":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New CSV file found " + Path.GetFileName(processingFile.FullName) + ". Identifying" + "");
                    var fileProfile = GetCSVFileType(xmlFile);
                    if (fileProfile == null)
                    {
                        thisResult.FileName = xmlFile;
                        thisResult.Processed = false;
                        return thisResult;
                    }
                    if (fileProfile == Guid.Empty)
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + pr.FileName + "No File Profile Found. Check Email for Details", Color.Orange);
                        thisResult.FileName = xmlFile;
                        thisResult.Processed = true;
                        return thisResult;
                    }
                    using (IUnitOfWork uow = new DTO.UnitOfWork(new Models.SatelliteModel(Globals.glConnString)))
                    {
                        var cust = uow.CustomerProfiles.Get((Guid)fileProfile);
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Profile " + cust.P_DESCRIPTION + " will be used.");
                        pr = ProcessOtherFiles(xmlFile, cust);
                        if (pr.Processed)
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + pr.FileName + " process complete.", Color.Green);
                        }
                        else
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + pr.FileName + "Error Processing. Check Email for Details", Color.Red);
                        }
                    }


                    //   PCFSHorizon(xmlFile, cust);
                    break;

            }

            return thisResult;
        }

        private Guid? GetCSVFileType(string fileName)
        {
            Guid result = Guid.Empty;
            try
            {
                using (IUnitOfWork uow = new DTO.UnitOfWork(new Models.SatelliteModel(Globals.glConnString)))
                {
                    var fileprofiles = uow.FileDescriptions.GetAll().ToList();
                    if (fileprofiles.Count > 0)
                    {
                        foreach (var fileDescription in fileprofiles)
                        {
                            try
                            {
                                string headerText = string.Empty;
                                StreamReader sr = new StreamReader(fileName);

                                if (fileDescription.PC_HeaderStart > 0)
                                {
                                    for (int i = 1; i <= fileDescription.PC_HeaderStart; i++)
                                    {
                                        headerText = sr.ReadLine();
                                    }
                                }
                                // headerText = sr.ReadLine();
                                if (!string.IsNullOrEmpty(headerText))
                                {
                                    string[] headers = headerText.Split(Convert.ToChar(fileDescription.PC_Delimiter));
                                    if (headers.Length == fileDescription.PC_Fieldcount)
                                    {
                                        if (!string.IsNullOrEmpty(fileDescription.PC_FirstFieldName))
                                        {
                                            if (fileDescription.PC_FirstFieldName == headers[0])
                                            {
                                                if (!string.IsNullOrEmpty(fileDescription.PC_LastFieldName))
                                                {
                                                    if (fileDescription.PC_LastFieldName == headers[headers.Length - 1])
                                                    {
                                                        return (Guid)fileDescription.PC_P;
                                                    }
                                                }
                                                else
                                                {
                                                    return (Guid)fileDescription.PC_P;
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                string strEx = ex.GetType().Name;
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Exception found. Error was " + ex.Message, System.Drawing.Color.Red);

                                MailModule mailModule = new MailModule(Globals.MailServerSettings);
                                mailModule.SendMsg(fileName, Globals.glAlertsTo, "Unhandled Exception found", ex.Message + Environment.NewLine + ex.InnerException);
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                MailModule mailModule = new MailModule(Globals.MailServerSettings);
                mailModule.SendMsg(fileName, Globals.glAlertsTo, "Unhandled Exception found", ex.Message + Environment.NewLine + ex.InnerException + Environment.NewLine +
                  "Stack Trace" + ex.StackTrace);

            }
            return null;
        }

        private ProcessResult ProcessOtherFiles(string fileName, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult thisResult = new ProcessResult();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Custom File Found (" + Path.GetFileName(fileName) + "). Continue processing Profile.");

            string custMethod = cust.P_METHOD;
            string custParams = cust.P_PARAMLIST;
            Type custType = this.GetType();
            if (string.IsNullOrEmpty(custMethod))
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " No Custom Method has been defined. Skipping file and notifying Support.", System.Drawing.Color.Red);

                MailModule mailModule = new MailModule(Globals.MailServerSettings);
                mailModule.SendMsg(fileName, Globals.glAlertsTo, cust.C_CODE + ": Missing Custom Method for Processing", "Missing Custom Method for Processing custom files." + Environment.NewLine +
                   "Profile Name: " + cust.P_DESCRIPTION);
                NodeResources.MoveFile(fileName, Globals.glFailPath);
                thisResult.FileName = fileName;
                thisResult.FolderLoc = Globals.glFailPath;
                thisResult.Processed = false;

            }
            else
            {

                MethodInfo custMethodToRun = custType.GetMethod(custMethod, BindingFlags.NonPublic | BindingFlags.Instance);
                var varResult = custMethodToRun.Invoke(this, new Object[] { fileName, cust });
                if (varResult != null)
                {
                    thisResult.Processed = true;
                    //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);
                    if (varResult is TransReference)
                    {
                        TransReference tr = varResult as TransReference;
                        if (tr.Ref3Type == TransReference.RefType.Error)
                        {

                            thisResult.Processed = true;
                            thisResult.FileName = fileName;
                            thisResult.FileName = Globals.glFailPath;

                        }
                        else
                        {
                            // recipientID = cust.P_recipientid;
                            // senderID = cust.P_SENDERID;
                            string archiveFile = Globals.ArchiveFile(cust.C_PATH + "\\Archive", fileName);
                        }
                    }

                }
            }

            return thisResult;

        }

        public TransReference CommonSalesOrder(string fileToSend, Models.Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new TransReference();
            FileProcessorService fps = new FileProcessorService(Globals.glOutputDir, fileToSend);
            fps.ConnString = Globals.glConnString;
            var processResult = fps.ProcessFile(GetCustProfileRecord(cust.P_ID));
            if (processResult.T_LOGTYPE == "Success")
            {
                result.Ref1Type = TransReference.RefType.WHSOrder;
                result.Reference1 = processResult.T_REF1;
                using (IUnitOfWork uow = new DTO.UnitOfWork(new SatelliteModel(Globals.glConnString)))
                {
                    uow.Transactions.Add(processResult);
                    if (uow.Complete() > 0)
                    {
                        MailModule mm = new MailModule(Globals.MailServerSettings);
                        if (cust.P_NOTIFY == "Y")
                        {
                            mm.SendMsg(string.Empty, cust.P_NOTIFYEMAIL, "Sales Order " + result.Reference1 + " converted and sent to Warehosue", string.Empty);
                        }

                    }

                }
            }


            return result;
        }

        private Customer_Profile GetCustProfileRecord(Guid p_ID)
        {
            using (IUnitOfWork uow = new DTO.UnitOfWork(new SatelliteModel(Globals.glConnString)))
            {
                return uow.CustomerProfiles.Get(p_ID);
            }
        }

        private TransReference CreateOrderReceipt(string fileToSend, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new TransReference();
            var transaction = new Models.Transaction();
            try
            {
                var csvTable = CsvHelper.ConvertCSVtoDataTable(fileToSend);
                using (IUnitOfWork uow = new DTO.UnitOfWork(new SatelliteModel(Globals.glConnString)))
                {
                    var profile = cust;
                    var mapping = uow.MapOperations.Find(x => x.MD_MapDescription == profile.P_MAPOPERATION).ToList();
                    if (mapping.Count > 0)
                    {
                        OrderReceipt orderReceipt = new OrderReceipt(Globals.glConnString, csvTable, mapping);
                        orderReceipt.SenderId = cust.P_SENDERID;
                        orderReceipt.RecipientId = cust.P_RECIPIENTID;
                        orderReceipt.MailSettings = (MailServerSettings)Globals.MailServerSettings;
                        var nodeFile = orderReceipt.ConvertToCommonReceipt(cust.P_NOTIFYEMAIL);
                        if (nodeFile != null)
                        {
                            ToCargowise commonFunc = new ToCargowise(Globals.AppLogPath);
                            var cwFile = commonFunc.CWFromCommon(nodeFile);
                            result.Reference1 = string.IsNullOrEmpty(nodeFile.WarehouseReceipt.ReceiveReference.Replace("\"", string.Empty)) ? nodeFile.WarehouseReceipt.CustomerReferences : nodeFile.WarehouseReceipt.ReceiveReference.Replace("\"", string.Empty);
                            result.Ref1Type = TransReference.RefType.WHSReceipt;
                            if (cwFile != null)
                            {
                                var cw = commonFunc.ToFile(cwFile, profile.P_SENDERID + "-" + result.Reference1, Globals.glOutputDir);
                                transaction.T_ID = Guid.NewGuid();
                                transaction.T_DIRECTION = "S";
                                transaction.T_DATETIME = DateTime.Now;
                                transaction.T_FILENAME = Path.GetFileName(fileToSend);
                                transaction.T_C = cust.P_C;
                                transaction.T_REF1 = result.Reference1;
                                transaction.T_REF1TYPE = result.Ref1Type.ToString();
                                transaction.T_P = cust.P_ID;
                                uow.Transactions.Add(transaction);
                                if (uow.Complete() > 0)
                                {
                                    MailModule mm = new MailModule(Globals.MailServerSettings);
                                    if (cust.P_NOTIFY == "Y")
                                    {
                                        mm.SendMsg("", cust.P_NOTIFYEMAIL, "Order Receipt " + result.Reference1 + " Received and converted", string.Empty);
                                    }
                                }
                            }
                            result.Ref3Type = TransReference.RefType.Error;
                            result.Reference3 = "Missing Products";
                        }
                    }
                }
                result.Ref1Type = TransReference.RefType.WHSOrder;
            }
            catch (Exception ex)
            {
                var errStr = ex.Message + Environment.NewLine +
                    ex.InnerException + Environment.NewLine +
                    ex.StackTrace;
                result.Ref3Type = TransReference.RefType.Error;
                result.Reference3 = errStr;
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + " Error found Converting File: " + fileToSend
                                            + ex.Message + ". " + "", System.Drawing.Color.Red);

                MailModule mailModule = new MailModule(Globals.MailServerSettings);
                mailModule.SendMsg(fileToSend, Globals.glAlertsTo, "PCFS Custom XML Conversion Error Found", ex.GetType().Name + " Error found Converting File: " + fileToSend
                    + ex.Message);
                NodeResources.MoveFile(fileToSend, Globals.glFailPath);

            }
            return result;
        }
        private string FixNumber(string bl)
        {
            string result;
            if (bl.Contains("/"))
            {
                result = bl.Replace("/", "-");
            }
            else
            {
                result = bl;
            }

            return result;
        }


        private ProcessResult ConvertCWFile(Customer_Profile cust, String file)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Beginning Cargowise Conversion: " + file);


            if (!String.IsNullOrEmpty(cust.P_METHOD))
            {
                string custMethod;
                string custParams;
                custMethod = cust.P_METHOD;
                custParams = cust.P_PARAMLIST;
                Type custType = this.GetType();


                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                try
                {
                    var varResult = custMethodToRun.Invoke(this, new Object[] { file, cust });
                    if (varResult != null)
                    {
                        //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);

                        result.FileName = file;
                        result.Processed = true;
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    strEx += " " + ex.Message;
                }
            }
            return result;

        }

        private ProcessResult ProcessUDM(string xmlfile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            UniversalInterchange cwFile = new UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                cwFile = (UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }

            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            TransReference trRef = new TransReference();
            List<DataSource> dscoll = new List<DataSource>();
            DataContext dc = new DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;

            if (dc.DataSourceCollection != null)
            {
                try
                {
                    //dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                    dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                }
                catch (Exception)
                {

                    NodeResources.AddText(rtbLog, "XML File Missing DataSourceCollection tag");
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e111;
                    error.Description = "DataSource Collection not found:";
                    error.Severity = "Fatal";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    procerror.ProcId = Guid.Empty;

                    thisResult.FolderLoc = Globals.glFailPath;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

            }
            try
            {
                cwRx++;
                NodeResources.AddLabel(lblCwRx, cwRx.ToString());
                senderID = cwFile.Header.SenderID;
                recipientID = cwFile.Header.RecipientID;
                if (cwFile.Body.BodyField.Shipment.DataContext.EventType != null)
                {
                    eventCode = cwFile.Body.BodyField.Shipment.DataContext.EventType.Code;
                }
                else
                {
                    eventCode = string.Empty;
                }
                if (cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose != null)
                {
                    reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                }
                else
                {
                    reasonCode = string.Empty;
                }

                var cust = GetCustProfileRecord(senderID, recipientID, reasonCode, eventCode);
                if (cust != null)
                {
                    if (string.IsNullOrEmpty(cust.P_SENDERID))
                    {

                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e100;
                        error.Description = "Customer Exists but no profile Found.";
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = xmlfile;
                        thisResult.FolderLoc = Globals.glFailPath;
                        thisResult.Processed = false;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        if (cust.P_MSGTYPE.Trim() == "Conversion")
                        {
                            // Rename the Cargowise XML File to the new Global XML
                            thisResult = ConvertCWFile(cust, xmlfile);
                            return thisResult;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
            }
            return thisResult;
        }

        private Customer_Profile GetCustProfileRecord(string senderID, string recipientID, string reasonCode, string eventCode)
        {

            using (IUnitOfWork uow = new DTO.UnitOfWork(new SatelliteModel(Globals.glConnString)))
            {
                var profileList = uow.CustomerProfiles.Find(x => x.P_SENDERID == senderID && x.P_RECIPIENTID == recipientID
                                                     && x.P_ACTIVE == "Y").OrderByDescending(x => x.P_EVENTCODE).ToList();
                if (profileList.Count > 0)
                {
                    foreach (var cp in profileList)
                    {
                        bool eventFound = false;
                        bool purposeFound = false;
                        //bool matchFound = false;
                        if ((cp.P_REASONCODE.Trim() == "*") && (cp.P_EVENTCODE.Trim() == "*"))
                        {

                        }
                        if (cp.P_REASONCODE.Trim() == reasonCode)
                        {
                            purposeFound = true;
                            if (cp.P_EVENTCODE.Trim() == eventCode)
                            {
                                eventFound = true;
                            }
                            else if (cp.P_EVENTCODE.Trim() == "*")
                            {
                                eventFound = true;
                            }
                        }
                        else if (cp.P_REASONCODE.Trim() == "*")
                        {
                            purposeFound = true;
                            if (cp.P_EVENTCODE.Trim() == eventCode)
                            {
                                eventFound = true;
                            }
                            else if (cp.P_EVENTCODE.Trim() == "*")
                            {
                                eventFound = true;
                            }
                        }

                        if (eventFound && purposeFound)
                        {
                            return cp;
                        }
                    }
                }
                return null;
            }
        }

        private void ProcessCargowiseFiles(string xmlfile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File Found " + Path.GetFileName(xmlfile) + ". Checking for Cargowise File.");
            String ns = CNodeBE.CTCAdapter.GetMessageNamespace(xmlfile);
            String appCode = CNodeBE.CTCAdapter.GetApplicationCode(ns);

            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            ProcessResult canDelete = new ProcessResult();
            if (appCode == "UDM")
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (UDM) XML File :" + xmlfile + ". " + "");
                if (CNodeBE.CTCAdapter.GetXMLType(xmlfile) == "UniversalEvent")
                {
                    //      canDelete = ProcessXUE(xmlfile);
                }
                else
                {
                    canDelete = ProcessUDM(xmlfile);
                }
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is not a valid Cargowise XML File :" + xmlfile + ". Checking Client Specific formats" + "");
                canDelete = ProcessCustomXML(xmlfile);
            }
            if (canDelete.Processed)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                File.Delete(string.IsNullOrEmpty(canDelete.FileName) ? xmlfile : canDelete.FileName);
            }
            else
            {
                if (canDelete.FileName != null)
                {
                    string f = NodeResources.MoveFile(xmlfile, canDelete.FolderLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, System.Drawing.Color.Red);
                    }
                    else
                    { xmlfile = f; }
                }




            }
        }
        void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            XmlSeverityType type = XmlSeverityType.Warning;
            if (Enum.TryParse<XmlSeverityType>("Error", out type))
            {
                if (type == XmlSeverityType.Error)
                {
                    throw new XmlSchemaValidationException(string.Format("Schema Not Found: {0}", e.Message), e.Exception);
                };
            }
        }
        private ProcessResult ProcessCustomXML(string XMLFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            thisResult.FileName = XMLFile;
            using (IUnitOfWork uow = new DTO.UnitOfWork(new SatelliteModel(Globals.glConnString)))
            {
                var profList = uow.CustomerProfiles.Find(x => x.P_XSD != null && x.P_ACTIVE == "Y").ToList();
                if (profList.Count > 0)
                {
                    Boolean processed = false;
                    FileInfo processingFile = new FileInfo(XMLFile);
                    filesRx++;
                    NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
                    totfilesRx++;
                    NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
                    TransReference trRef = new TransReference();
                    String archiveFile;
                    string recipientID = "";
                    string senderID = "";
                    string custMethod = string.Empty;
                    string custParams = string.Empty;

                    while (!processed)
                    {
                        foreach (var profile in profList)
                        {
                            try
                            {
                                string schemaFile = Path.Combine(Globals.glProfilePath, "Lib", profile.P_XSD.Trim());
                                if (IdentifyXml(XMLFile, schemaFile))
                                {
                                    archiveFile = Globals.ArchiveFile(profile.C_PATH.Trim() + "\\Archive", XMLFile);
                                    Type custType = this.GetType();
                                    MethodInfo custMethodToRun = custType.GetMethod(profile.P_METHOD);
                                    try
                                    {
                                        var varResult = custMethodToRun.Invoke(this, new Object[] { XMLFile, profile });
                                        if (varResult != null)
                                        {
                                            //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);
                                            if (varResult is TransReference)
                                            {
                                                TransReference tr = varResult as TransReference;
                                                if (tr.Ref3Type == TransReference.RefType.Error)
                                                {
                                                    processed = true;
                                                    thisResult.Processed = false;
                                                    thisResult.FileName = XMLFile;
                                                    thisResult.FolderLoc = Globals.glFailPath;
                                                    return thisResult;
                                                }
                                                else
                                                {
                                                    processed = true;
                                                    thisResult.Processed = true;
                                                    recipientID = profile.P_RECIPIENTID.Trim();
                                                    senderID = profile.P_SENDERID.Trim();
                                                    processed = true;
                                                    thisResult.Processed = true;
                                                }
                                            }

                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + " Error found Converting File: " + XMLFile
                                            + ex.Message + ". " + "", System.Drawing.Color.Red);

                                        MailModule mailModule = new MailModule(Globals.MailServerSettings);
                                        mailModule.SendMsg(XMLFile, Globals.glAlertsTo, "PCFS Custom XML Conversion Error Found", ex.GetType().Name + " Error found Converting File: " + XMLFile
                                            + ex.Message);
                                        NodeResources.MoveFile(XMLFile, Globals.glFailPath);
                                        thisResult.Processed = true;
                                        thisResult.FolderLoc = Globals.glFailPath;

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                var procerror = new NodeData.Models.Processing_Error();
                                CTCErrorCode error = new CTCErrorCode();
                                string strEx = ex.GetType().Name;
                                string errMsg = "Error Type: " + strEx + Environment.NewLine;
                                errMsg += "Message: " + ex.Message + Environment.NewLine;
                                errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
                                errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
                                errMsg += "Source: " + ex.Source;
                                error.Code = NodeError.e110;
                                error.Description = "Catch All Error :" + ex.Message;
                                error.Severity = "Warning";

                                // File.Move(xmlFile, Path.Combine(Globals.glFailPath, Path.GetFileName(xmlFile)));
                                thisResult.FolderLoc = Globals.glFailPath;
                                thisResult.Processed = false;
                                processed = true;
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: " + strEx + ex.Message + ". " + "", System.Drawing.Color.Red);

                                MailModule mailModule = new MailModule(Globals.MailServerSettings);
                                mailModule.SendMsg(XMLFile, Globals.glAlertsTo, "PCFS Unhandled Exception: ProcessCustomXML", errMsg);
                                return thisResult;
                            }
                        }
                        if (!processed)
                        {

                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Unable to process " + Path.GetFileName(XMLFile) + "No matching routines found. Moving to Failed.";
                            error.Severity = "Warning";

                            thisResult.FolderLoc = Globals.glFailPath;
                            thisResult.Processed = false;
                            string f = NodeResources.MoveFile(XMLFile, Globals.glFailPath);
                            if (f.StartsWith("Warning"))
                            {
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, System.Drawing.Color.Red);
                            }
                            else
                            { XMLFile = f; }


                            thisResult.Processed = false;
                            thisResult.FolderLoc = Globals.glFailPath;
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(XMLFile) + "No matching routines found. Moving to Failed." + "", System.Drawing.Color.Red);
                            processed = true;
                        }

                    }
                }
            }
            return thisResult;
        }



        private bool IdentifyXml(string XMLFile, string schemaFile)
        {
            try
            {
                XmlTextReader sXsd = new XmlTextReader(schemaFile);
                String ns = CNodeBE.CTCAdapter.GetMessageNamespace(XMLFile);
                XmlSchema schema = new XmlSchema();
                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                schema = XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.Schemas.Add(schema);
                settings.ValidationEventHandler += (o, ea) =>
                {
                    throw new XmlSchemaValidationException(
                        string.Format("Schema Not Found: {0}",
                                      ea.Message),
                        ea.Exception);
                };

                using (var stream = new FileStream(XMLFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                using (var cusXML = XmlReader.Create(stream, settings))
                {
                    while (cusXML.Read())
                    {

                    }
                    return true;
                }
            }
            catch (XmlSchemaValidationException ex)
            {
                string strEx = ex.Message;

                return false;
            }
            catch (Exception ex)
            {
                string log = ex.ToLogString(ex.StackTrace);
                return false;
            }
        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters());
        }

        private void eAdapterTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public TransReference SurfAgencyProcessPick(string xmlFile, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            TransReference tr = null;
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            UniversalInterchange toConvert;
            try
            {
                using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
                {
                    XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                    toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
                    fStream.Close();
                }
                XUS.Shipment cwShipment = new XUS.Shipment();
                String senderID = toConvert.Header.SenderID;
                cwShipment = toConvert.Body.BodyField.Shipment;
                DataContext dc = new DataContext();
                dc = cwShipment.DataContext;
                var surfAgency = new SurfboardAgency(_connString);
                tr = new TransReference();
                tr = surfAgency.GeneratePickResponse(cwShipment, cust);
            }
            catch (Exception ex)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + ": " + ex.Message);
            }

            return tr;
        }

        public TransReference CreateCWProductFile(DataTable dt, XMLLocker.Cargowise.NDM.Action partOperation, string senderid, string recipientid)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference tRef = new TransReference();


            if (dt.Rows.Count > 0)
            {

                try
                {
                    XMLLocker.Cargowise.NDM.UniversalInterchange universalInterchange = new XMLLocker.Cargowise.NDM.UniversalInterchange();
                    XMLLocker.Cargowise.NDM.UniversalInterchangeHeader universalInterchangeHeader = new XMLLocker.Cargowise.NDM.UniversalInterchangeHeader();
                    XMLLocker.Cargowise.NDM.UniversalInterchangeBody universalInterchangeBody = new XMLLocker.Cargowise.NDM.UniversalInterchangeBody();
                    XMLLocker.Cargowise.NDM.ProductData productData = new XMLLocker.Cargowise.NDM.ProductData();
                    XMLLocker.Cargowise.NDM.Native native = new XMLLocker.Cargowise.NDM.Native();
                    XMLLocker.Cargowise.NDM.NativeHeader nativeHeader = new XMLLocker.Cargowise.NDM.NativeHeader();
                    XMLLocker.Cargowise.NDM.NativeBody nativeBody = new XMLLocker.Cargowise.NDM.NativeBody();

                    XMLLocker.Cargowise.NDM.NativeProduct nativeProduct = new XMLLocker.Cargowise.NDM.NativeProduct();
                    List<XMLLocker.Cargowise.NDM.NativeProductOrgPartRelation> nativeProductOrgRelationColl = new List<XMLLocker.Cargowise.NDM.NativeProductOrgPartRelation>();
                    XMLLocker.Cargowise.NDM.NativeProductOrgPartRelation nativeProductOrgPartRelation = new XMLLocker.Cargowise.NDM.NativeProductOrgPartRelation();
                    XMLLocker.Cargowise.NDM.NativeProductOrgPartRelationOrgHeader orgPartRelationOrgHeader = new XMLLocker.Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                    List<XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcode> nativeProductBarcodeColl = new List<XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                    XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcode nativeProductBarcode = new XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                    List<XMLLocker.Cargowise.NDM.NativeProductOrgPartUnit> nativeProductOrgPartUnitColl = new List<XMLLocker.Cargowise.NDM.NativeProductOrgPartUnit>();
                    int iCount = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (string.IsNullOrEmpty(dr["UPC"].ToString()))
                        {
                            continue;
                        }
                        else
                        {
                            iCount++;
                            if (iCount == 430)
                            {
                            }

                            nativeProduct = new XMLLocker.Cargowise.NDM.NativeProduct();
                            nativeProduct.PartNum = dr["Name"].ToString().Trim();
                            nativeProduct.StockKeepingUnit = "UNT";
                            decimal num;
                            string wgtValue = string.Empty;
                            wgtValue = dr.ContainsColumn("Weight (kgs)") ? dr["Weight (kgs)"].ToString() : string.Empty;
                            wgtValue = string.IsNullOrEmpty(wgtValue) ?
                                dr.ContainsColumn("WEIGHT KILOS") ? dr["WEIGHT KILOS"].ToString() : string.Empty
                                    : wgtValue;

                            if (decimal.TryParse(wgtValue, out num))
                            {
                                nativeProduct.Weight = num;
                                nativeProduct.WeightSpecified = true;
                                nativeProduct.WeightUQ = "KG";
                            }

                            num = 0;
                            string hgtValue = dr.ContainsColumn("Box Height (cm)") ? dr["Box Height (cm)"].ToString() : string.Empty;
                            hgtValue = string.IsNullOrEmpty(hgtValue) ? dr.ContainsColumn("HEIGHT CM") ? dr["HEIGHT CM"].ToString() : string.Empty
                                : hgtValue;
                            nativeProduct.WeightSpecified = true;
                            if (decimal.TryParse(hgtValue, out num))
                            {
                                nativeProduct.Height = num;
                            }
                            num = 0;
                            string widthValue = dr.ContainsColumn("Box Width (cm)") ? dr["Box Width (cm)"].ToString() : string.Empty;
                            widthValue = string.IsNullOrEmpty(widthValue) ? dr.ContainsColumn("WIDTH CM") ? dr["WIDTH CM"].ToString() : string.Empty
                                : widthValue;

                            nativeProduct.HeightSpecified = true;
                            if (decimal.TryParse(widthValue, out num))
                            {
                                nativeProduct.Width = num;
                                nativeProduct.WidthSpecified = true;
                            }
                            num = 0;
                            string depValue = dr.ContainsColumn("Box Length (cm)") ? dr["Box Length (cm)"].ToString() : string.Empty;
                            depValue = string.IsNullOrEmpty(depValue) ? dr.ContainsColumn("DEPTH CM") ? dr["DEPTH CM"].ToString() : string.Empty
                                  : depValue;
                            if (decimal.TryParse(depValue, out num))
                            {
                                nativeProduct.Depth = num;
                                nativeProduct.DepthSpecified = true;
                            }
                            num = 0;
                            string volValue = dr.ContainsColumn("CBM") ? dr["CBM"].ToString() : string.Empty;
                            volValue = string.IsNullOrEmpty(volValue) ? dr.ContainsColumn("Cubic Area") ? dr["Cubic Area"].ToString() : string.Empty
                                    : volValue;
                            decimal volume;
                            if (decimal.TryParse(volValue, out volume))
                            {
                                nativeProduct.Cubic = volume;
                                nativeProduct.CubicSpecified = true;
                                nativeProduct.CubicUQ = "M3";
                            }
                            nativeProduct.MeasureUQ = "CM";
                            nativeProduct.Desc = dr["Description"].ToString().Trim().Truncate(80);
                            nativeProduct.ActionSpecified = true;
                            nativeProduct.Action = partOperation;
                            nativeProduct.IsActive = true;
                            nativeProduct.IsWarehouseProduct = true;
                            nativeProduct.IsWarehouseProductSpecified = true;
                            nativeProduct.IsBarcoded = true;
                            nativeProduct.IsBarcodedSpecified = true;

                            orgPartRelationOrgHeader = new XMLLocker.Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                            nativeProductOrgRelationColl = new List<XMLLocker.Cargowise.NDM.NativeProductOrgPartRelation>();
                            orgPartRelationOrgHeader.Code = senderid;
                            nativeProductOrgPartRelation.OrgHeader = orgPartRelationOrgHeader;
                            nativeProductOrgPartRelation.Relationship = "OWN";
                            nativeProductOrgPartRelation.Action = partOperation;
                            nativeProductOrgPartRelation.ActionSpecified = true;
                            nativeProductOrgRelationColl.Add(nativeProductOrgPartRelation);

                            //orgPartRelationOrgHeader = new NativeProductOrgPartRelationOrgHeader();
                            //nativeProductOrgPartRelation = new NativeProductOrgPartRelation();
                            //orgPartRelationOrgHeader.Code = "SURFAGESYD";
                            //nativeProductOrgPartRelation.OrgHeader = orgPartRelationOrgHeader;
                            //nativeProductOrgPartRelation.Relationship = "OWN";
                            //nativeProductOrgPartRelation.Action = partOperation;
                            //nativeProductOrgPartRelation.ActionSpecified = true;
                            //nativeProductOrgRelationColl.Add(nativeProductOrgPartRelation);

                            nativeProduct.OrgPartRelationCollection = nativeProductOrgRelationColl.ToArray();
                            if (!String.IsNullOrEmpty(dr["UPC"].ToString().Trim()))
                            {
                                nativeProductBarcodeColl = new List<XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                                nativeProduct.IsBarcoded = true;
                                nativeProductBarcode = new XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                                nativeProductBarcode.Action = XMLLocker.Cargowise.NDM.Action.MERGE;
                                nativeProductBarcode.ActionSpecified = true;
                                nativeProductBarcode.Barcode = dr["UPC"].ToString().Trim();
                                //nativeProductBarcode.PackType.TableName = "RefPackType";
                                XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType productPackType = new XMLLocker.Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType();
                                productPackType.TableName = "RefPackType";
                                productPackType.Code = "UNT";
                                nativeProductBarcode.PackType = productPackType;
                                nativeProductBarcodeColl.Add(nativeProductBarcode);
                                nativeProduct.OrgSupplierPartBarcodeCollection = nativeProductBarcodeColl.ToArray();
                            }
                            else
                            {
                                nativeProduct.IsBarcoded = false;
                            }


                            var cwNSUniversal = new XmlSerializerNamespaces();
                            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                            var cwNSNative = new XmlSerializerNamespaces();
                            cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
                            productData.OrgSupplierPart = nativeProduct;
                            productData.version = "2.0";
                            nativeProduct.Action = partOperation;
                            nativeBody.Any = new[] { productData.AsXmlElement(cwNSNative) };
                            nativeHeader.OwnerCode = senderid;
                            nativeHeader.EnableCodeMapping = true;
                            nativeHeader.EnableCodeMappingSpecified = true;
                            native.version = "2.0";
                            native.Header = nativeHeader;
                            native.Body = nativeBody;
                            universalInterchange.version = "1.1";
                            universalInterchangeHeader.SenderID = senderid;
                            universalInterchangeHeader.RecipientID = recipientid;
                            universalInterchangeBody.Any = new[] { native.AsXmlElement(cwNSUniversal) };
                            universalInterchange.Body = universalInterchangeBody;
                            universalInterchange.Header = universalInterchangeHeader;
                            String cwXML = Path.Combine(Globals.glOutputDir, senderid + "PRODUCT" + dr["Name"].ToString().Trim() + "-" + partOperation.ToString() + ".xml");
                            int iFileCount = 0;
                            while (File.Exists(cwXML))
                            {
                                iFileCount++;
                                cwXML = Path.Combine(Globals.glOutputDir, senderid + "PRODUCT" + dr["Name"].ToString().Trim() + "-" + partOperation + iFileCount + ".xml");
                            }
                            Stream outputCW = File.Open(cwXML, FileMode.Create);
                            StringWriter writer = new StringWriter();
                            XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.NDM.UniversalInterchange));
                            xSer.Serialize(outputCW, universalInterchange, cwNSUniversal);
                            outputCW.Flush();
                            outputCW.Close();
                        }
                    }

                }
                catch (Exception ex)
                {
                    //NodeResources.GenieMessage(NodeResources.GenerateArticleXML("ERP.ArticleEx", "replyFromExport", "articleId", articleID.Trim(), false, "Error Converting XML: " + ex.Message));
                    string strEx = ex.GetType().Name;
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Unable to Convert to Product: " + strEx + "  :" + ex.Message);
                }
            }
            else
            {

            }
            return tRef;
        }

        public TransReference GSI_Orders(string xmlFile, Customer_Profile cust)
        {

            MethodBase m = MethodBase.GetCurrentMethod();
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new TransReference();
            result = null;
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now processing Global Surf Order");
            var gSurf = new GlobalSurf(_connString);
            var common = gSurf.ConvertNetSuiteOrder(xmlFile, cust);
            if (common != null)
            {
                string orderno = common.WarehouseOrders.FirstOrDefault().OrderNumber;
                var cw = new CargowiseFunctions(Globals.glOutputDir)
                {
                    Ref1 = orderno,
                    Ref1Type = "WHSOrder"

                };
                var outFile = cw.CWfromCommon(common);
                result = new TransReference();
                result.Ref1Type = TransReference.RefType.WHSPick;
                result.Reference1 = orderno;
                Globals.ArchiveFile(Globals.glArcLocation, xmlFile);

                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New Order created: " + cust.P_SENDERID + ": " + orderno, Color.Green);
            }



            return result;
        }


        private string[] GetAddressFromText(string address, ref int counter)
        {
            if (counter > 10)
            {
                return null;
            }

            var addTemp = address.Replace(',', ' ');

            string[] add = RemoveBlanks(addTemp.Split(' '));
            counter++;

            try
            {
                //  add = address.Replace(',', ' ').Split(' ');
                //add = add.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                if (add.Length == 1)
                {
                    add = address.Split(',');
                }
                if (add.Length == 2)
                {
                    if (!add[1].All(Char.IsDigit))
                    {
                        add = GetAddressFromText(address.Replace(',', ' '), ref counter);
                    }


                }
            }
            catch (StackOverflowException)
            {
                add = null;
            }


            return add;

        }

        private string[] RemoveBlanks(string[] val)
        {
            List<string> valList = val.ToList();
            try
            {
                for (int i = 0; i < valList.Count; i++)
                {

                    if (string.IsNullOrEmpty(valList[i]))
                    {
                        valList.RemoveAt(i);
                    }

                }
            }
            catch (Exception)
            {

            }
            return valList.ToArray();
        }

        public void ImportPO(DataTable tb, string refPO, string senderCode, string recipCode)
        {

            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            {

                UniversalInterchange uInt = new UniversalInterchange();
                UniversalInterchangeHeader uHeader = new UniversalInterchangeHeader();
                uHeader.RecipientID = recipCode;
                uHeader.SenderID = senderCode;
                uInt.Header = uHeader;
                UniversalInterchangeBody uBody = new UniversalInterchangeBody();
                UniversalShipmentData uShipData = new UniversalShipmentData();
                XUS.Shipment shipment = new XUS.Shipment();
                DataContext dc = new DataContext();
                List<DataTarget> dataTargetColl = new List<DataTarget>();
                DataTarget dataTarget = new DataTarget();
                dataTarget.Type = "WarehouseReceive";
                dataTargetColl.Add(dataTarget);
                dc.DataTargetCollection = dataTargetColl.ToArray();
                ShipmentOrder pOrder = new ShipmentOrder();
                pOrder.OrderNumber = refPO;
                pOrder.ClientReference = refPO;
                shipment.DataContext = dc;
                //TODO 
                //Create Variable to allow for Return Authority
                CodeDescriptionPair codePair = new CodeDescriptionPair();
                codePair.Code = "REC";
                codePair.Description = "GOODS RECEIPT";
                pOrder.Type = codePair;
                ShipmentOrderWarehouse warehouse = new ShipmentOrderWarehouse();
                warehouse.Code = "SYD";
                warehouse.Name = "PCFS SYDNEY";
                pOrder.Warehouse = warehouse;
                List<Date> dateColl = new List<Date>();
                Date orderDate = new Date();
                orderDate.Type = DateType.BookingConfirmed;
                orderDate.Value = DateTime.Now.ToString("s");
                orderDate.IsEstimate = false;
                orderDate.IsEstimateSpecified = true;
                dateColl.Add(orderDate);
                pOrder.DateCollection = dateColl.ToArray();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                int i = 0;
                orderLine.Content = CollectionContent.Complete;
                List<ShipmentOrderOrderLineCollectionOrderLine> olColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                orderLine.ContentSpecified = true;
                foreach (DataRow dr in tb.Rows)
                {
                    i++;
                    ShipmentOrderOrderLineCollectionOrderLine oLine = new ShipmentOrderOrderLineCollectionOrderLine();
                    oLine.ExpectedQuantity = decimal.Parse(dr["QTY"].ToString());
                    oLine.ExpectedQuantitySpecified = true;

                    oLine.LineNumber = i;
                    oLine.LineNumberSpecified = true;
                    CodeDescriptionPair cdUnit = new CodeDescriptionPair();
                    cdUnit.Code = "UNT";
                    oLine.OrderedQtyUnit = cdUnit;
                    XUS.Product product = new XUS.Product();
                    product.Code = dr["CODE"].ToString(); ;
                    product.Description = dr["DESC"].ToString();
                    oLine.Product = product;
                    olColl.Add(oLine);
                }
                orderLine.OrderLine = olColl.ToArray();
                pOrder.OrderLineCollection = orderLine;
                List<OrganizationAddress> OrgAddColl = new List<OrganizationAddress>();
                OrganizationAddress oa = new OrganizationAddress();
                oa.AddressType = "ConsignorDocumentaryAddress";
                oa.OrganizationCode = senderCode;
                OrgAddColl.Add(oa);
                shipment.OrganizationAddressCollection = OrgAddColl.ToArray();
                shipment.Order = pOrder;
                uShipData.Shipment = shipment;
                uBody.BodyField = uShipData;
                uShipData.version = "1.1";
                uShipData.Shipment = shipment;
                String cwXML = Path.Combine(Globals.glOutputDir, senderCode + "PO" + pOrder.OrderNumber.Trim() + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(Globals.glOutputDir, senderCode + "PO" + pOrder.OrderNumber.Trim() + iFileCount + ".xml");
                }
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                StringWriter writer = new StringWriter();
                uInt.Body = uBody;
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                xSer.Serialize(outputCW, uInt, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }

        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private void mapMasterListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMappingEnum mappingEnum = new frmMappingEnum();
            mappingEnum.Show();
        }

        private void newMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMappingDefinition mappingDefinition = new frmMappingDefinition();
            mappingDefinition.Show();
        }

        public TransReference MoveItConnNote(string xmlFile, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new TransReference();
            UniversalInterchange toConvert;
            try
            {
                using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
                {
                    XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                    toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
                    fStream.Close();
                }
                MoveIt.CreateMoveItFile(toConvert, cust);

            }

            catch (Exception)
            {

            }


            return result;
        }

        public TransReference GSI_Process_Pick(string xmlFile, Customer_Profile cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            TransReference result = new PCFSatellite.TransReference();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Order Receipt");
            UniversalInterchange toConvert;
            try
            {
                string shipmentNo = "";
                using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
                {
                    XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                    toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
                    fStream.Close();
                }
                XUS.Shipment cwShipment = new XUS.Shipment();
                String senderID = toConvert.Header.SenderID;
                cwShipment = toConvert.Body.BodyField.Shipment;
                DataContext dc = new DataContext();
                dc = cwShipment.DataContext;

                DataSource[] dscoll;
                dscoll = dc.DataSourceCollection;
                string activityReply = string.Empty;
                string methodReply = string.Empty;
                List<GLOSURF.Shipments> gloShipmentColl = new List<GLOSURF.Shipments>();
                bool pickFound = false;
                foreach (DataSource ds in dscoll)
                {
                    if (ds.Type == "WarehouseOrder")
                    {
                        pickFound = true;
                        shipmentNo = ds.Key;
                    }
                }
                if (pickFound)
                {


                    GLOSURF.ShipmentHeader header = new ShipmentHeader();
                    GLOSURF.ShipmentHeaderShipmentHeader shipmentHeader = new ShipmentHeaderShipmentHeader();
                    shipmentHeader.TradingPartnerId = "362ALLGLOBALSUR";
                    shipmentHeader.BillOfLadingNumber = shipmentNo;
                    shipmentHeader.ShipDate = DateTime.Now;
                    shipmentHeader.ShipmentIdentification = shipmentNo;

                    shipmentHeader.TsetPurposeCode = ShipmentHeaderShipmentHeaderTsetPurposeCode.Item06;
                    shipmentHeader.TsetPurposeCodeSpecified = true;
                    header.ShipmentHeader1 = shipmentHeader;
                    List<GLOSURF.ShipmentHeaderDates> datesColl = new List<ShipmentHeaderDates>();
                    GLOSURF.ShipmentHeaderDates dates;

                    foreach (var mileStone in cwShipment.MilestoneCollection)
                    {
                        dates = new ShipmentHeaderDates();

                        switch (mileStone.EventCode)
                        {
                            case "WHE":  //Warehouse Entered Date
                                dates.Date = DateTime.Parse(mileStone.ActualDate);
                                dates.DateSpecified = true;
                                dates.DateTimeQualifier = ShipmentHeaderDatesDateTimeQualifier.Item006;
                                dates.DateTimeQualifierSpecified = true;

                                datesColl.Add(dates);
                                break;


                            case "FIN":  // Warehouse Picking Finalized
                                if (mileStone.ActualDate != null)
                                {
                                    dates.Date = DateTime.Parse(mileStone.ActualDate);
                                    dates.DateSpecified = true;
                                    dates.DateTimeQualifier = ShipmentHeaderDatesDateTimeQualifier.Item037;
                                    dates.DateTimeQualifierSpecified = true;
                                    datesColl.Add(dates);
                                }
                                break;
                        }



                    }
                    header.Dates = datesColl.ToArray();

                    List<GLOSURF.ShipmentHeaderAddress> addressColl = new List<ShipmentHeaderAddress>();
                    foreach (OrganizationAddress org in cwShipment.OrganizationAddressCollection)
                    {
                        GLOSURF.ShipmentHeaderAddress address = new ShipmentHeaderAddress();
                        try
                        {
                            switch (org.AddressType)
                            {
                                case "ConsignorDocumentaryAddress":
                                    address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.BOWN;

                                    break;

                                case "ConsigneeAddress":
                                    address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.CN;

                                    break;
                                case "TransportCompanyDocumentaryAddress":
                                    address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.TP;

                                    break;
                                case "ConsignorPickupDeliveryAddress":
                                    address.AddressTypeCode = ShipmentHeaderAddressAddressTypeCode.WH;

                                    break;
                            }
                            address.AddressTypeCodeSpecified = true;
                            address.AddressName = org.CompanyName;
                            if (org.Address1 != null)
                            {
                                address.Address1 = org.Address1;
                            }

                            if (org.Address2 != null)
                            {
                                address.Address2 = org.Address2;
                            }

                            if (org.City != null)
                            {
                                address.City = org.City;
                            }

                            if (org.Postcode != null)
                            {
                                address.PostalCode = org.Postcode;
                            }

                            if (org.State != null)
                            {
                                address.State = org.State;
                            }

                            if (org.Country != null)
                            {
                                address.Country = org.Country.Name;
                            }

                            addressColl.Add(address);
                        }
                        catch (Exception ex)
                        {
                            string strex = ex.GetType().Name;
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Error Adding Address(es)." + ex.Message);

                        }


                    }
                    header.Address = addressColl.ToArray();
                    List<GLOSURF.ShipmentHeaderQuantityAndWeight> quantColl = new List<ShipmentHeaderQuantityAndWeight>();
                    GLOSURF.ShipmentHeaderQuantityAndWeight quant = new ShipmentHeaderQuantityAndWeight();
                    switch (cwShipment.TotalNoOfPacksPackageType.Code)
                    {
                        case "PCE":
                            quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCS;
                            break;
                        default:
                            quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCS;
                            break;

                    }
                    quant.LadingQuantity = cwShipment.TotalNoOfPacks;
                    quant.LadingQuantitySpecified = true;
                    quant.Volume = cwShipment.TotalVolume;
                    quant.VolumeSpecified = true;
                    quant.VolumeUOM = ShipmentHeaderQuantityAndWeightVolumeUOM.CR;
                    quant.VolumeUOMSpecified = true;
                    quant.Weight = cwShipment.TotalWeight;
                    quant.WeightSpecified = true;
                    quant.WeightUOM = ShipmentHeaderQuantityAndWeightWeightUOM.KG;
                    quant.WeightUOMSpecified = true;
                    switch (cwShipment.TotalNoOfPacksPackageType.Code)
                    {
                        case "PCE":
                            quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCS;
                            break;
                        case "PAK":
                            quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PCK;
                            break;
                        case "CTN":
                            quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.CTN;
                            break;
                        case "PLT":
                            quant.PackingMedium = ShipmentHeaderQuantityAndWeightPackingMedium.PLT;
                            break;
                    }

                    quant.PackingMediumSpecified = true;
                    quantColl.Add(quant);
                    header.QuantityAndWeight = quantColl.ToArray();
                    GLOSURF.ShipmentOrderLevel orderLevel = new ShipmentOrderLevel();
                    GLOSURF.ShipmentOrderLevelOrderHeader orderHeader = new ShipmentOrderLevelOrderHeader();
                    orderHeader.PurchaseOrderNumber = cwShipment.Order.OrderNumber;
                    List<GLOSURF.ShipmentHeaderReferences> referencesColl = new List<ShipmentHeaderReferences>();
                    GLOSURF.ShipmentHeaderReferences references = new ShipmentHeaderReferences();
                    references.ReferenceID = shipmentNo;
                    references.ReferenceQual = ShipmentHeaderReferencesReferenceQual.Item2I;
                    references.ReferenceQualSpecified = true;
                    referencesColl.Add(references);
                    references = new ShipmentHeaderReferences();
                    references.ReferenceID = cwShipment.Order.TransportReference;
                    references.ReferenceQual = ShipmentHeaderReferencesReferenceQual.CI;
                    references.ReferenceQualSpecified = true;
                    referencesColl.Add(references);
                    header.References = referencesColl.ToArray();
                    if (!string.IsNullOrEmpty(cwShipment.LocalProcessing.DeliveryRequiredBy))
                    {
                        orderHeader.DeliveryDate = DateTime.Parse(cwShipment.LocalProcessing.DeliveryRequiredBy);
                        orderHeader.DeliveryDateSpecified = true;
                    }
                    else
                    {
                        orderHeader.DeliveryDateSpecified = false;
                    }
                    orderLevel.OrderHeader = orderHeader;
                    GLOSURF.ShipmentOrderLevelPackLevel packLevel = new ShipmentOrderLevelPackLevel();
                    GLOSURF.ShipmentOrderLevelPackLevelPack pack = new ShipmentOrderLevelPackLevelPack();
                    pack.PackLevelType = ShipmentOrderLevelPackLevelPackPackLevelType.P;
                    pack.PackLevelTypeSpecified = true;
                    packLevel.Pack = pack;
                    List<GLOSURF.ShipmentOrderLevelPackLevelItemLevel> itemLevelColl = new List<ShipmentOrderLevelPackLevelItemLevel>();
                    bool partShip = false;
                    foreach (ShipmentOrderOrderLineCollectionOrderLine line in cwShipment.Order.OrderLineCollection.OrderLine)
                    {
                        GLOSURF.ShipmentOrderLevelPackLevelItemLevel item = new ShipmentOrderLevelPackLevelItemLevel();
                        ShipmentOrderLevelPackLevelItemLevelShipmentLine sline = new ShipmentOrderLevelPackLevelItemLevelShipmentLine();
                        sline.LineSequenceNumber = line.LineNumber.ToString();

                        sline.BuyerPartNumber = line.Product.Code;
                        sline.ShipQty = line.QuantityMet;
                        sline.ShipQtySpecified = true;
                        sline.OrderQty = line.OrderedQty;
                        sline.OrderQtySpecified = true;
                        sline.ShipQtyUOM = ShipmentOrderLevelPackLevelItemLevelShipmentLineShipQtyUOM.PC;
                        sline.ShipQtyUOMSpecified = true;
                        item.ShipmentLine = sline;
                        GLOSURF.ShipmentOrderLevelPackLevelItemLevelCarrierInformation carrier = new ShipmentOrderLevelPackLevelItemLevelCarrierInformation();

                        if (line.ShortfallQuantity > 0)
                        {
                            partShip = true;
                            carrier.StatusCode = ShipmentOrderLevelPackLevelItemLevelCarrierInformationStatusCode.PR;

                        }
                        else
                        {
                            carrier.StatusCode = ShipmentOrderLevelPackLevelItemLevelCarrierInformationStatusCode.CL;

                        }

                        carrier.StatusCodeSpecified = true;
                        List<ShipmentOrderLevelPackLevelItemLevelCarrierInformation> carrierColl = new List<ShipmentOrderLevelPackLevelItemLevelCarrierInformation>();
                        carrierColl.Add(carrier);
                        item.CarrierInformation = carrierColl.ToArray();
                        List<GLOSURF.ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription> productDescColl = new List<ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription>();
                        GLOSURF.ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription prodDescr = new ShipmentOrderLevelPackLevelItemLevelProductOrItemDescription();
                        prodDescr.ProductDescription = line.Product.Description;
                        productDescColl.Add(prodDescr);
                        item.ProductOrItemDescription = productDescColl.ToArray();
                        itemLevelColl.Add(item);
                    }

                    List<GLOSURF.ShipmentHeaderCarrierInformation> headerCarrierInformation = new List<ShipmentHeaderCarrierInformation>();
                    GLOSURF.ShipmentHeaderCarrierInformation hc = new ShipmentHeaderCarrierInformation();
                    if (partShip)
                    {
                        hc.StatusCode = ShipmentHeaderCarrierInformationStatusCode.PR;
                    }
                    else
                    {
                        hc.StatusCode = ShipmentHeaderCarrierInformationStatusCode.CL;
                    }
                    hc.CarrierTransMethodCode = ShipmentHeaderCarrierInformationCarrierTransMethodCode.L;
                    hc.CarrierTransMethodCodeSpecified = true;
                    hc.StatusCodeSpecified = true;
                    headerCarrierInformation.Add(hc);
                    header.CarrierInformation = headerCarrierInformation.ToArray();
                    packLevel.Items = itemLevelColl.ToArray();
                    List<GLOSURF.ShipmentOrderLevelPackLevel> plColl = new List<ShipmentOrderLevelPackLevel>();
                    plColl.Add(packLevel);
                    orderLevel.Items = plColl.ToArray();
                    List<GLOSURF.ShipmentOrderLevel> ordColl = new List<ShipmentOrderLevel>();
                    ordColl.Add(orderLevel);
                    List<GLOSURF.Shipment> shipmentColl = new List<GLOSURF.Shipment>();
                    GLOSURF.Shipment gloShipment = new GLOSURF.Shipment();
                    gloShipment.Items = ordColl.ToArray();
                    gloShipment.Header = header;
                    shipmentColl.Add(gloShipment);
                    GLOSURF.Shipments ship = new Shipments();
                    ship.Shipment = shipmentColl.ToArray();
                    String cwXML = Path.Combine(Globals.glOutputDir, "GLOSUR" + "PICK" + cwShipment.Order.OrderNumber.Trim() + ".xml");
                    int iFileCount = 0;
                    while (File.Exists(cwXML))
                    {
                        iFileCount++;
                        cwXML = Path.Combine(Globals.glOutputDir, "GLOSUR" + "PICK" + cwShipment.Order.OrderNumber.Trim() + iFileCount + ".xml");
                    }
                    Stream outputGLO = File.Open(cwXML, FileMode.Create);
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(GLOSURF.Shipments));
                    var gloNS = new XmlSerializerNamespaces();
                    gloNS.Add("", "http://www.spscommerce.com/RSX");

                    xSer.Serialize(outputGLO, ship, gloNS);
                    outputGLO.Flush();
                    outputGLO.Close();
                    string archiveFile = Globals.ArchiveFile(Globals.glArcLocation, cwXML);
                    //    NodeResources.AddTransaction(NodeResources.GetCustID(Globals.glCustCode), Guid.Empty, xmlFile, "R", true, result, archiveFile);
                    archiveFile = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);


                    //  gloShipment.Header = header;




                }
            }
            catch (InvalidOperationException iop)
            {
                string strEx = iop.Message;

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }

            return result;
        }


    }
}

