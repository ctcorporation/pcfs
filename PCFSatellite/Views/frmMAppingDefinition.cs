﻿using PCFSatellite.DTO;
using PCFSatellite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PCFSatellite
{


    public partial class frmMappingDefinition : Form
    {

        #region Members 
        DataTable tableMapping;
        #endregion
        private BindingSource bsMap = new BindingSource();

        public frmMappingDefinition()
        {
            InitializeComponent();


        }



        private void bbReadCSV_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.FileName = edReadCSV.Text;
            fd.ShowDialog();
            edReadCSV.Text = fd.FileName;

        }

        private void bbReadFields_Click(object sender, EventArgs e)
        {


            if (File.Exists(edReadCSV.Text))
            {
                var headers = GetHeaderLocation(edReadCSV.Text);
                if (headers == null)
                {
                    return;
                }
                if (GetDataLocation(edReadCSV.Text))
                {
                    FillFieldList(headers);
                }



            }

        }

        private bool GetDataLocation(string data)
        {
            string bodyText = string.Empty;
            bool dataFound = false;
            using (StreamReader sr = new StreamReader(data))
            {
                bodyText = sr.ReadLine();
                try
                {

                    int curRow = 0;
                    while (!dataFound)
                    {
                        DialogResult dr = MessageBox.Show("Do the following look to be the First Data Line ?" + Environment.NewLine + "(" + bodyText + ")", "Locating first line of data", MessageBoxButtons.YesNoCancel);
                        switch (dr)
                        {
                            case DialogResult.Yes:
                                dataFound = true;


                                break;

                            case DialogResult.No:
                                if (!sr.EndOfStream)
                                {
                                    bodyText = sr.ReadLine();
                                }

                                break;
                            case DialogResult.Cancel:
                                dataFound = false;
                                continue;
                                break;
                        }
                        curRow++;
                    }

                }
                catch (Exception)
                {
                }
                sr.Dispose();
                return dataFound;

            };
        }

        private void FillFieldList(List<string> fields)
        {

            var data = (from x in fields
                        select new DataItem { StringVal = x }).ToList();
            BindingSource bsFrom = new BindingSource { DataSource = data };

            lbFromFieldList.DisplayMember = "StringVal";
            lbFromFieldList.ValueMember = "StringVal";
            lbFromFieldList.DataSource = bsFrom;
        }

        private List<string> GetHeaderLocation(string data)
        {
            List<string> result = new List<string>();
            string headerText = string.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(data))
                {
                    headerText = sr.ReadLine();
                    string cDelimiter = NodeResources.GetDelimiter(headerText);
                    lbFromFieldList.Items.Clear();
                    try
                    {
                        if (!string.IsNullOrEmpty(cDelimiter))
                        {
                            string[] headers = headerText.Split(Convert.ToChar(cDelimiter));
                            if (headers.Length > 0)
                            {
                                edDelimiter.Text = cDelimiter;
                                edFieldCount.Text = headers.Length.ToString();
                                if (headers[0].Substring(0, 1) == "\"")
                                {
                                    cbQuotations.Checked = true;
                                }
                                else
                                {
                                    cbQuotations.Checked = false;
                                }
                                bool headerFound = false;
                                int curRow = 0;
                                while (!headerFound)
                                {
                                    DialogResult dr = MessageBox.Show("Do the following look to be Field Names ?" + Environment.NewLine + "(" + headerText + ")", "Field Name Testing", MessageBoxButtons.YesNoCancel);
                                    switch (dr)
                                    {
                                        case DialogResult.Yes:
                                            headerFound = true;
                                            edHeaderRow.Text = curRow.ToString();

                                            result = headerText.Split(Convert.ToChar(cDelimiter)).ToList();
                                            break;

                                        case DialogResult.No:
                                            if (!sr.EndOfStream)
                                            {
                                                headerText = sr.ReadLine();
                                                cDelimiter = NodeResources.GetDelimiter(headerText);
                                                lbFromFieldList.Items.Clear();
                                            }
                                            break;
                                        case DialogResult.Cancel:
                                            lbFromFieldList.Items.Clear();
                                            return null;
                                            break;
                                    }
                                    curRow++;
                                }


                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        var err = ex.GetType().Name;


                    }
                    sr.Dispose();
                    return result;
                };
            }
            catch (IOException ex)
            {
                MessageBox.Show("File is in Use. Please close file in other program and try again", "Read File in use Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return null;
        }

        private void frmMappingDefinition_Load(object sender, EventArgs e)

        {
            FillOperationList();
            tableMapping = new DataTable();
            tableMapping.Columns.Add("ConvertType", typeof(string));
            tableMapping.Columns.Add("MapFrom", typeof(string));
            tableMapping.Columns.Add("MapTo", typeof(string));
            tableMapping.Columns.Add("DataType", typeof(string));
            dgMapping.DataSource = tableMapping;

            cmbDataTypes.Items.Clear();
            cmbDataTypes.DisplayMember = "Text";
            cmbDataTypes.ValueMember = "Value";
            var dataItems = new[]
            {
                new {Text = "(none selected)", Value="N/A"},
                new {Text = "Boolean", Value="BOO"},
                new {Text = "Date/Time", Value="DAT"},
                new {Text = "Decimal", Value="NUM"},
                new {Text = "Integer", Value="INT"},
                new {Text = "String", Value="STR"}
            };
            cmbDataTypes.DataSource = dataItems;
        }

        private void FillOperationList()
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteModel(Globals.connString())))
            {
                var allOps = (uow.MapEnums.GetAll()).ToList();

                if (allOps.Count > 0)
                {
                    var mapOps = (from m in allOps
                                  select new DataItem { StringVal = m.E_PROCESSNAME, Val = m.E_FIELDTYPE }).ToList();
                    bsMap.DataSource = mapOps.DistinctBy(x => x.StringVal);

                    cmdOperationList.ValueMember = "Val";
                    cmdOperationList.DisplayMember = "StringVal";
                    cmdOperationList.DataSource = bsMap;

                }
            }

            //SqlDataAdapter da = new SqlDataAdapter("Select E_ProcessName from Vw_MappingOperations order by E_ProcessName", sqlConn);
            //DataSet ds = new DataSet();
            //da.Fill(ds, "Enums");
            //DataRow dr = ds.Tables["Enums"].NewRow();
            //dr["E_PROCESSNAME"] = "(none selected)";
            //ds.Tables["Enums"].Rows.InsertAt(dr, 0);
            //cmdOperationList.DataSource = ds.Tables["Enums"];

        }

        private void bbCLose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdOperationList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmdOperationList.SelectedText != "(none selected)")
            {
                GetEnums(cmdOperationList.Text);
            }
        }

        private void GetEnums(string selectedText)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteModel(Globals.connString())))
            {
                var enums = (uow.MapEnums.Find(x => x.E_PROCESSNAME == selectedText).OrderBy(x => x.E_FIELDNAME)).ToList();

                if (enums.Count > 0)
                {
                    var mapOps = (from m in enums
                                  select new DataItem { StringVal = m.E_FIELDNAME }).ToList();


                    lbDataFieldList.DisplayMember = "StringVal";
                    lbDataFieldList.ValueMember = "StringVal";
                    BindingSource bsFrom = new BindingSource { DataSource = mapOps.DistinctBy(x => x.StringVal) };
                    lbDataFieldList.DataSource = bsFrom;

                }
            }


        }

        private void btnAddField_Click(object sender, EventArgs e)
        {
            AddField();
        }

        private void btnAddRequiredField_Click(object sender, EventArgs e)
        {
            AddRequiredField();
        }

        private void btnAddMapping_Click(object sender, EventArgs e)
        {
            if (cmbDataTypes.SelectedItem.ToString() != "N/A")
            {
                if (btnAddMapping.Text.Split(':').Count() == 2)
                {
                    DataRow dr = tableMapping.NewRow();
                    dr["ConvertType"] = cmdOperationList.SelectedValue;
                    var currentLabel = btnAddMapping.Text.Split(':');
                    if (currentLabel.Length == 2)
                    {
                        dr["MapFrom"] = currentLabel[1];
                        dr["MapTo"] = currentLabel[0];
                        dr["DataType"] = cmbDataTypes.SelectedValue.ToString();
                        tableMapping.Rows.Add(dr);
                        foreach (var item in lbDataFieldList.Items.Cast<DataItem>().ToList())

                        {
                            //if (item.StringVal == currentLabel[1].Trim())
                            //{
                            //    lbDataFieldList.Items.Remove(item);
                            //}
                        }
                        foreach (var item in lbFromFieldList.Items.Cast<DataItem>().ToList())
                        {
                            //if (item.StringVal == currentLabel[0].Trim())
                            //{
                            //    lbFromFieldList.Items.Remove(item);
                            //}
                        }
                    }
                }
            }
        }

        private void lbFromFieldList_DoubleClick(object sender, EventArgs e)
        {
            AddField();
        }

        private void AddField()
        {
            if (cmdOperationList.SelectedValue.ToString() != "N/A")
            {
                DataItem selItem = (DataItem)lbFromFieldList.SelectedItem;
                var currentLabel = btnAddMapping.Text.Split(':');
                if (currentLabel.Length == 2)
                {
                    currentLabel[0] = selItem.StringVal;
                    btnAddMapping.Text = currentLabel[0] + " : " + currentLabel[1];
                }

                else
                {

                    btnAddMapping.Text = selItem.StringVal + " : ";
                }
            }
        }

        private void lbDataFieldList_DoubleClick(object sender, EventArgs e)
        {
            AddRequiredField();
        }

        private void AddRequiredField()
        {
            if (cmdOperationList.SelectedValue.ToString() != "N/A")
            {
                DataItem selItem = (DataItem)lbDataFieldList.SelectedItem;
                var currentLabel = btnAddMapping.Text.Split(':');
                if (currentLabel.Length == 2)
                {
                    currentLabel[1] = selItem.StringVal;
                    btnAddMapping.Text = currentLabel[0] + " : " + currentLabel[1];
                }

                else
                {

                    btnAddMapping.Text = " : " + selItem.StringVal;

                }
            }
        }

        private void bbSaveMapping_Click(object sender, EventArgs e)
        {
            if (cmdOperationList.SelectedValue.ToString() != "N/A")
            {
                if (string.IsNullOrEmpty(edMapDescr.Text))
                {
                    MessageBox.Show("You cannot have a blank Map Description", "Save Mapping Operation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                using (IUnitOfWork uow = new UnitOfWork(new SatelliteModel(Globals.connString())))
                {
                    var map = uow.MapOperations.Find(x => x.MD_MapDescription.ToLower().Trim() == edMapDescr.Text.ToLower()
                                                       && x.MD_Type == cmdOperationList.SelectedValue.ToString()).ToList();
                    if (map.Count > 0)
                    {
                        foreach (var m in map)
                        {
                            uow.MapOperations.Remove(m);
                        }
                    }
                    foreach (DataRow row in tableMapping.Rows)
                    {

                        MapOperation md = new MapOperation
                        {
                            MD_ID = Guid.NewGuid(),
                            MD_MapDescription = edMapDescr.Text,
                            MD_Type = cmdOperationList.SelectedValue.ToString(),
                            MD_FromField = row["MapFrom"].ToString(),
                            MD_ToField = row["MapTo"].ToString(),
                            MD_DataType = row["DataType"].ToString()

                        };
                        uow.MapOperations.Add(md);

                    }

                    uow.Complete();
                }

                MessageBox.Show("Mapping Profile Saved.", "Create Custom Mapping", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //FillMapList();


            }
            else
            {
                MessageBox.Show("Please ensure you have selected a Data Type before adding to the mapping", "Missing Data Type selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }


}
class DataItem
{
    public string StringVal { get; set; }
    public string Val { get; set; }
}
