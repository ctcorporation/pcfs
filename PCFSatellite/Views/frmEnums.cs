﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace PCFSatellite
{
    public partial class frmEnums : Form
    {
        SqlConnection sqlConn;
        public Guid cw_ID;
        public frmEnums()
        {
            InitializeComponent();
        }

        private void frmEnums_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            LoadData();
        }


        private void LoadData()
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT CW_ID, CW_ENUMTYPE, CW_ENUM, CW_MAPVALUE FROM Cargowise_Enums ORDER BY CW_ENUMTYPE, CW_MAPVALUE ", sqlConn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ENUMS");
            dgEnums.DataSource = ds;
            dgEnums.DataMember = "ENUMS";




        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(edCargowiseValue.Text) && !string.IsNullOrEmpty(edCustomValue.Text) && !string.IsNullOrEmpty(edDatatype.Text))
            {


                SqlCommand addEnum = new SqlCommand();
                addEnum.CommandType = CommandType.StoredProcedure;
                addEnum.Connection = sqlConn;

                if (string.IsNullOrEmpty(NodeResources.GetEnum(edDatatype.Text, edCustomValue.Text)))
                {
                    addEnum.CommandText = "ADD_Enums";


                }
                else
                {
                    addEnum.CommandText = "EDIT_Enums";
                    SqlParameter cwID = addEnum.Parameters.Add("@CW_ID", SqlDbType.UniqueIdentifier);
                    cwID.Value = cw_ID;
                }
                SqlParameter type = addEnum.Parameters.AddWithValue("@CW_ENUMTYPE", edDatatype.Text);
                SqlParameter cwenum = addEnum.Parameters.AddWithValue("@CW_ENUM", edCargowiseValue.Text);
                SqlParameter mapvalue = addEnum.Parameters.AddWithValue("@CW_MAPVALUE", edCustomValue.Text);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                addEnum.ExecuteNonQuery();
                cw_ID = Guid.Empty;
                LoadData();
            }

        }

        private void dgEnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cw_ID = (Guid)dgEnums[0, e.RowIndex].Value;
            edCargowiseValue.Text = dgEnums["CWValue", e.RowIndex].Value.ToString();
            edCustomValue.Text = dgEnums["Custom", e.RowIndex].Value.ToString();
            edDatatype.Text = dgEnums["Type", e.RowIndex].Value.ToString();
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            edDatatype.Text = "";
            edCustomValue.Text = "";
            edCargowiseValue.Text = "";
            cw_ID = Guid.Empty;
            edDatatype.Focus();
        }

        private void edDatatype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true;
            }
        }
    }
}
