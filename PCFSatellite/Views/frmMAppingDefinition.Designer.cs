﻿namespace PCFSatellite
{
    partial class frmMappingDefinition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMappingDefinition));
            this.bbCLose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdOperationList = new System.Windows.Forms.ComboBox();
            this.lbDataFieldList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edReadCSV = new System.Windows.Forms.TextBox();
            this.bbReadCSV = new System.Windows.Forms.Button();
            this.bbReadFields = new System.Windows.Forms.Button();
            this.lbFromFieldList = new System.Windows.Forms.ListBox();
            this.bbSaveMapping = new System.Windows.Forms.Button();
            this.edDelimiter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbQuotations = new System.Windows.Forms.CheckBox();
            this.cbHasHeaders = new System.Windows.Forms.CheckBox();
            this.edFieldCount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edHeaderRow = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edDataRow = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDataTypes = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddMapping = new System.Windows.Forms.Button();
            this.btnAddField = new System.Windows.Forms.Button();
            this.btnAddRequiredField = new System.Windows.Forms.Button();
            this.dgMapping = new System.Windows.Forms.DataGridView();
            this.Col1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.edMapDescr = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).BeginInit();
            this.SuspendLayout();
            // 
            // bbCLose
            // 
            this.bbCLose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbCLose.Location = new System.Drawing.Point(882, 462);
            this.bbCLose.Name = "bbCLose";
            this.bbCLose.Size = new System.Drawing.Size(75, 23);
            this.bbCLose.TabIndex = 0;
            this.bbCLose.Text = "&Close";
            this.bbCLose.UseVisualStyleBackColor = true;
            this.bbCLose.Click += new System.EventHandler(this.bbCLose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Operation";
            // 
            // cmdOperationList
            // 
            this.cmdOperationList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmdOperationList.FormattingEnabled = true;
            this.cmdOperationList.Location = new System.Drawing.Point(145, 38);
            this.cmdOperationList.Name = "cmdOperationList";
            this.cmdOperationList.Size = new System.Drawing.Size(271, 21);
            this.cmdOperationList.TabIndex = 2;
            this.cmdOperationList.SelectedIndexChanged += new System.EventHandler(this.cmdOperationList_SelectedIndexChanged);
            // 
            // lbDataFieldList
            // 
            this.lbDataFieldList.FormattingEnabled = true;
            this.lbDataFieldList.Location = new System.Drawing.Point(644, 169);
            this.lbDataFieldList.Name = "lbDataFieldList";
            this.lbDataFieldList.Size = new System.Drawing.Size(232, 316);
            this.lbDataFieldList.TabIndex = 3;
            this.lbDataFieldList.DoubleClick += new System.EventHandler(this.lbDataFieldList_DoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Read Fields from File";
            // 
            // edReadCSV
            // 
            this.edReadCSV.Location = new System.Drawing.Point(145, 12);
            this.edReadCSV.Name = "edReadCSV";
            this.edReadCSV.Size = new System.Drawing.Size(356, 20);
            this.edReadCSV.TabIndex = 5;
            // 
            // bbReadCSV
            // 
            this.bbReadCSV.Location = new System.Drawing.Point(501, 11);
            this.bbReadCSV.Name = "bbReadCSV";
            this.bbReadCSV.Size = new System.Drawing.Size(27, 23);
            this.bbReadCSV.TabIndex = 6;
            this.bbReadCSV.Text = "...";
            this.bbReadCSV.UseVisualStyleBackColor = true;
            this.bbReadCSV.Click += new System.EventHandler(this.bbReadCSV_Click);
            // 
            // bbReadFields
            // 
            this.bbReadFields.Location = new System.Drawing.Point(569, 41);
            this.bbReadFields.Name = "bbReadFields";
            this.bbReadFields.Size = new System.Drawing.Size(75, 23);
            this.bbReadFields.TabIndex = 7;
            this.bbReadFields.Text = "Read Fields";
            this.bbReadFields.UseVisualStyleBackColor = true;
            this.bbReadFields.Click += new System.EventHandler(this.bbReadFields_Click);
            // 
            // lbFromFieldList
            // 
            this.lbFromFieldList.FormattingEnabled = true;
            this.lbFromFieldList.Location = new System.Drawing.Point(27, 172);
            this.lbFromFieldList.Name = "lbFromFieldList";
            this.lbFromFieldList.Size = new System.Drawing.Size(232, 316);
            this.lbFromFieldList.TabIndex = 8;
            this.lbFromFieldList.DoubleClick += new System.EventHandler(this.lbFromFieldList_DoubleClick);
            // 
            // bbSaveMapping
            // 
            this.bbSaveMapping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbSaveMapping.Location = new System.Drawing.Point(882, 418);
            this.bbSaveMapping.Name = "bbSaveMapping";
            this.bbSaveMapping.Size = new System.Drawing.Size(75, 36);
            this.bbSaveMapping.TabIndex = 9;
            this.bbSaveMapping.Text = "Save Mapping";
            this.bbSaveMapping.UseVisualStyleBackColor = true;
            this.bbSaveMapping.Click += new System.EventHandler(this.bbSaveMapping_Click);
            // 
            // edDelimiter
            // 
            this.edDelimiter.Location = new System.Drawing.Point(623, 13);
            this.edDelimiter.Name = "edDelimiter";
            this.edDelimiter.Size = new System.Drawing.Size(21, 20);
            this.edDelimiter.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(546, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "CSV Delimiter";
            // 
            // cbQuotations
            // 
            this.cbQuotations.AutoSize = true;
            this.cbQuotations.Location = new System.Drawing.Point(416, 98);
            this.cbQuotations.Name = "cbQuotations";
            this.cbQuotations.Size = new System.Drawing.Size(135, 17);
            this.cbQuotations.TabIndex = 18;
            this.cbQuotations.Text = "Enclosed in Quotations";
            this.cbQuotations.UseVisualStyleBackColor = true;
            // 
            // cbHasHeaders
            // 
            this.cbHasHeaders.AutoSize = true;
            this.cbHasHeaders.Location = new System.Drawing.Point(302, 98);
            this.cbHasHeaders.Name = "cbHasHeaders";
            this.cbHasHeaders.Size = new System.Drawing.Size(108, 17);
            this.cbHasHeaders.TabIndex = 17;
            this.cbHasHeaders.Text = "Has Header Row";
            this.cbHasHeaders.UseVisualStyleBackColor = true;
            // 
            // edFieldCount
            // 
            this.edFieldCount.Location = new System.Drawing.Point(169, 95);
            this.edFieldCount.Name = "edFieldCount";
            this.edFieldCount.Size = new System.Drawing.Size(100, 20);
            this.edFieldCount.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Number of Fields in CSV File.";
            // 
            // edHeaderRow
            // 
            this.edHeaderRow.Location = new System.Drawing.Point(144, 66);
            this.edHeaderRow.Name = "edHeaderRow";
            this.edHeaderRow.Size = new System.Drawing.Size(39, 20);
            this.edHeaderRow.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Header Row is at : ";
            // 
            // edDataRow
            // 
            this.edDataRow.Location = new System.Drawing.Point(295, 66);
            this.edDataRow.Name = "edDataRow";
            this.edDataRow.Size = new System.Drawing.Size(39, 20);
            this.edDataRow.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(198, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Data starts at Row: ";
            // 
            // cmbDataTypes
            // 
            this.cmbDataTypes.FormattingEnabled = true;
            this.cmbDataTypes.Location = new System.Drawing.Point(356, 444);
            this.cmbDataTypes.Name = "cmbDataTypes";
            this.cmbDataTypes.Size = new System.Drawing.Size(181, 21);
            this.cmbDataTypes.TabIndex = 39;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(288, 447);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "Data Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(288, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Mapping Definition";
            // 
            // btnAddMapping
            // 
            this.btnAddMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMapping.Image")));
            this.btnAddMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddMapping.Location = new System.Drawing.Point(336, 414);
            this.btnAddMapping.Name = "btnAddMapping";
            this.btnAddMapping.Size = new System.Drawing.Size(237, 23);
            this.btnAddMapping.TabIndex = 36;
            this.btnAddMapping.Text = "MapField";
            this.btnAddMapping.UseVisualStyleBackColor = true;
            this.btnAddMapping.Click += new System.EventHandler(this.btnAddMapping_Click);
            // 
            // btnAddField
            // 
            this.btnAddField.Location = new System.Drawing.Point(288, 414);
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.Size = new System.Drawing.Size(42, 23);
            this.btnAddField.TabIndex = 35;
            this.btnAddField.Text = ">";
            this.btnAddField.UseVisualStyleBackColor = true;
            this.btnAddField.Click += new System.EventHandler(this.btnAddField_Click);
            // 
            // btnAddRequiredField
            // 
            this.btnAddRequiredField.Location = new System.Drawing.Point(579, 414);
            this.btnAddRequiredField.Name = "btnAddRequiredField";
            this.btnAddRequiredField.Size = new System.Drawing.Size(38, 23);
            this.btnAddRequiredField.TabIndex = 34;
            this.btnAddRequiredField.Text = "<";
            this.btnAddRequiredField.UseVisualStyleBackColor = true;
            this.btnAddRequiredField.Click += new System.EventHandler(this.btnAddRequiredField_Click);
            // 
            // dgMapping
            // 
            this.dgMapping.AllowUserToAddRows = false;
            this.dgMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col1,
            this.Col2,
            this.Col3});
            this.dgMapping.Location = new System.Drawing.Point(288, 192);
            this.dgMapping.Name = "dgMapping";
            this.dgMapping.ReadOnly = true;
            this.dgMapping.RowHeadersVisible = false;
            this.dgMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMapping.Size = new System.Drawing.Size(329, 216);
            this.dgMapping.TabIndex = 33;
            // 
            // Col1
            // 
            this.Col1.DataPropertyName = "ConvertType";
            this.Col1.HeaderText = "Type";
            this.Col1.Name = "Col1";
            this.Col1.ReadOnly = true;
            this.Col1.Width = 50;
            // 
            // Col2
            // 
            this.Col2.DataPropertyName = "MapFrom";
            this.Col2.HeaderText = "Map From";
            this.Col2.Name = "Col2";
            this.Col2.ReadOnly = true;
            this.Col2.Width = 120;
            // 
            // Col3
            // 
            this.Col3.DataPropertyName = "MapTo";
            this.Col3.HeaderText = "Map To";
            this.Col3.Name = "Col3";
            this.Col3.ReadOnly = true;
            this.Col3.Width = 120;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Fields from Data File";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(641, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Operation Required Fields";
            // 
            // edMapDescr
            // 
            this.edMapDescr.Location = new System.Drawing.Point(172, 131);
            this.edMapDescr.Name = "edMapDescr";
            this.edMapDescr.Size = new System.Drawing.Size(356, 20);
            this.edMapDescr.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Mapping Description";
            // 
            // frmMappingDefinition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 497);
            this.Controls.Add(this.edMapDescr);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbDataTypes);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnAddMapping);
            this.Controls.Add(this.btnAddField);
            this.Controls.Add(this.btnAddRequiredField);
            this.Controls.Add(this.dgMapping);
            this.Controls.Add(this.edDataRow);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.edHeaderRow);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbQuotations);
            this.Controls.Add(this.cbHasHeaders);
            this.Controls.Add(this.edFieldCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.edDelimiter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bbSaveMapping);
            this.Controls.Add(this.lbFromFieldList);
            this.Controls.Add(this.bbReadFields);
            this.Controls.Add(this.bbReadCSV);
            this.Controls.Add(this.edReadCSV);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbDataFieldList);
            this.Controls.Add(this.cmdOperationList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbCLose);
            this.Name = "frmMappingDefinition";
            this.Text = "Mapping Definition";
            this.Load += new System.EventHandler(this.frmMappingDefinition_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbCLose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmdOperationList;
        private System.Windows.Forms.ListBox lbDataFieldList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edReadCSV;
        private System.Windows.Forms.Button bbReadCSV;
        private System.Windows.Forms.Button bbReadFields;
        private System.Windows.Forms.ListBox lbFromFieldList;
        private System.Windows.Forms.Button bbSaveMapping;
        private System.Windows.Forms.TextBox edDelimiter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbQuotations;
        private System.Windows.Forms.CheckBox cbHasHeaders;
        private System.Windows.Forms.TextBox edFieldCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edHeaderRow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edDataRow;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDataTypes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAddMapping;
        private System.Windows.Forms.Button btnAddField;
        private System.Windows.Forms.Button btnAddRequiredField;
        private System.Windows.Forms.DataGridView dgMapping;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edMapDescr;
        private System.Windows.Forms.Label label10;
    }
}