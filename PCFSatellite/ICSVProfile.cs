﻿namespace PCFSatellite
{
    interface ICSVProfile
    {
        string csvName
        {
            get; set;
        }
        char csvDelimiter
        {
            get; set;
        }
        int fieldCount
        {
            get; set;
        }

        string fieldNameFirst
        {
            get; set;
        }

        string fieldNameLast
        {
            get; set;
        }

        bool hasHeaderRow
        {
            get; set;
        }



    }
}
