﻿

using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface IProfileRepository : IGenericRepository<Profile>
    { }


}