﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface IFileDescriptionRepository : IGenericRepository<FileDescription>
    {
        SatelliteModel SatelliteModel { get; }
    }
}