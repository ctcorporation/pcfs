﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface IEnumDTO
    {
        SatelliteModel Context { get; set; }

        string GetEnum(string enumType, string mapValue);
        string GetMapValue(string enumType, string eNum);
    }
}