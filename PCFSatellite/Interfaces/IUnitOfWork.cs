﻿using System;

namespace PCFSatellite
{
    public interface IUnitOfWork : IDisposable
    {

        IFileDescriptionRepository FileDescriptions
        { get; }
        ICargowiseContextRepository CargowiseContexts
        {
            get;
        }
        IProductRepository Products { get; }
        IDTSRepository DTSs { get; }
        ICustomerProfileRepository CustomerProfiles { get; }
        IMapOperationRepository MapOperations { get; }
        ICustProfileRepository CustProfiles { get; }
        ITransactionRepository Transactions { get; }
        ICargowise_EnumsRepository CargowiseEnums { get; }
        ICustomerRepository Customers { get; }
        IProfileRepository Profiles { get; }

        IMapEnumRepository MapEnums { get; }
        int Complete();

    }
}