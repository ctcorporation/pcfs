﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface IProductRepository : IGenericRepository<Models.Product>
    {
        SatelliteModel SatelliteModel { get; }
    }
}