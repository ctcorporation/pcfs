﻿

using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {

    }
}