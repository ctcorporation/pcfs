﻿using PCFSatellite.Models;
using System.Collections.Generic;

namespace PCFSatellite
{
    public interface IMapOperationRepository : IGenericRepository<MapOperation>
    {
        List<string> GetDistinctMaps();
        SatelliteModel SatelliteModel { get; }
    }
}