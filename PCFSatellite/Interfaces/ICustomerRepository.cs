﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface ICustomerRepository
    {
        SatelliteModel SatelliteModel { get; }
    }
}