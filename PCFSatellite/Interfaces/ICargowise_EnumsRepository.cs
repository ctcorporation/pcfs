﻿
using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface ICargowise_EnumsRepository : IGenericRepository<Cargowise_Enums>
    {
        SatelliteModel SatelliteModel { get; }
    }
}