﻿using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface ICargowiseContextRepository : IGenericRepository<CargowiseContext>
    {
        SatelliteModel SatelliteModel { get; }
    }
}