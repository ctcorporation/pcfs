﻿
using PCFSatellite.Models;

namespace PCFSatellite
{
    public interface ICustProfileRepository : IGenericRepository<Profile>
    {

    }
}