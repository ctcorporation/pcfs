﻿using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace PCFSatellite
{
    static class NodeResources
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var knownKeys = new HashSet<TKey>();
            return source.Where(element => knownKeys.Add(keySelector(element)));
        }
        public static string GetDelimiter(string fileName)
        {

            char[] pattern = new char[] { Convert.ToChar(";"), Convert.ToChar(","), Convert.ToChar("|"), Convert.ToChar("*"), Convert.ToChar(9) };
            int fCount = 0;
            char c = new char();
            for (int dCount = 0; dCount < pattern.Length; dCount++)
            {
                string[] fields = fileName.Split(pattern[dCount]);
                if (fields.Length > 0)
                {
                    if (fields.Length > fCount)
                    {
                        fCount = fields.Length;
                        c = pattern[dCount];
                    }

                }

            }

            return c.ToString();
        }

        public static string ReplaceFirstOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.LastIndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            try
            {
                StreamReader sr = new StreamReader(strFilePath);
                string[] headers = sr.ReadLine().Split(',');
                DataTable dt = new DataTable();
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    // var line = sr.ReadLine().Replace("\"\"", "");
                    string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    if (headers.Length == rows.Length)
                    {
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i];
                        }
                        bool emptyCol = true;
                        for (int i = 0; i < dr.ItemArray.Length; i++)
                        {
                            if (string.IsNullOrEmpty(dr[i].ToString()))
                            {
                                emptyCol = true;
                                dr[i] = string.Empty;
                            }
                            else
                            {
                                if (dr[i].ToString() == "\"\"")
                                {
                                    dr[i] = string.Empty;
                                }
                                // Replace "" in a string with a single ' 
                                if (dr[i].ToString().Contains("\"\""))
                                {
                                    dr[i] = dr[i].ToString().Replace("\"\"", "'");
                                }
                                if (dr[i].ToString().Contains("'"))
                                {
                                    dr[i].ToString().Replace("'", @"\'");

                                }
                                emptyCol = false;
                                char firstChar = !string.IsNullOrEmpty(dr[i].ToString()) ? dr[i].ToString()[0] : '\0';
                                char lastChar = !string.IsNullOrEmpty(dr[i].ToString()) ? dr[i].ToString()[dr[i].ToString().Length - 1] : '\0';

                                if (firstChar == Convert.ToChar("\'"))
                                {
                                    string newString = dr[i].ToString();
                                    newString = ReplaceFirstOccurrence(newString, "\"", string.Empty);
                                    dr[i] = newString;
                                }
                                if (lastChar == Convert.ToChar("\'"))
                                {
                                    string newString = dr[i].ToString();
                                    newString = ReplaceLastOccurrence(newString, "\"", string.Empty);
                                    dr[i] = newString;
                                }

                            }
                            //if (!string.IsNullOrEmpty(dr[i].ToString()))
                            //{
                            //    //Replace Empty Strings "" with string.empty
                            //    if (dr[i].ToString() == "\"\"")
                            //    {
                            //        dr[i] = string.Empty;
                            //    }
                            //    // Replace "" in a string with a single ' 
                            //    if (dr[i].ToString().Contains("\"\""))
                            //    {
                            //        dr[i] = dr[i].ToString().Replace("\"\"", "'");
                            //    }
                            //    // Remove the first and last " from a string 
                            //    if (dr[i].ToString()[0] == '"' & dr[i].ToString()[dr[i].ToString().Length - 1] == '"')
                            //    {
                            //        string s = dr[i].ToString();
                            //        s = ReplaceFirstOccurrence(s, "\"", string.Empty);
                            //        s = ReplaceLastOccurrence(s, "\"", string.Empty);
                            //        dr[i] = s;
                            //    }
                            //    emptyCol = false;
                            //}   
                        }
                        if (!emptyCol)
                        {
                            dt.Rows.Add(dr);
                        }

                    }
                }
                sr.Close();

                return dt;
            }
            catch (IOException ex)
            {
                return null;
            }

        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath, int headerFrom, int dataFrom)
        {
            StreamReader sr = new StreamReader(strFilePath);
            for (int i = 0; i < dataFrom; i++)
            {
                sr.ReadLine();
            }
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {

                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }


        public static string[] GetAddressFromText(string address)
        {
            address.Replace(',', ' ');
            string[] add = address.Replace(',', ' ').Split(' ');
            add = add.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (add.Length == 1)
            {
                add = address.Split(',');
            }
            if (add.Length == 2)
            {
                if (!add[1].All(Char.IsDigit))
                {
                    add = GetAddressFromText(address.Replace(',', ' '));
                }


            }

            return add;

        }


        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public static bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }

        public static void AddText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static string GetEnum(string enumType, string mapValue)
        {
            string result = string.Empty;

            using (NodeData.IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.glConnString)))
            {
                var enumVal = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == enumType && x.CW_MAPVALUE == mapValue).FirstOrDefault();
                if (enumVal != null)
                {
                    result = enumVal.CW_ENUM;
                }
            }

            return result;

        }

        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename;
            }
            return newfile;
        }

    }


}
