﻿using System.Windows.Forms;

namespace PCFSatellite
{
    public static class Prompt
    {
        public static string ShowDialog(string text, string Caption)
        {
            Form prompt = new Form()
            {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = Caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label lblText = new Label() { Text = text, Left = 50, Top = 20, Width = 420 };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 100 };
            Button btnOk = new Button() { Text = "Ok", Left = 350, Top = 70, Width = 100, DialogResult = DialogResult.OK };
            btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            btnOk.Image = ((System.Drawing.Image)(resources.GetObject("bbClose.Image")));
            btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnOk.Location = new System.Drawing.Point(400, 80);
            btnOk.Name = "bbClose";
            btnOk.Size = new System.Drawing.Size(75, 23);
            btnOk.TabIndex = 5;
            btnOk.Text = "&Ok";
            btnOk.UseVisualStyleBackColor = true;
            btnOk.Click += (sender, e) => { prompt.Close(); };

            prompt.Controls.Add(lblText);
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(btnOk);
            prompt.AcceptButton = btnOk;
            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";

        }

    }
}
