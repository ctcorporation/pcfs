﻿using System;
using static PCFSatellite.SatelliteErrors;

namespace PCFSatellite
{
    class ProcessingErrors
    {
        private Guid procid;

        private string senderid;

        private string recipientid;

        private CTCErrorCode errorCode;

        private string filename;

        private DateTime procdate;

        private string reference;

        private string reftype;

        public Guid ProcId
        {
            get
            { return procid; }
            set
            { procid = value; }
        }

        public string SenderId
        {
            get
            {
                return senderid;
            }
            set
            {
                senderid = value;
            }
        }
        public string RecipientId
        {
            get
            {
                return recipientid;
            }
            set
            {
                recipientid = value;
            }
        }

        public CTCErrorCode ErrorCode
        {
            get
            {
                return errorCode;

            }
            set
            {
                errorCode = value;
            }
        }
        public string FileName
        {
            get
            { return filename; }
            set
            { filename = value; }
        }

        public string Reference
        {
            get { return reference; }
            set
            {
                reference = value;
            }
        }
        public string RefType
        {
            get
            {
                return reftype;
            }
            set
            {
                reftype = value;
            }
        }

    }
}
