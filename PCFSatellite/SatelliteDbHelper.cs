﻿using PCFSatellite;
using PCFSatellite.DTO;
using PCFSatellite.Models;
using System.Linq;

namespace SatelliteData
{
    public class SatelliteDBHelper
    {
        private string _connString { get; set; }

        public SatelliteDBHelper(string connString)
        {
            _connString = connString;
        }

        public bool ValidProduct(PCFSatellite.Models.Product product)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteModel(Globals.glConnString)))
            {
                var prod = uow.Products.Find(x => x.PW_PARTNUM == product.PW_PARTNUM).FirstOrDefault();
                if (prod != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public string GetBarcodeFromProductCode(string code)
        {
            using (IUnitOfWork uow = new UnitOfWork(new SatelliteModel(Globals.glConnString)))
            {
                var prod = uow.Products.Find(x => x.PW_PARTNUM == code).FirstOrDefault();
                return prod.PW_BARCODE;
            }

        }
    }
}
