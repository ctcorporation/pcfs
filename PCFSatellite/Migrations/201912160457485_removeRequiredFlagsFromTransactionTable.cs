﻿namespace PCFSatellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequiredFlagsFromTransactionTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "T_LOGMSG", c => c.String());
            AddColumn("dbo.Transactions", "T_LOGTYPE", c => c.String(maxLength: 10));
            AlterColumn("dbo.Transactions", "T_TRIAL", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
            AlterColumn("dbo.Transactions", "T_INVOICED", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "T_INVOICED", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
            AlterColumn("dbo.Transactions", "T_TRIAL", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
            DropColumn("dbo.Transactions", "T_LOGTYPE");
            DropColumn("dbo.Transactions", "T_LOGMSG");
        }
    }
}
