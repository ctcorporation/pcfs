﻿namespace PCFSatellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialBuild : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Cargowise_Enums",
            //    c => new
            //        {
            //            CW_ID = c.Guid(nullable: false),
            //            CW_ENUMTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            CW_ENUM = c.String(maxLength: 10, fixedLength: true),
            //            CW_MAPVALUE = c.String(maxLength: 20, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CW_ID);
            
            //CreateTable(
            //    "dbo.CargowiseContext",
            //    c => new
            //        {
            //            CC_ID = c.Guid(nullable: false),
            //            CC_Context = c.String(maxLength: 50, unicode: false),
            //            CC_Description = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.CC_ID);
            
            //CreateTable(
            //    "dbo.CONTACTS",
            //    c => new
            //        {
            //            S_ID = c.Guid(nullable: false),
            //            S_CONTACTNAME = c.String(maxLength: 50, unicode: false),
            //            S_EMAILADDRESS = c.String(unicode: false),
            //            S_C = c.Guid(),
            //            S_ALERTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.S_ID);
            
            //CreateTable(
            //    "dbo.VW_CustomerProfile",
            //    c => new
            //        {
            //            C_IS_ACTIVE = c.String(maxLength: 1),
            //            C_ON_HOLD = c.String(maxLength: 1),
            //            C_FTP_CLIENT = c.String(maxLength: 1),
            //            P_ID = c.Guid(nullable: false),
            //            C_ID = c.Guid(nullable: false),
            //            P_DIRECTION = c.String(maxLength: 1),
            //            P_DTS = c.String(maxLength: 1),
            //            P_ACTIVE = c.String(maxLength: 1),
            //            P_NOTIFY = c.String(maxLength: 1),
            //            C_NAME = c.String(maxLength: 50),
            //            C_CODE = c.String(maxLength: 15),
            //            C_PATH = c.String(maxLength: 80),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3),
            //            P_SERVER = c.String(),
            //            P_USERNAME = c.String(),
            //            P_PASSWORD = c.String(),
            //            P_DESCRIPTION = c.String(),
            //            P_PORT = c.String(maxLength: 10),
            //            P_DELIVERY = c.String(maxLength: 1),
            //            P_PATH = c.String(maxLength: 100),
            //            P_XSD = c.String(maxLength: 50),
            //            P_LIBNAME = c.String(maxLength: 50),
            //            P_RECIPIENTID = c.String(maxLength: 15),
            //            P_SENDERID = c.String(maxLength: 15),
            //            P_BILLTO = c.String(maxLength: 15),
            //            P_CHARGEABLE = c.String(maxLength: 1),
            //            P_MSGTYPE = c.String(maxLength: 20),
            //            P_MESSAGETYPE = c.String(maxLength: 3),
            //            P_FILETYPE = c.String(maxLength: 7),
            //            P_EMAILADDRESS = c.String(maxLength: 100),
            //            P_SSL = c.String(maxLength: 1),
            //            P_SENDEREMAIL = c.String(maxLength: 100),
            //            P_SUBJECT = c.String(maxLength: 100),
            //            P_MESSAGEDESCR = c.String(maxLength: 20),
            //            P_EVENTCODE = c.String(maxLength: 3),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50),
            //            C_SHORTNAME = c.String(maxLength: 10),
            //            P_METHOD = c.String(maxLength: 50),
            //            P_PARAMLIST = c.String(),
            //            P_NOTIFYEMAIL = c.String(maxLength: 100),
            //            P_MAPOPERATION = c.String(maxLength: 100),
            //        })
            //    .PrimaryKey(t => t.P_ID);
            
            //CreateTable(
            //    "dbo.Customer",
            //    c => new
            //        {
            //            C_ID = c.Guid(nullable: false),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_PATH = c.String(maxLength: 80, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_TRIALSTART = c.DateTime(),
            //            C_TRIALEND = c.DateTime(),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //        })
            //    .PrimaryKey(t => t.C_ID);
            
            //CreateTable(
            //    "dbo.DTS",
            //    c => new
            //        {
            //            D_ID = c.Guid(nullable: false),
            //            D_C = c.Guid(),
            //            D_INDEX = c.Int(),
            //            D_FINALPROCESSING = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            D_P = c.Guid(),
            //            D_FILETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            D_DTSTYPE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            D_DTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            D_SEARCHPATTERN = c.String(unicode: false),
            //            D_NEWVALUE = c.String(unicode: false),
            //            D_QUALIFIER = c.String(unicode: false),
            //            D_TARGET = c.String(unicode: false),
            //            D_CURRENTVALUE = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.D_ID);
            
            //CreateTable(
            //    "dbo.FileDescription",
            //    c => new
            //        {
            //            PC_ID = c.Guid(nullable: false),
            //            PC_P = c.Guid(),
            //            PC_FileName = c.String(maxLength: 50, unicode: false),
            //            PC_HasHeader = c.Boolean(),
            //            PC_Delimiter = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            PC_Fieldcount = c.Int(),
            //            PC_FirstFieldName = c.String(maxLength: 50, unicode: false),
            //            PC_LastFieldName = c.String(maxLength: 50, unicode: false),
            //            PC_Quotations = c.Boolean(),
            //            PC_HeaderStart = c.Int(),
            //            PC_DataStart = c.Int(),
            //        })
            //    .PrimaryKey(t => t.PC_ID);
            
            //CreateTable(
            //    "dbo.MapOperations",
            //    c => new
            //        {
            //            MD_ID = c.Guid(nullable: false),
            //            MD_MapDescription = c.String(nullable: false, maxLength: 100),
            //            MD_Type = c.String(nullable: false, maxLength: 4),
            //            MD_FromField = c.String(maxLength: 50),
            //            MD_ToField = c.String(maxLength: 50),
            //            MD_DataType = c.String(),
            //        })
            //    .PrimaryKey(t => t.MD_ID);
            
            //CreateTable(
            //    "dbo.Mapping_Definition",
            //    c => new
            //        {
            //            M_ID = c.Guid(nullable: false),
            //            M_DataFrom = c.String(maxLength: 50),
            //            M_DataTo = c.String(maxLength: 50),
            //            M_FieldFrom = c.String(maxLength: 50),
            //            M_FieldTo = c.String(maxLength: 50),
            //            M_DataType = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            M_Length = c.Int(),
            //            M_P = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.M_ID);
            
            //CreateTable(
            //    "dbo.PCFS-WSF-Products",
            //    c => new
            //        {
            //            PW_ID = c.Guid(nullable: false),
            //            PW_ARTICLEID = c.String(maxLength: 10, fixedLength: true),
            //            PW_PARTNUM = c.String(maxLength: 50, unicode: false),
            //            PW_PRODUCTNAME = c.String(maxLength: 150, unicode: false),
            //            PW_BARCODE = c.String(maxLength: 20, unicode: false),
            //            PW_UOM = c.String(maxLength: 50, unicode: false),
            //            PW_CWUOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            PW_IMPORTED = c.DateTime(),
            //            PW_LASTUPDATE = c.DateTime(),
            //            PW_CARGOWISEREPLY = c.DateTime(),
            //            PW_CHILDARTICLE = c.String(maxLength: 10, fixedLength: true),
            //            PW_CHILDQTY = c.Int(),
            //            PW_CHILDUOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.PW_ID);
            
            //CreateTable(
            //    "dbo.PCFS-WSF-PurchaseLine",
            //    c => new
            //        {
            //            PL_ID = c.Guid(nullable: false),
            //            PL_PO = c.Guid(),
            //            PL_Code = c.String(maxLength: 50, unicode: false),
            //            PL_QTY = c.Int(),
            //            PL_UOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            PL_Article = c.String(maxLength: 10, fixedLength: true),
            //        })
            //    .PrimaryKey(t => t.PL_ID);
            
            //CreateTable(
            //    "dbo.PCFS-WSF-PurchaseOrder",
            //    c => new
            //        {
            //            PO_ID = c.Guid(nullable: false),
            //            PO_POID = c.Int(),
            //            PO_POCODE = c.String(maxLength: 20, unicode: false),
            //            PO_ETA = c.DateTime(),
            //            PO_CARGOWISEREPLY = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.PO_ID);
            
            //CreateTable(
            //    "dbo.PCFS-WSF-SalesLine",
            //    c => new
            //        {
            //            SL_ID = c.Guid(nullable: false),
            //            SL_SO = c.Guid(),
            //            SL_ARTICLE = c.Int(),
            //            SL_QTY = c.Int(),
            //            SL_PARTNUM = c.String(maxLength: 50, unicode: false),
            //            SL_UOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            SL_LINEID = c.Int(),
            //            SL_ORIGINALARTICLE = c.Int(),
            //            SL_PACKSIZE = c.Int(),
            //            SL_LINENO = c.Int(),
            //            SL_BARCODE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            SL_DESCRIPTION = c.String(maxLength: 100, unicode: false),
            //            SL_BATCHNO = c.String(maxLength: 50, unicode: false),
            //        })
            //    .PrimaryKey(t => t.SL_ID);
            
            //CreateTable(
            //    "dbo.PCFS-WSF-SalesOrder",
            //    c => new
            //        {
            //            SO_ID = c.Guid(nullable: false),
            //            SO_SOID = c.Int(),
            //            SO_SOCODE = c.String(maxLength: 20, fixedLength: true),
            //            SO_ORDERDATE = c.DateTime(),
            //            SO_SHIPDATE = c.DateTime(),
            //            SO_CUSTOMER = c.String(maxLength: 100, unicode: false),
            //            SO_DELSTREET1 = c.String(maxLength: 100, unicode: false),
            //            SO_DELSTREET2 = c.String(maxLength: 100, unicode: false),
            //            SO_DELPOCODE = c.String(maxLength: 4, fixedLength: true, unicode: false),
            //            SO_DELSUBURB = c.String(maxLength: 100, unicode: false),
            //            SO_DELSTATE = c.String(maxLength: 20, unicode: false),
            //            SO_CARGOWISEREPLY = c.DateTime(),
            //            SO_PICKINGNOTES = c.String(unicode: false),
            //            SO_DELIVERYNOTES = c.String(unicode: false),
            //            SO_DELIVERYENTITY = c.String(maxLength: 100, unicode: false),
            //            SO_ORGCODE = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            SO_PH = c.String(maxLength: 20, unicode: false),
            //            SO_COMPANY = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.SO_ID);
            
            //CreateTable(
            //    "dbo.PCFS-WSF-UnitConversions",
            //    c => new
            //        {
            //            UC_ID = c.Guid(nullable: false),
            //            UC_PW = c.Guid(),
            //            UC_QTY = c.Int(),
            //            UC_PARENTUOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            UC_UOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.UC_ID);
            
            //CreateTable(
            //    "dbo.Processing_Errors",
            //    c => new
            //        {
            //            E_PK = c.Guid(nullable: false),
            //            E_SENDERID = c.String(maxLength: 15, unicode: false),
            //            E_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            E_PROCDATE = c.DateTime(),
            //            E_FILENAME = c.String(unicode: false),
            //            E_ERRORDESC = c.String(unicode: false),
            //            E_ERRORCODE = c.String(maxLength: 10, fixedLength: true),
            //            E_P = c.Guid(),
            //            E_IGNORE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            E_REFERENCE = c.String(maxLength: 50, unicode: false),
            //            E_REFTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.E_PK);
            
            //CreateTable(
            //    "dbo.Profile",
            //    c => new
            //        {
            //            P_ID = c.Guid(nullable: false),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 7, fixedLength: true, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            P_GROUPCHARGES = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_NOTIFY = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_NOTIFYEMAIL = c.String(maxLength: 100, unicode: false),
            //            P_MAPOPERATION = c.String(maxLength: 100),
            //        })
            //    .PrimaryKey(t => t.P_ID);
            
            //CreateTable(
            //    "dbo.TODO",
            //    c => new
            //        {
            //            L_ID = c.Guid(nullable: false),
            //            L_P = c.Guid(),
            //            L_FILENAME = c.String(unicode: false),
            //            L_LASTRESULT = c.String(unicode: false),
            //            L_DATE = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.L_ID);
            
            //CreateTable(
            //    "dbo.Transaction_Log",
            //    c => new
            //        {
            //            X_ID = c.Guid(nullable: false),
            //            X_P = c.Guid(),
            //            X_FILENAME = c.String(maxLength: 150, unicode: false),
            //            X_DATE = c.DateTime(),
            //            X_SUCCESS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            X_C = c.Guid(),
            //            X_LASTRESULT = c.String(maxLength: 120, unicode: false),
            //        })
            //    .PrimaryKey(t => t.X_ID);
            
            //CreateTable(
            //    "dbo.Transactions",
            //    c => new
            //        {
            //            T_ID = c.Guid(nullable: false),
            //            T_C = c.Guid(),
            //            T_P = c.Guid(),
            //            T_DATETIME = c.DateTime(),
            //            T_FILENAME = c.String(maxLength: 100, unicode: false),
            //            T_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            T_INVOICED = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            T_INVOICEDATE = c.DateTime(),
            //            T_MSGTYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            T_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            T_DIRECTION = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            T_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            T_REF1 = c.String(maxLength: 50),
            //            T_REF2 = c.String(maxLength: 50),
            //            T_REF3 = c.String(maxLength: 50),
            //            T_ARCHIVE = c.String(maxLength: 100),
            //            T_REF1TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            T_REF2TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            T_REF3TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.T_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Transactions");
            DropTable("dbo.Transaction_Log");
            DropTable("dbo.TODO");
            DropTable("dbo.Profile");
            DropTable("dbo.Processing_Errors");
            DropTable("dbo.PCFS-WSF-UnitConversions");
            DropTable("dbo.PCFS-WSF-SalesOrder");
            DropTable("dbo.PCFS-WSF-SalesLine");
            DropTable("dbo.PCFS-WSF-PurchaseOrder");
            DropTable("dbo.PCFS-WSF-PurchaseLine");
            DropTable("dbo.PCFS-WSF-Products");
            DropTable("dbo.Mapping_Definition");
            DropTable("dbo.MapOperations");
            DropTable("dbo.FileDescription");
            DropTable("dbo.DTS");
            DropTable("dbo.Customer");
            DropTable("dbo.VW_CustomerProfile");
            DropTable("dbo.CONTACTS");
            DropTable("dbo.CargowiseContext");
            DropTable("dbo.Cargowise_Enums");
        }
    }
}
