﻿namespace PCFSatellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changebarcodelength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PCFS-WSF-Products", "PW_BARCODE", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PCFS-WSF-Products", "PW_BARCODE", c => c.String(maxLength: 20, unicode: false));
        }
    }
}
