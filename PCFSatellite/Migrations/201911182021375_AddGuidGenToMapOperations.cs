﻿namespace PCFSatellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddGuidGenToMapOperations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MapOperations", "MD_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
        }

        public override void Down()
        {
        }
    }
}
