﻿namespace PCFSatellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class procerrorsupdate : DbMigration
    {
        public override void Up()
        {
            //DropPrimaryKey("dbo.PROCESSING_ERRORS");
            //DropPrimaryKey("dbo.Transactions");
            //AlterColumn("dbo.PROCESSING_ERRORS", "E_PK", c => c.Guid(nullable: false, identity: true));
            //AlterColumn("dbo.Transactions", "T_ID", c => c.Guid(nullable: false, identity: true));
            //AddPrimaryKey("dbo.PROCESSING_ERRORS", "E_PK");
            //AddPrimaryKey("dbo.Transactions", "T_ID");
        }

        public override void Down()
        {
            DropPrimaryKey("dbo.Transactions");
            DropPrimaryKey("dbo.PROCESSING_ERRORS");
            AlterColumn("dbo.Transactions", "T_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.PROCESSING_ERRORS", "E_PK", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Transactions", "T_ID");
            AddPrimaryKey("dbo.PROCESSING_ERRORS", "E_PK");
        }
    }
}
