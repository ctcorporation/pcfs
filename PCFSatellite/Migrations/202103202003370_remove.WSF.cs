﻿namespace PCFSatellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class removeWSF : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.PCFS-WSF-PurchaseLine");
            DropTable("dbo.PCFS-WSF-PurchaseOrder");
            DropTable("dbo.PCFS-WSF-SalesLine");
            DropTable("dbo.PCFS-WSF-SalesOrder");
            DropTable("dbo.PCFS-WSF-UnitConversions");
        }

        public override void Down()
        {
            CreateTable(
                "dbo.PCFS-WSF-UnitConversions",
                c => new
                {
                    UC_ID = c.Guid(nullable: false),
                    UC_PW = c.Guid(),
                    UC_QTY = c.Int(),
                    UC_PARENTUOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    UC_UOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
                })
                .PrimaryKey(t => t.UC_ID);

            CreateTable(
                "dbo.PCFS-WSF-SalesOrder",
                c => new
                {
                    SO_ID = c.Guid(nullable: false),
                    SO_SOID = c.Int(),
                    SO_SOCODE = c.String(maxLength: 20, fixedLength: true),
                    SO_ORDERDATE = c.DateTime(),
                    SO_SHIPDATE = c.DateTime(),
                    SO_CUSTOMER = c.String(maxLength: 100, unicode: false),
                    SO_DELSTREET1 = c.String(maxLength: 100, unicode: false),
                    SO_DELSTREET2 = c.String(maxLength: 100, unicode: false),
                    SO_DELPOCODE = c.String(maxLength: 4, fixedLength: true, unicode: false),
                    SO_DELSUBURB = c.String(maxLength: 100, unicode: false),
                    SO_DELSTATE = c.String(maxLength: 20, unicode: false),
                    SO_CARGOWISEREPLY = c.DateTime(),
                    SO_PICKINGNOTES = c.String(unicode: false),
                    SO_DELIVERYNOTES = c.String(unicode: false),
                    SO_DELIVERYENTITY = c.String(maxLength: 100, unicode: false),
                    SO_ORGCODE = c.String(maxLength: 15, fixedLength: true, unicode: false),
                    SO_PH = c.String(maxLength: 20, unicode: false),
                    SO_COMPANY = c.String(maxLength: 100, unicode: false),
                })
                .PrimaryKey(t => t.SO_ID);

            CreateTable(
                "dbo.PCFS-WSF-SalesLine",
                c => new
                {
                    SL_ID = c.Guid(nullable: false),
                    SL_SO = c.Guid(),
                    SL_ARTICLE = c.Int(),
                    SL_QTY = c.Int(),
                    SL_PARTNUM = c.String(maxLength: 50, unicode: false),
                    SL_UOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    SL_LINEID = c.Int(),
                    SL_ORIGINALARTICLE = c.Int(),
                    SL_PACKSIZE = c.Int(),
                    SL_LINENO = c.Int(),
                    SL_BARCODE = c.String(maxLength: 20, fixedLength: true, unicode: false),
                    SL_DESCRIPTION = c.String(maxLength: 100, unicode: false),
                    SL_BATCHNO = c.String(maxLength: 50, unicode: false),
                })
                .PrimaryKey(t => t.SL_ID);

            CreateTable(
                "dbo.PCFS-WSF-PurchaseOrder",
                c => new
                {
                    PO_ID = c.Guid(nullable: false),
                    PO_POID = c.Int(),
                    PO_POCODE = c.String(maxLength: 20, unicode: false),
                    PO_ETA = c.DateTime(),
                    PO_CARGOWISEREPLY = c.DateTime(),
                })
                .PrimaryKey(t => t.PO_ID);

            CreateTable(
                "dbo.PCFS-WSF-PurchaseLine",
                c => new
                {
                    PL_ID = c.Guid(nullable: false),
                    PL_PO = c.Guid(),
                    PL_Code = c.String(maxLength: 50, unicode: false),
                    PL_QTY = c.Int(),
                    PL_UOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    PL_Article = c.String(maxLength: 10, fixedLength: true),
                })
                .PrimaryKey(t => t.PL_ID);

        }
    }
}
