﻿namespace PCFSatellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateFileDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileDescription", "PC_Description", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileDescription", "PC_Description");
        }
    }
}
