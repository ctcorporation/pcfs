﻿using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;


namespace PhytoTherapy
{
    class NodeResources
    {

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath, bool hasHeaders)
        {
            StreamReader sr = new StreamReader(strFilePath);
            if (hasHeaders)
            {

            }
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }



        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }



        public static string GetEnum(string enumType, string mapValue)
        {
            string result = string.Empty;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
                {

                    SqlCommand sqlEnum = new SqlCommand("GetCWEnum", sqlConn);
                    sqlEnum.CommandType = CommandType.StoredProcedure;
                    SqlParameter varEnum = sqlEnum.Parameters.Add("@ENUM", SqlDbType.NChar, 50);
                    SqlParameter varEnumType = sqlEnum.Parameters.Add("@ENUMTYPE", SqlDbType.VarChar, 100);
                    SqlParameter varMapVal = sqlEnum.Parameters.Add("@MAPVALUE", SqlDbType.VarChar, 100);
                    varEnum.Direction = ParameterDirection.Output;
                    varEnumType.Value = enumType;
                    varMapVal.Value = mapValue;
                    sqlConn.Open();
                    sqlEnum.ExecuteNonQuery();
                    result = varEnum.Value.ToString().Trim();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Accessing Database: " + ex.Message);
            }
            return result;

        }

        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename;
            }
            return newfile;
        }

    }


}
