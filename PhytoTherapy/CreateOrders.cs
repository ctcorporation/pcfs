﻿using System;

namespace PhytoTherapy
{
    public class CreateOrders
    {
        public static Tuple<Guid, TransReference> DespatchOrder(string fileName, CustProfileRecord cust)
        {
            DataTable dt = NodeResources.ConvertCSVtoDataTable(fileName);
            DataSet dsPCFS = new DataSet();
            dsPCFS.Tables.Add(dt);
            string archive = Globals.ArchiveFile(Globals.glArcLocation, fileName);
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            // File.Delete(fileName);
            Guid result = new Guid();
            TransReference resTrans = new TransReference();
            using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
            {
                SqlCommand addPCFSSo = new SqlCommand();
                addPCFSSo.Connection = sqlConn;
                addPCFSSo.CommandText = "AddPCFS_SO";
                addPCFSSo.CommandType = CommandType.StoredProcedure;
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                SqlParameter orderNo = addPCFSSo.Parameters.Add("@ORDERNO", SqlDbType.NChar, 20);
                SqlParameter so_orderdate = addPCFSSo.Parameters.Add("@SO_ORDERDATE", SqlDbType.DateTime);
                SqlParameter so_shipdate = addPCFSSo.Parameters.Add("@SO_SHIPDATE", SqlDbType.DateTime);
                SqlParameter so_customer = addPCFSSo.Parameters.Add("@SO_CUSTOMER", SqlDbType.VarChar, 100);
                SqlParameter so_delstreet1 = addPCFSSo.Parameters.Add("@SO_DELSTREET1", SqlDbType.VarChar, 100);
                SqlParameter so_delstreet2 = addPCFSSo.Parameters.Add("@SO_DELSTREET2", SqlDbType.VarChar, 100);
                SqlParameter so_delpocode = addPCFSSo.Parameters.Add("@SO_DELPOCODE", SqlDbType.Char, 4);
                SqlParameter so_delsuburb = addPCFSSo.Parameters.Add("@SO_DELSUBURB", SqlDbType.VarChar, 100);
                SqlParameter so_delstate = addPCFSSo.Parameters.Add("@SO_DELSTATE", SqlDbType.VarChar, 20);
                SqlParameter so_deliveryNotes = addPCFSSo.Parameters.Add("@SO_DELIVERYNOTES", SqlDbType.VarChar, -1);
                SqlParameter so_pickingNotes = addPCFSSo.Parameters.Add("@SO_PICKINGNOTES", SqlDbType.VarChar, -1);
                SqlParameter so_ph = addPCFSSo.Parameters.Add("@SO_PH", SqlDbType.VarChar, 20);
                SqlParameter so_company = addPCFSSo.Parameters.Add("@SO_COMPANY", SqlDbType.VarChar, 100);
                SqlParameter so_id = addPCFSSo.Parameters.Add("@SO_ID", SqlDbType.UniqueIdentifier);
                SqlParameter so_deliveryEntity = addPCFSSo.Parameters.Add("@SO_DELIVERYENTITY", SqlDbType.VarChar, 100);
                SqlParameter so_OrgCode = addPCFSSo.Parameters.Add("@SO_ORGCODE", SqlDbType.Char, 15);
                so_id.Direction = ParameterDirection.Output;
                SqlCommand addPCFSSOL = new SqlCommand("AddPCFS_SOLine", sqlConn);
                addPCFSSOL.CommandType = CommandType.StoredProcedure;
                SqlParameter sol_id = addPCFSSOL.Parameters.Add("@SO_ID", SqlDbType.UniqueIdentifier);
                SqlParameter soqty = addPCFSSOL.Parameters.Add("@QTY", SqlDbType.Int);
                SqlParameter itemBarcode = addPCFSSOL.Parameters.Add("@ITEMBARCODE", SqlDbType.VarChar, 20);
                SqlParameter lineNo = addPCFSSOL.Parameters.Add("@LINENO", SqlDbType.Int);
                SqlParameter partNo = addPCFSSOL.Parameters.Add("@PARTNUM", SqlDbType.VarChar, 50);
                SqlParameter uom = addPCFSSOL.Parameters.Add("@UOM", SqlDbType.VarChar, 10);

                SqlParameter productName = addPCFSSOL.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar, 100);
                string tempNotes = string.Empty;

                if (dt.Columns.Count == 11)
                {
                    DataRow dr = dt.Rows[9];
                    orderNo.Value = dr[1].ToString().Replace("\"", string.Empty);
                    resTrans.Ref1Type = TransReference.RefType.Order;
                    resTrans.Reference1 = orderNo.Value.ToString();


                    DateTime dOrderDate;
                    int iAddressEnd = 0;

                    for (int i = 0; i < 8; i++)
                    {
                        if (dt.Rows[i][0].ToString().Replace("\"", string.Empty).Contains("*****"))
                        {
                            iAddressEnd = i - 1;
                            break;
                        }
                    }
                    so_customer.Value = dt.Rows[0][0].ToString().Replace("\"", string.Empty);

                    if (iAddressEnd > 4)
                    {
                        so_company.Value = dt.Rows[1][0].ToString().Replace("\"", string.Empty);
                        so_delstreet1.Value = dt.Rows[2][0].ToString().Replace("\"", string.Empty);
                        so_delstreet2.Value = dt.Rows[3][0].ToString().Replace("\"", string.Empty);

                    }
                    else
                    {
                        so_delstreet1.Value = dt.Rows[1][0].ToString().Replace("\"", string.Empty);
                    }
                    string[] substate = dt.Rows[iAddressEnd][0].ToString().Replace("\"", string.Empty).Split(' ');
                    if (substate.Length == 3)
                    {
                        so_delsuburb.Value = substate[0];
                        so_delstate.Value = substate[1];
                        so_delpocode.Value = substate[2];

                    }
                    string suburb = string.Empty;
                    if (substate.Length > 3)
                    {
                        so_delpocode.Value = substate[substate.Length - 1];
                        so_delstate.Value = substate[substate.Length - 2];
                        for (int i = 0; i < substate.Length - 2; i++)
                        {
                            suburb += substate[i] + " ";
                        }
                        so_delsuburb.Value = suburb;
                    }



                    if (DateTime.TryParse(dr[5].ToString().Replace("\"", string.Empty), out dOrderDate))
                    {
                        so_orderdate.Value = dOrderDate;
                    }
                    so_OrgCode.Value = cust.P_Senderid;
                    DateTime dShipDate;
                    if (DateTime.TryParse(dt.Rows[10][1].ToString().Replace("\"", string.Empty), out dShipDate))
                    {
                        so_shipdate.Value = dShipDate;
                    }

                    try
                    {
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        sqlConn.Open();

                        addPCFSSo.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        string strEx = ex.GetType().Name;
                        //  NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: " + strEx + ex.Message + ". " + "", System.Drawing.Color.Red);
                        MailModule.sendMsg(fileName, Globals.glAlertsTo, "PCFS Unhandled Exception: ProcessCustomXML", ex);

                        resTrans.Ref3Type = TransReference.RefType.Error;
                        resTrans.Reference3 = "Error importing Order";
                        resTrans.Ref2Type = TransReference.RefType.Order;
                        resTrans.Reference2 = orderNo.Value.ToString();
                        return new Tuple<Guid, TransReference>(Guid.Empty, resTrans);
                    }
                    sol_id.Value = so_id.Value;
                    result = (Guid)so_id.Value;
                    int iLineno = 0;
                    for (int i = 12; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[i][0].ToString().Replace("\"", string.Empty)))
                        {
                            iLineno++;
                            lineNo.Value = iLineno;
                            partNo.Value = dt.Rows[i][0].ToString().Replace("\"", string.Empty); ;
                            productName.Value = dt.Rows[i][2].ToString().Replace("\"", string.Empty); ;
                            string sUom = NodeResources.GetEnum("UOM", dt.Rows[i][5].ToString().Replace("\"", string.Empty).ToUpper());
                            if (!string.IsNullOrEmpty(sUom))
                            {
                                uom.Value = sUom;
                            }
                            else
                                uom.Value = "CTN";
                            decimal iqty;
                            soqty.Value = decimal.TryParse(dt.Rows[i][4].ToString().Replace("\"", string.Empty), out iqty) ? (int)iqty : 0;
                            if (sqlConn.State == ConnectionState.Open)
                            {
                                sqlConn.Close();
                            }
                            sqlConn.Open();
                            addPCFSSOL.ExecuteNonQuery();
                        }

                    }
                }

            }

            return new Tuple<Guid, TransReference>(result, resTrans);

        }
    }
}
